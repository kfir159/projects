﻿
namespace TriviaClient
{
    partial class FormJoinRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.buttonJoinRoom = new System.Windows.Forms.Button();
            this.listRooms = new System.Windows.Forms.ListBox();
            this.labelError = new System.Windows.Forms.Label();
            this.buttonBack = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(261, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Rooms:";
            // 
            // buttonJoinRoom
            // 
            this.buttonJoinRoom.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.buttonJoinRoom.FlatAppearance.BorderSize = 0;
            this.buttonJoinRoom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonJoinRoom.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.buttonJoinRoom.Location = new System.Drawing.Point(350, 279);
            this.buttonJoinRoom.Name = "buttonJoinRoom";
            this.buttonJoinRoom.Size = new System.Drawing.Size(75, 32);
            this.buttonJoinRoom.TabIndex = 2;
            this.buttonJoinRoom.Text = "Join Room";
            this.buttonJoinRoom.UseVisualStyleBackColor = false;
            this.buttonJoinRoom.Click += new System.EventHandler(this.buttonJoinRoom_Click);
            // 
            // listRooms
            // 
            this.listRooms.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.listRooms.FormattingEnabled = true;
            this.listRooms.Location = new System.Drawing.Point(264, 133);
            this.listRooms.Name = "listRooms";
            this.listRooms.Size = new System.Drawing.Size(274, 95);
            this.listRooms.TabIndex = 3;
            // 
            // labelError
            // 
            this.labelError.AutoSize = true;
            this.labelError.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelError.ForeColor = System.Drawing.Color.IndianRed;
            this.labelError.Location = new System.Drawing.Point(358, 337);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(40, 18);
            this.labelError.TabIndex = 13;
            this.labelError.Text = "dsda";
            this.labelError.Visible = false;
            // 
            // buttonBack
            // 
            this.buttonBack.BackgroundImage = global::TriviaClient.Properties.Resources.backArrorButton1;
            this.buttonBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonBack.FlatAppearance.BorderSize = 0;
            this.buttonBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBack.Location = new System.Drawing.Point(1, 1);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(62, 57);
            this.buttonBack.TabIndex = 26;
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // FormJoinRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(33)))), ((int)(((byte)(36)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonBack);
            this.Controls.Add(this.labelError);
            this.Controls.Add(this.listRooms);
            this.Controls.Add(this.buttonJoinRoom);
            this.Controls.Add(this.label1);
            this.Name = "FormJoinRoom";
            this.Text = "FormJoinRoom";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonJoinRoom;
        private System.Windows.Forms.ListBox listRooms;
        private System.Windows.Forms.Label labelError;
        private System.Windows.Forms.Button buttonBack;
    }
}