﻿
namespace TriviaClient
{
    partial class FormMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCreateRoom = new System.Windows.Forms.Button();
            this.buttonJoinRoom = new System.Windows.Forms.Button();
            this.buttonLogout = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.buttonHighScore = new System.Windows.Forms.Button();
            this.buttonPersonalStatistics = new System.Windows.Forms.Button();
            this.labelError = new System.Windows.Forms.Label();
            this.buttonAddQuestion = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonCreateRoom
            // 
            this.buttonCreateRoom.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.buttonCreateRoom.FlatAppearance.BorderSize = 0;
            this.buttonCreateRoom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCreateRoom.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.buttonCreateRoom.Location = new System.Drawing.Point(116, 37);
            this.buttonCreateRoom.Name = "buttonCreateRoom";
            this.buttonCreateRoom.Size = new System.Drawing.Size(153, 59);
            this.buttonCreateRoom.TabIndex = 0;
            this.buttonCreateRoom.Text = "Create Room";
            this.buttonCreateRoom.UseVisualStyleBackColor = false;
            this.buttonCreateRoom.Click += new System.EventHandler(this.buttonCreateRoom_Click);
            // 
            // buttonJoinRoom
            // 
            this.buttonJoinRoom.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.buttonJoinRoom.FlatAppearance.BorderSize = 0;
            this.buttonJoinRoom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonJoinRoom.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.buttonJoinRoom.Location = new System.Drawing.Point(526, 156);
            this.buttonJoinRoom.Name = "buttonJoinRoom";
            this.buttonJoinRoom.Size = new System.Drawing.Size(153, 59);
            this.buttonJoinRoom.TabIndex = 1;
            this.buttonJoinRoom.Text = "Join Room";
            this.buttonJoinRoom.UseVisualStyleBackColor = false;
            this.buttonJoinRoom.Click += new System.EventHandler(this.buttonJoinRoom_Click);
            // 
            // buttonLogout
            // 
            this.buttonLogout.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.buttonLogout.FlatAppearance.BorderSize = 0;
            this.buttonLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.buttonLogout.Location = new System.Drawing.Point(526, 278);
            this.buttonLogout.Name = "buttonLogout";
            this.buttonLogout.Size = new System.Drawing.Size(153, 59);
            this.buttonLogout.TabIndex = 2;
            this.buttonLogout.Text = "Logout";
            this.buttonLogout.UseVisualStyleBackColor = false;
            this.buttonLogout.Click += new System.EventHandler(this.buttonLogout_Click);
            // 
            // buttonExit
            // 
            this.buttonExit.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.buttonExit.FlatAppearance.BorderSize = 0;
            this.buttonExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.buttonExit.Location = new System.Drawing.Point(319, 341);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(153, 59);
            this.buttonExit.TabIndex = 3;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = false;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // buttonHighScore
            // 
            this.buttonHighScore.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.buttonHighScore.FlatAppearance.BorderSize = 0;
            this.buttonHighScore.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonHighScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.buttonHighScore.Location = new System.Drawing.Point(116, 156);
            this.buttonHighScore.Name = "buttonHighScore";
            this.buttonHighScore.Size = new System.Drawing.Size(153, 59);
            this.buttonHighScore.TabIndex = 5;
            this.buttonHighScore.Text = "High Score";
            this.buttonHighScore.UseVisualStyleBackColor = false;
            this.buttonHighScore.Click += new System.EventHandler(this.buttonHighScore_Click);
            // 
            // buttonPersonalStatistics
            // 
            this.buttonPersonalStatistics.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.buttonPersonalStatistics.FlatAppearance.BorderSize = 0;
            this.buttonPersonalStatistics.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPersonalStatistics.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.buttonPersonalStatistics.Location = new System.Drawing.Point(526, 37);
            this.buttonPersonalStatistics.Name = "buttonPersonalStatistics";
            this.buttonPersonalStatistics.Size = new System.Drawing.Size(153, 59);
            this.buttonPersonalStatistics.TabIndex = 6;
            this.buttonPersonalStatistics.Text = "My Statistics";
            this.buttonPersonalStatistics.UseVisualStyleBackColor = false;
            this.buttonPersonalStatistics.Click += new System.EventHandler(this.buttonPersonalStatistics_Click);
            // 
            // labelError
            // 
            this.labelError.AutoSize = true;
            this.labelError.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelError.ForeColor = System.Drawing.Color.IndianRed;
            this.labelError.Location = new System.Drawing.Point(364, 227);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(40, 18);
            this.labelError.TabIndex = 23;
            this.labelError.Text = "dsda";
            this.labelError.Visible = false;
            // 
            // buttonAddQuestion
            // 
            this.buttonAddQuestion.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.buttonAddQuestion.FlatAppearance.BorderSize = 0;
            this.buttonAddQuestion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddQuestion.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.buttonAddQuestion.Location = new System.Drawing.Point(116, 278);
            this.buttonAddQuestion.Name = "buttonAddQuestion";
            this.buttonAddQuestion.Size = new System.Drawing.Size(153, 59);
            this.buttonAddQuestion.TabIndex = 24;
            this.buttonAddQuestion.Text = "Add Question";
            this.buttonAddQuestion.UseVisualStyleBackColor = false;
            this.buttonAddQuestion.Click += new System.EventHandler(this.buttonAddQuestion_Click);
            // 
            // button1
            // 
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(62, 57);
            this.button1.TabIndex = 25;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // FormMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(33)))), ((int)(((byte)(36)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonAddQuestion);
            this.Controls.Add(this.labelError);
            this.Controls.Add(this.buttonPersonalStatistics);
            this.Controls.Add(this.buttonHighScore);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.buttonLogout);
            this.Controls.Add(this.buttonJoinRoom);
            this.Controls.Add(this.buttonCreateRoom);
            this.Name = "FormMenu";
            this.Text = "FormMenu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonCreateRoom;
        private System.Windows.Forms.Button buttonJoinRoom;
        private System.Windows.Forms.Button buttonLogout;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Button buttonHighScore;
        private System.Windows.Forms.Button buttonPersonalStatistics;
        private System.Windows.Forms.Label labelError;
        private System.Windows.Forms.Button buttonAddQuestion;
        private System.Windows.Forms.Button button1;
    }
}