﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;

namespace TriviaClient
{
    public partial class FormViewRoom : Form
    {
        private int _id;
        private string _name;
        private bool _started;

        public FormViewRoom(string roomName, int id)
        {
            this._name = roomName;
            this._id = id;
            this._started = false;
            
            InitializeComponent();
            this.Text = "View Room Screen";

            this.labelRoomName.Text = this._name;
            this.timer1.Interval = 3000;
            this.timer1.Start();
        }

        /// <summary>
        ///  This function do refrash to the list of players in the room.
        /// </summary>
        private void timer1_Tick(object sender, EventArgs e)
        {
            (int code, JObject j) = Requests.req(Codes.GET_STATE, "{}");

            if (code == Codes.ERROR)
            {
                this.labelError.Text = "an error accured";
                return;
            }

            string[] players = j["players"].ToString().Split(',');

            if(j["gameBegun"].ToString() == "1")
            {
                if (!this._started)
                {
                    this._started = true;
                    FormGame game = new FormGame(Int32.Parse(j["answerTimeout"].ToString()),
                    Int32.Parse(j["questionCount"].ToString()));
                    this.Hide();
                    game.ShowDialog();
                    this.Show();
                }
            }
            else if (this._started)
            {
                this.leaveGame();
            }

            this.listPlayers.Items.Clear();

            if (players.Length >= 1)
            {
                this.labelAdmin.Text = players[0];
            }

            if (Program.username == this.labelAdmin.Text)
            {
                this.buttonStartGame.Visible = true;
            }

            foreach (string player in players)
            {
                this.listPlayers.Items.Add(player);
            }
        }

        /// <summary>
        ///  This function button to start the game.
        /// </summary>
        private void buttonStartGame_Click(object sender, EventArgs e)
        {
            (int code, JObject j) = Requests.req(Codes.START_GAME, "{}");
            
            if (code == Codes.ERROR)
            {
                this.labelError.Text = "Something went wrong while trying to start the game.";
                return;
            }
        }

        /// <summary>
        ///  This function is button to back.
        /// </summary>
        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.leaveGame();
            this.Close();
        }

        private void leaveGame()
        {
            this.timer1.Stop();

            int reqCode = (this.labelAdmin.Text == Program.username) ? Codes.CLOSE_ROOM : Codes.LEAVE_ROOM;
            (int code, JObject j) = Requests.req(reqCode, "{}");

            if (code == Codes.ERROR)
            {
                this.labelError.Text = "an error accured";
                return;
            }
        }
    }
}
