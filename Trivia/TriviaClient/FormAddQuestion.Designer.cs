﻿
namespace TriviaClient
{
    partial class FormAddQuestion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxQuestion = new System.Windows.Forms.TextBox();
            this.textBoxAns2 = new System.Windows.Forms.TextBox();
            this.textBoxAns3 = new System.Windows.Forms.TextBox();
            this.textBoxCorrectAns = new System.Windows.Forms.TextBox();
            this.textBoxAns1 = new System.Windows.Forms.TextBox();
            this.buttonSubmit = new System.Windows.Forms.Button();
            this.labelFeedback = new System.Windows.Forms.Label();
            this.buttonBack = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxQuestion
            // 
            this.textBoxQuestion.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.textBoxQuestion.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxQuestion.Location = new System.Drawing.Point(240, 55);
            this.textBoxQuestion.Name = "textBoxQuestion";
            this.textBoxQuestion.Size = new System.Drawing.Size(513, 24);
            this.textBoxQuestion.TabIndex = 0;
            // 
            // textBoxAns2
            // 
            this.textBoxAns2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.textBoxAns2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxAns2.Location = new System.Drawing.Point(452, 112);
            this.textBoxAns2.Name = "textBoxAns2";
            this.textBoxAns2.Size = new System.Drawing.Size(100, 24);
            this.textBoxAns2.TabIndex = 1;
            // 
            // textBoxAns3
            // 
            this.textBoxAns3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.textBoxAns3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxAns3.Location = new System.Drawing.Point(653, 112);
            this.textBoxAns3.Name = "textBoxAns3";
            this.textBoxAns3.Size = new System.Drawing.Size(100, 24);
            this.textBoxAns3.TabIndex = 2;
            // 
            // textBoxCorrectAns
            // 
            this.textBoxCorrectAns.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBoxCorrectAns.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxCorrectAns.Location = new System.Drawing.Point(452, 167);
            this.textBoxCorrectAns.Name = "textBoxCorrectAns";
            this.textBoxCorrectAns.Size = new System.Drawing.Size(100, 24);
            this.textBoxCorrectAns.TabIndex = 3;
            // 
            // textBoxAns1
            // 
            this.textBoxAns1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.textBoxAns1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxAns1.Location = new System.Drawing.Point(240, 112);
            this.textBoxAns1.Name = "textBoxAns1";
            this.textBoxAns1.Size = new System.Drawing.Size(100, 24);
            this.textBoxAns1.TabIndex = 4;
            // 
            // buttonSubmit
            // 
            this.buttonSubmit.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.buttonSubmit.FlatAppearance.BorderSize = 0;
            this.buttonSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.buttonSubmit.Location = new System.Drawing.Point(332, 300);
            this.buttonSubmit.Name = "buttonSubmit";
            this.buttonSubmit.Size = new System.Drawing.Size(132, 74);
            this.buttonSubmit.TabIndex = 5;
            this.buttonSubmit.Text = "submit";
            this.buttonSubmit.UseVisualStyleBackColor = false;
            this.buttonSubmit.Click += new System.EventHandler(this.buttonSubmit_Click);
            // 
            // labelFeedback
            // 
            this.labelFeedback.AutoSize = true;
            this.labelFeedback.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelFeedback.Location = new System.Drawing.Point(356, 231);
            this.labelFeedback.Name = "labelFeedback";
            this.labelFeedback.Size = new System.Drawing.Size(33, 18);
            this.labelFeedback.TabIndex = 6;
            this.labelFeedback.Text = "tmp";
            this.labelFeedback.Visible = false;
            // 
            // buttonBack
            // 
            this.buttonBack.BackgroundImage = global::TriviaClient.Properties.Resources.backArrorButton1;
            this.buttonBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonBack.FlatAppearance.BorderSize = 0;
            this.buttonBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBack.Location = new System.Drawing.Point(2, 1);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(62, 57);
            this.buttonBack.TabIndex = 26;
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Location = new System.Drawing.Point(125, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 20);
            this.label1.TabIndex = 27;
            this.label1.Text = "Question:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label2.Location = new System.Drawing.Point(105, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 20);
            this.label2.TabIndex = 28;
            this.label2.Text = "Wrong Answers:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label3.Location = new System.Drawing.Point(105, 171);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 20);
            this.label3.TabIndex = 29;
            this.label3.Text = "Correct Answer:";
            // 
            // FormAddQuestion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(33)))), ((int)(((byte)(36)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonBack);
            this.Controls.Add(this.labelFeedback);
            this.Controls.Add(this.buttonSubmit);
            this.Controls.Add(this.textBoxAns1);
            this.Controls.Add(this.textBoxCorrectAns);
            this.Controls.Add(this.textBoxAns3);
            this.Controls.Add(this.textBoxAns2);
            this.Controls.Add(this.textBoxQuestion);
            this.Name = "FormAddQuestion";
            this.Text = "FormAddQuestion";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxQuestion;
        private System.Windows.Forms.TextBox textBoxAns2;
        private System.Windows.Forms.TextBox textBoxAns3;
        private System.Windows.Forms.TextBox textBoxCorrectAns;
        private System.Windows.Forms.TextBox textBoxAns1;
        private System.Windows.Forms.Button buttonSubmit;
        private System.Windows.Forms.Label labelFeedback;
        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}