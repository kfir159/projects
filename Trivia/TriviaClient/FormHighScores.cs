﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Windows.Forms;

namespace TriviaClient
{
    public partial class FormHighScores : Form
    {
        public FormHighScores()
        {
            InitializeComponent();
            this.Text = "High Scores Screen";

            string[] scores = GetTopUsers();

            if (scores.Length == 0)
            {
                this.labelError.Text = "there are no users.";
            }
            else
            {
                if (scores.Length >= 1)
                {
                    string[] split = scores[0].Split('.');
                    this.labelFirstUsername.Text = split[0];
                    this.labelFirstScore.Text = split[1];
                }
                if (scores.Length >= 2)
                {
                    string[] split = scores[1].Split('.');
                    this.labelSecondUsername.Text = split[0];
                    this.labelSecondScore.Text = split[1];
                }
                if (scores.Length >= 3)
                {
                    string[] split = scores[2].Split('.');
                    this.labelThirdUsername.Text = split[0];
                    this.labelThirdScore.Text = split[1];
                }
                if (scores.Length >= 4)
                {
                    string[] split = scores[3].Split('.');
                    this.labelFourthUsername.Text = split[0];
                    this.labelFourthScore.Text = split[1];
                }
                if (scores.Length == 5)
                {
                    string[] split = scores[4].Split('.');
                    this.labelFifthUsername.Text = split[0];
                    this.labelFifthScore.Text = split[1];
                }
            }
           
        }

        /// <summary>
        /// Getting the top 5 users and their scores from the severs db
        /// </summary>
        /// <returns>Top 5 users in (name.score,name2.score2)</returns>
        private string[] GetTopUsers()
        {
            (int code, JObject j) = Requests.req(Codes.GET_HIGH_SCORE, "{}");

            if (code == Codes.ERROR)
            {
                this.labelError.Text = "error.";
                return null;
            }

            return j["scores"].ToString().Split(',');
        }

        /// <summary>
        /// Closes the program
        /// </summary>
        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
