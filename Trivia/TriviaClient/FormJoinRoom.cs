﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;

namespace TriviaClient
{
    public partial class FormJoinRoom : Form
    {
        public FormJoinRoom()
        {
            InitializeComponent();
            this.Text = "Join Room Screen";
            updateRooms();
        }

        /// <summary>
        /// Updating the room list
        /// </summary>
        private void updateRooms()
        {
            (int code, JObject j) = Requests.req(Codes.GET_ROOMS, "{}");

            if (code == Codes.ERROR)
            {
                this.labelError.Text = "Error in receving rooms from server.";
                return;
            }

            if (j["rooms"].ToString() == "")
            {
                return;
            }

            string[] rooms = j["rooms"].ToString().Split(',');

            foreach (string room in rooms)
            {
                listRooms.Items.Add(room.Split('.')[3]);
            }
        }

        /// <summary>
        /// Asking the server to join a chosen room.
        /// </summary>
        private void buttonJoinRoom_Click(object sender, EventArgs e)
        {
            if (listRooms.SelectedItems.Count != 1)
            {
                this.labelError.Text = "please choose one room.";
                return;
            }

            Dictionary<string, object> dict = new Dictionary<string, object> {  { "roomId", listRooms.SelectedIndex + 1  }  };

            (int code, JObject j) = Requests.req(Codes.JOIN_ROOM, dict);

            if (code == Codes.ERROR)
            {
                this.labelError.Text = "Couldnt join room.";
                return;
            }

            this.Hide();
            FormViewRoom view = new FormViewRoom(listRooms.SelectedItem.ToString(), listRooms.SelectedIndex + 1);
            view.ShowDialog();
            this.Show();
        }

        /// <summary>
        /// Closes the program.
        /// </summary>
        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
