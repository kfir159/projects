﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TriviaClient
{
    /// <summary>
    /// Helper class for keeping the codes organized
    /// </summary>
    static class Codes
    {
        public const int LOGIN = 1;
        public const int SIGNUP = 2;
        public const int LOGOUT = 3;
        public const int GET_ROOMS = 4;
        public const int CREATE_ROOM = 5;
        public const int JOIN_ROOM = 6;
        public const int GET_STATS = 7;
        public const int GET_HIGH_SCORE = 8;
        public const int GET_PLAYERS = 9;
        public const int CLOSE_ROOM = 10;
        public const int START_GAME = 11;
        public const int GET_STATE = 12;
        public const int LEAVE_ROOM = 13;
        public const int GET_RESULT = 14;
        public const int SUBMIT_ANSWERS = 15;
        public const int GET_QUESTION = 16;
        public const int LEAVE_GAME = 17;
        public const int ADD_QUESTION = 18;
        public const int ERROR = 101;
    }

    static class JsonHelper
    {
        /// <summary>
        /// Makes a given dict a binary string ready for the server
        /// </summary>
        public static string dictToBin(object dict)
        {
            string json = JsonConvert.SerializeObject(dict);
            // making the binary form of the request
            string bin = ToBinary(Encoding.ASCII.GetBytes(json));
            return bin;
        }

        /// <summary>
        /// Serializes a request for the server.
        /// </summary>
        public static byte[] Serialize(int reqId, object dict)
        {
            string bin = ToBinary(BitConverter.GetBytes(reqId)).PadLeft(8, '0');
            string dictBin = dictToBin(dict);
            int dataLen = (dictBin.Length % 8 == 0) ? (dictBin.Length / 8) : ((dictBin.Length / 8) + 1);
            bin += ToBinary(BitConverter.GetBytes(dataLen)).PadLeft(8 * 4, '0');
            bin += dictBin;
            return Encoding.ASCII.GetBytes(bin);
        }

        /// <summary>
        ///  This function do Deserialize to the buff.
        /// </summary>
        public static JObject Deserialize(string buff)
        {
            return JObject.Parse(buff);
        }

        /// <summary>
        /// Helper function for making data binary.
        /// </summary>
        public static string ToBinary(Byte[] data)
        {
            string bin = "";
            foreach (byte b in data)
            {
                if (b != 0)
                {
                    bin += Convert.ToString(b, 2).PadLeft(8, '0');
                }
            }
            return bin;
        }
    }
}
