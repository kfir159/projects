﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace TriviaClient
{
    static class Requests
    {
        /// <summary>
        /// Sends a general request to the server.
        /// </summary>
        /// <param name="code">Request code (From the Codes class)</param>
        /// <param name="reqDict">An object represnt the dict to serialize for the server.</param>
        /// <returns>Ret code and a json object to read output data.</returns>
        public static (int, JObject) req(int code, object reqDict)
        {
            Program.comm.Send(TriviaClient.JsonHelper.Serialize(code, reqDict));
            (int respCode, string resp) = Program.comm.Recv();

            JObject j = (respCode == Codes.ERROR) ? null : TriviaClient.JsonHelper.Deserialize(resp);
            return (respCode, j);
        }
    }
}
