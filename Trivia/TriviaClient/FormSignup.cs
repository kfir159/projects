﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TriviaClient
{
    public partial class FormSignup : Form
    {
        public FormSignup()
        {
            InitializeComponent();
            this.Text = "Signup Screen";
        }

        /// <summary>
        ///  This function is a button that do signup.
        /// </summary>
        private void ButtonSignup_Click(object sender, EventArgs e)
        {
            this.labelError.Hide();

            if (this.TextBoxEmail.TextLength == 0 || this.TextBoxUsername.TextLength == 0 ||
                this.TextBoxPassword.TextLength == 0)
            {
                this.labelError.Text = "insert email, username and password.";
                this.labelError.Show();
                return;
            }

            Dictionary<string, string> dict = new Dictionary<string, string> {
                { "username", this.TextBoxUsername.Text },
                { "password", this.TextBoxPassword.Text },
                { "email", this.TextBoxEmail.Text  }
            };

            (int code, JObject j) = Requests.req(Codes.SIGNUP, dict);

            if (code == Codes.ERROR)
            {
                this.labelError.Text = "signup failed.";
                this.labelError.Show();
                return;
            }

            Program.username = this.TextBoxUsername.Text;
            FormMenu menu = new FormMenu();

            this.Hide();
            menu.ShowDialog();
            this.Show();
        }

        /// <summary>
        ///  This function is button to back.
        /// </summary>
        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
