﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TriviaClient
{
    public partial class FormAddQuestion : Form
    {
        public FormAddQuestion()
        {
            InitializeComponent();
            this.Text = "Add Question Screen";
        }

        /// <summary>
        /// Click function that submit the fields of the screen as a new question.
        /// </summary>
        private void buttonSubmit_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>
            {
                {"question", this.textBoxQuestion.Text },
                {"wrongAns1", this.textBoxAns1.Text },
                {"wrongAns2", this.textBoxAns2.Text },
                {"wrongAns3", this.textBoxAns3.Text },
                {"correctAns", this.textBoxCorrectAns.Text }
,           };

            (int code, JObject j) = Requests.req(Codes.ADD_QUESTION, dict);

            if (code == Codes.ERROR)
            {
                this.labelFeedback.Text = "Something went wrong";
            }
            else
            {
                this.labelFeedback.Text = "Your question has been added to the data base";
            }
            this.labelFeedback.Visible = true;
        }

        /// <summary>
        /// Closes the program in click. 
        /// </summary>
        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
