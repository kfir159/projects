﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;

namespace TriviaClient
{
    public partial class FormPersonalStatistics : Form
    {
        public FormPersonalStatistics()
        {
            InitializeComponent();
            this.Text = "Personal Statistics Screen";

            this.labelUsername.Text = Program.username + "'s stats:";

            string[] stats =  GetUserStats();
            
            if (stats.Length != 5)
            {
                this.labelError.Text = "there are no enughe statistics.";
                return;
            }

            this.labelCorrectAnswers.Text = stats[0];
            this.labelGames.Text = stats[1];
            this.labelAnswers.Text = stats[2];
            this.labelAverageAnswerTime.Text = stats[3];
            this.labelScore.Text = stats[4];
        }

        /// <summary>
        ///  This function get the statistics of user.
        /// </summary>
        private string[] GetUserStats()
        {
            (int code, JObject j) = Requests.req(Codes.GET_STATS, "{}");

            if (code == Codes.ERROR)
            {
                this.labelError.Text = "error.";
                return null;
            }

            return j["stats"].ToString().Split(',');
        }

        /// <summary>
        ///  This function is button to back.
        /// </summary>
        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
