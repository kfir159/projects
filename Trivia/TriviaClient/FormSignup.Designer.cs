﻿
namespace TriviaClient
{
    partial class FormSignup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextBoxUsername = new System.Windows.Forms.TextBox();
            this.TextBoxEmail = new System.Windows.Forms.TextBox();
            this.TextBoxPassword = new System.Windows.Forms.TextBox();
            this.ButtonSignup = new System.Windows.Forms.Button();
            this.LabelUsername = new System.Windows.Forms.Label();
            this.LabelEmail = new System.Windows.Forms.Label();
            this.LabelPassword = new System.Windows.Forms.Label();
            this.labelError = new System.Windows.Forms.Label();
            this.buttonBack = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TextBoxUsername
            // 
            this.TextBoxUsername.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.TextBoxUsername.Location = new System.Drawing.Point(363, 100);
            this.TextBoxUsername.Name = "TextBoxUsername";
            this.TextBoxUsername.Size = new System.Drawing.Size(171, 20);
            this.TextBoxUsername.TabIndex = 0;
            // 
            // TextBoxEmail
            // 
            this.TextBoxEmail.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.TextBoxEmail.Location = new System.Drawing.Point(363, 141);
            this.TextBoxEmail.Name = "TextBoxEmail";
            this.TextBoxEmail.Size = new System.Drawing.Size(171, 20);
            this.TextBoxEmail.TabIndex = 1;
            // 
            // TextBoxPassword
            // 
            this.TextBoxPassword.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.TextBoxPassword.Location = new System.Drawing.Point(363, 180);
            this.TextBoxPassword.Name = "TextBoxPassword";
            this.TextBoxPassword.PasswordChar = '*';
            this.TextBoxPassword.Size = new System.Drawing.Size(171, 20);
            this.TextBoxPassword.TabIndex = 2;
            // 
            // ButtonSignup
            // 
            this.ButtonSignup.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ButtonSignup.FlatAppearance.BorderSize = 0;
            this.ButtonSignup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonSignup.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F);
            this.ButtonSignup.ForeColor = System.Drawing.Color.Black;
            this.ButtonSignup.Location = new System.Drawing.Point(343, 244);
            this.ButtonSignup.Name = "ButtonSignup";
            this.ButtonSignup.Size = new System.Drawing.Size(131, 57);
            this.ButtonSignup.TabIndex = 3;
            this.ButtonSignup.Text = "Signup";
            this.ButtonSignup.UseVisualStyleBackColor = false;
            this.ButtonSignup.Click += new System.EventHandler(this.ButtonSignup_Click);
            // 
            // LabelUsername
            // 
            this.LabelUsername.AutoSize = true;
            this.LabelUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.LabelUsername.ForeColor = System.Drawing.Color.White;
            this.LabelUsername.Location = new System.Drawing.Point(255, 98);
            this.LabelUsername.Name = "LabelUsername";
            this.LabelUsername.Size = new System.Drawing.Size(97, 22);
            this.LabelUsername.TabIndex = 4;
            this.LabelUsername.Text = "Username:";
            // 
            // LabelEmail
            // 
            this.LabelEmail.AutoSize = true;
            this.LabelEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.LabelEmail.ForeColor = System.Drawing.Color.White;
            this.LabelEmail.Location = new System.Drawing.Point(255, 141);
            this.LabelEmail.Name = "LabelEmail";
            this.LabelEmail.Size = new System.Drawing.Size(59, 22);
            this.LabelEmail.TabIndex = 5;
            this.LabelEmail.Text = "Email:";
            // 
            // LabelPassword
            // 
            this.LabelPassword.AutoSize = true;
            this.LabelPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.LabelPassword.ForeColor = System.Drawing.Color.White;
            this.LabelPassword.Location = new System.Drawing.Point(255, 179);
            this.LabelPassword.Name = "LabelPassword";
            this.LabelPassword.Size = new System.Drawing.Size(94, 22);
            this.LabelPassword.TabIndex = 6;
            this.LabelPassword.Text = "Password:";
            // 
            // labelError
            // 
            this.labelError.AutoSize = true;
            this.labelError.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelError.ForeColor = System.Drawing.Color.IndianRed;
            this.labelError.Location = new System.Drawing.Point(340, 330);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(97, 18);
            this.labelError.TabIndex = 10;
            this.labelError.Text = "signup failed. ";
            this.labelError.Visible = false;
            // 
            // buttonBack
            // 
            this.buttonBack.BackgroundImage = global::TriviaClient.Properties.Resources.backArrorButton1;
            this.buttonBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonBack.FlatAppearance.BorderSize = 0;
            this.buttonBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBack.Location = new System.Drawing.Point(3, 3);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(62, 57);
            this.buttonBack.TabIndex = 26;
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // FormSignup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(33)))), ((int)(((byte)(36)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonBack);
            this.Controls.Add(this.labelError);
            this.Controls.Add(this.LabelPassword);
            this.Controls.Add(this.LabelEmail);
            this.Controls.Add(this.LabelUsername);
            this.Controls.Add(this.ButtonSignup);
            this.Controls.Add(this.TextBoxPassword);
            this.Controls.Add(this.TextBoxEmail);
            this.Controls.Add(this.TextBoxUsername);
            this.Name = "FormSignup";
            this.Text = "FormSignup";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextBoxUsername;
        private System.Windows.Forms.TextBox TextBoxEmail;
        private System.Windows.Forms.TextBox TextBoxPassword;
        private System.Windows.Forms.Button ButtonSignup;
        private System.Windows.Forms.Label LabelUsername;
        private System.Windows.Forms.Label LabelEmail;
        private System.Windows.Forms.Label LabelPassword;
        private System.Windows.Forms.Label labelError;
        private System.Windows.Forms.Button buttonBack;
    }
}