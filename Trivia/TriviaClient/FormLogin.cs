﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TriviaClient
{
    public partial class FormLogin : Form
    {
        private FormSignup _signupForm;
        public FormLogin()
        {
            InitializeComponent();
            this.Text = "Login Screen";
            this._signupForm = new FormSignup();
        }

        /// <summary>
        /// Sends a login request to the server, checking input
        /// </summary>
        private void buttonLogin_Click(object sender, EventArgs e)
        {
            this.labelError.Hide();

            if (this.textBoxUsername.TextLength == 0 || this.textBoxPassword.TextLength == 0)
            {
                this.labelError.Text = "insert username and password.";
                this.labelError.Show();
                return;
            }

            Dictionary<string, string> dict = new Dictionary<string, string> {
                { "username", this.textBoxUsername.Text },
                { "password", this.textBoxPassword.Text }
            };

            (int code, JObject j) = Requests.req(Codes.LOGIN, dict);

            if (code == Codes.ERROR)
            {
                this.labelError.Text = "your password or username is wrong.";
                this.labelError.Show();
                return;
            }

            Program.username = this.textBoxUsername.Text;
            FormMenu menu = new FormMenu();
            this.Hide();
            menu.ShowDialog();
            this.Show(); // in case of returning to the login window
        }

        /// <summary>
        /// Goes to the signup screen.
        /// </summary>
        private void buttonSignup_Click(object sender, EventArgs e)
        {
            this.Hide();
            this._signupForm.ShowDialog();
            this.Show();
        }

        /// <summary>
        /// Exists the program
        /// </summary>
        private void button1_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            this.textBoxPassword.PasswordChar = (this.textBoxPassword.PasswordChar == '*') ? '\0' : '*';
        }
    }
}
