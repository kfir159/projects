﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;

namespace TriviaClient
{
    public partial class FormGame : Form
    {
        private Dictionary<int, int> _answerIds;

        private int _ansTimeOut;
        private int _questionsAmount;
        private int _currQuestion;
        private int _currTime;
        private int _countCorrectAns;

        public FormGame(int ansTimeOut, int questionsAmount)
        {
            this._ansTimeOut = ansTimeOut;
            this._questionsAmount = questionsAmount;
            this._currQuestion = 0;
            this._answerIds = new Dictionary<int, int>();
            this._countCorrectAns = 0;

            InitializeComponent();
            this.Text = "Game Screen";

            this.labelError.Visible = false;

            this._currTime = this._ansTimeOut;
            this.timer1.Interval = 1000;
            this.timer1.Start();

            this.RecvQuestion();
        }

        private void buttonAns1_Click(object sender, EventArgs e)
        {
            this.SubmitAns(this._answerIds[0]+1);
        }

        private void buttonAns2_Click(object sender, EventArgs e)
        {
            this.SubmitAns(this._answerIds[1]+1);
        }

        private void buttonAns3_Click(object sender, EventArgs e)
        {
            this.SubmitAns(this._answerIds[2]+1);
        }

        private void buttonAns4_Click(object sender, EventArgs e)
        {
            this.SubmitAns(this._answerIds[3]+1);
        }

        private void SubmitAns(int id)
        {
            // reseting the current time
            this._currTime = _ansTimeOut;

            Dictionary<string, int> dict = new Dictionary<string, int> {
                { "id", id }
            };

            (int code, JObject j) = Requests.req(Codes.SUBMIT_ANSWERS, dict);

            if (id == Int32.Parse(j["correctAnswerId"].ToString()))
            {
                this._countCorrectAns++;
            }

            if (code == Codes.ERROR)
            {
                this.labelError.Visible = true;
                this.labelError.Text = "Something went wrong while submiting your answer to the server.";
                return;
            }

            // checking if this was the last question to anwer
            if (this._questionsAmount - this._currQuestion == 1)
            {
                this.timer1.Stop();
                FormResults result = new FormResults();
                this.Hide();
                result.ShowDialog();
                this.Show();
                return;
            }

            this.RecvQuestion();          
        }

        private void RecvQuestion()
        {
            (int code, JObject j) = Requests.req(Codes.GET_QUESTION, "{}");

            if (code == Codes.ERROR)
            {
                this.labelError.Visible = true;
                this.labelError.Text = "Something went wrong while receving a question from the serevr.";
                return;
            }

            this.labelQuestion.Text = j["question"].ToString();

            string[] answers = new string[4];
            int id = 0;
            
            foreach (string ans in j["answers"].ToString().Split(','))
            {
                answers[id] = ans.Split('.')[1];
                id++;
            }

            int[] randomIds = {  0,  1,  2,  3  };
            this.Shuffle(randomIds);

            this._answerIds.Clear();
            for(int i = 0 ; i < 4; i++)
                this._answerIds.Add(i, randomIds[i]);

            // showing the user how many questions are left
            this.labelQuestionsLeft.Text = (this._questionsAmount - this._currQuestion).ToString();
            this._currQuestion++;
            this.labelCurrectAns.Text = this._countCorrectAns.ToString();

            // giving each button an answer text
            this.buttonAns1.Text = answers[this._answerIds[0]];
            this.buttonAns2.Text = answers[this._answerIds[1]];
            this.buttonAns3.Text = answers[this._answerIds[2]];
            this.buttonAns4.Text = answers[this._answerIds[3]];
        }

        private void Shuffle(int[] array)
        {
            Random rng = new Random();
            int n = array.Length;

            while (n > 1)
            {
                int k = rng.Next(n);
                n--;
                int temp = array[n];
                array[n] = array[k];
                array[k] = temp;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this._currTime--;
            this.labelTime.Text = this._currTime.ToString();

            // timout
            if (this._currTime == 0)
            {
                this.SubmitAns(5);
                this.RecvQuestion();
            }
        }
    }
}
