﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;

namespace TriviaClient
{
    public partial class FormMenu : Form
    {
        public FormMenu()
        {
            InitializeComponent();
            this.Text = "Menu Screen";
        }
        
        /// <summary>
        /// Opens the Create room screen.
        /// </summary>
        private void buttonCreateRoom_Click(object sender, EventArgs e)
        {
            FormCreateRoom createRoom = new FormCreateRoom();
            this.Hide();
            createRoom.ShowDialog();
            this.Show();
        }

        /// <summary>
        /// Opens the Join room screen.
        /// </summary>
        private void buttonJoinRoom_Click(object sender, EventArgs e)
        {
            FormJoinRoom joinRoom = new FormJoinRoom();
            this.Hide();
            joinRoom.ShowDialog();
            this.Show();
        }

        /// <summary>
        /// Exists the program.
        /// </summary>
        private void buttonExit_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        /// <summary>
        /// Opens the High score screen.
        /// </summary>
        private void buttonHighScore_Click(object sender, EventArgs e)
        {
            FormHighScores highScore = new FormHighScores();
            this.Hide();
            highScore.ShowDialog();
            this.Show();
        }

        /// <summary>
        /// Opens the Personal stats screen.
        /// </summary>
        private void buttonPersonalStatistics_Click(object sender, EventArgs e)
        {
            FormPersonalStatistics stats = new FormPersonalStatistics();
            this.Hide();
            stats.ShowDialog();
            this.Show();
        }

        /// <summary>
        ///  This function is a button that do logout.
        /// </summary>
        private void buttonLogout_Click(object sender, EventArgs e)
        {
            (int code, JObject j) = Requests.req(Codes.LOGOUT, "{}");

            if (code == Codes.ERROR)
            {
                this.labelError.Text = "logout failed.";
            }

            FormLogin login = new FormLogin();
            this.Hide();
            login.ShowDialog();
            this.Show();
        }

        /// <summary>
        ///  This function is a button that add question.
        /// </summary>
        private void buttonAddQuestion_Click(object sender, EventArgs e)
        {
            FormAddQuestion q = new FormAddQuestion();
            this.Hide();
            q.ShowDialog();
            this.Show();
        }
    }
}
