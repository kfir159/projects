﻿
namespace TriviaClient
{
    partial class FormHighScores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.labelThirdScore = new System.Windows.Forms.Label();
            this.labelThirdUsername = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelSecondScore = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelSecondUsername = new System.Windows.Forms.Label();
            this.labelFirstScore = new System.Windows.Forms.Label();
            this.labelFirstUsername = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelFifthUsername = new System.Windows.Forms.Label();
            this.labelFifthScore = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelFourthUsername = new System.Windows.Forms.Label();
            this.labelFourthScore = new System.Windows.Forms.Label();
            this.labelError = new System.Windows.Forms.Label();
            this.buttonBack = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F);
            this.label1.ForeColor = System.Drawing.Color.Gold;
            this.label1.Location = new System.Drawing.Point(376, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 37);
            this.label1.TabIndex = 0;
            this.label1.Text = "First";
            // 
            // labelThirdScore
            // 
            this.labelThirdScore.AutoSize = true;
            this.labelThirdScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelThirdScore.ForeColor = System.Drawing.Color.White;
            this.labelThirdScore.Location = new System.Drawing.Point(560, 246);
            this.labelThirdScore.Name = "labelThirdScore";
            this.labelThirdScore.Size = new System.Drawing.Size(0, 18);
            this.labelThirdScore.TabIndex = 1;
            // 
            // labelThirdUsername
            // 
            this.labelThirdUsername.AutoSize = true;
            this.labelThirdUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelThirdUsername.ForeColor = System.Drawing.Color.White;
            this.labelThirdUsername.Location = new System.Drawing.Point(494, 246);
            this.labelThirdUsername.Name = "labelThirdUsername";
            this.labelThirdUsername.Size = new System.Drawing.Size(0, 18);
            this.labelThirdUsername.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.label4.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.label4.Location = new System.Drawing.Point(530, 209);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 22);
            this.label4.TabIndex = 3;
            this.label4.Text = "Third";
            // 
            // labelSecondScore
            // 
            this.labelSecondScore.AutoSize = true;
            this.labelSecondScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelSecondScore.ForeColor = System.Drawing.Color.White;
            this.labelSecondScore.Location = new System.Drawing.Point(250, 222);
            this.labelSecondScore.Name = "labelSecondScore";
            this.labelSecondScore.Size = new System.Drawing.Size(0, 18);
            this.labelSecondScore.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.label6.ForeColor = System.Drawing.Color.Silver;
            this.label6.Location = new System.Drawing.Point(204, 181);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 26);
            this.label6.TabIndex = 11;
            this.label6.Text = "Second";
            // 
            // labelSecondUsername
            // 
            this.labelSecondUsername.AutoSize = true;
            this.labelSecondUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelSecondUsername.ForeColor = System.Drawing.Color.White;
            this.labelSecondUsername.Location = new System.Drawing.Point(184, 222);
            this.labelSecondUsername.Name = "labelSecondUsername";
            this.labelSecondUsername.Size = new System.Drawing.Size(0, 18);
            this.labelSecondUsername.TabIndex = 12;
            // 
            // labelFirstScore
            // 
            this.labelFirstScore.AutoSize = true;
            this.labelFirstScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.labelFirstScore.ForeColor = System.Drawing.Color.White;
            this.labelFirstScore.Location = new System.Drawing.Point(422, 110);
            this.labelFirstScore.Name = "labelFirstScore";
            this.labelFirstScore.Size = new System.Drawing.Size(0, 25);
            this.labelFirstScore.TabIndex = 14;
            // 
            // labelFirstUsername
            // 
            this.labelFirstUsername.AutoSize = true;
            this.labelFirstUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.labelFirstUsername.ForeColor = System.Drawing.Color.White;
            this.labelFirstUsername.Location = new System.Drawing.Point(331, 110);
            this.labelFirstUsername.Name = "labelFirstUsername";
            this.labelFirstUsername.Size = new System.Drawing.Size(0, 25);
            this.labelFirstUsername.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(154, 321);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 18);
            this.label2.TabIndex = 16;
            this.label2.Text = "Fourth";
            // 
            // labelFifthUsername
            // 
            this.labelFifthUsername.AutoSize = true;
            this.labelFifthUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelFifthUsername.ForeColor = System.Drawing.Color.White;
            this.labelFifthUsername.Location = new System.Drawing.Point(589, 355);
            this.labelFifthUsername.Name = "labelFifthUsername";
            this.labelFifthUsername.Size = new System.Drawing.Size(0, 18);
            this.labelFifthUsername.TabIndex = 17;
            // 
            // labelFifthScore
            // 
            this.labelFifthScore.AutoSize = true;
            this.labelFifthScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelFifthScore.ForeColor = System.Drawing.Color.White;
            this.labelFifthScore.Location = new System.Drawing.Point(655, 355);
            this.labelFifthScore.Name = "labelFifthScore";
            this.labelFifthScore.Size = new System.Drawing.Size(0, 18);
            this.labelFifthScore.TabIndex = 18;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(624, 321);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 18);
            this.label7.TabIndex = 19;
            this.label7.Text = "Fifth";
            // 
            // labelFourthUsername
            // 
            this.labelFourthUsername.AutoSize = true;
            this.labelFourthUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelFourthUsername.ForeColor = System.Drawing.Color.White;
            this.labelFourthUsername.Location = new System.Drawing.Point(118, 355);
            this.labelFourthUsername.Name = "labelFourthUsername";
            this.labelFourthUsername.Size = new System.Drawing.Size(0, 18);
            this.labelFourthUsername.TabIndex = 20;
            // 
            // labelFourthScore
            // 
            this.labelFourthScore.AutoSize = true;
            this.labelFourthScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelFourthScore.ForeColor = System.Drawing.Color.White;
            this.labelFourthScore.Location = new System.Drawing.Point(184, 355);
            this.labelFourthScore.Name = "labelFourthScore";
            this.labelFourthScore.Size = new System.Drawing.Size(0, 18);
            this.labelFourthScore.TabIndex = 21;
            // 
            // labelError
            // 
            this.labelError.AutoSize = true;
            this.labelError.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelError.ForeColor = System.Drawing.Color.IndianRed;
            this.labelError.Location = new System.Drawing.Point(380, 267);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(40, 18);
            this.labelError.TabIndex = 22;
            this.labelError.Text = "dsda";
            this.labelError.Visible = false;
            // 
            // buttonBack
            // 
            this.buttonBack.BackgroundImage = global::TriviaClient.Properties.Resources.backArrorButton1;
            this.buttonBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonBack.FlatAppearance.BorderSize = 0;
            this.buttonBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBack.Location = new System.Drawing.Point(2, 1);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(62, 57);
            this.buttonBack.TabIndex = 26;
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // FormHighScores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(33)))), ((int)(((byte)(36)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonBack);
            this.Controls.Add(this.labelError);
            this.Controls.Add(this.labelFourthScore);
            this.Controls.Add(this.labelFourthUsername);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.labelFifthScore);
            this.Controls.Add(this.labelFifthUsername);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelFirstUsername);
            this.Controls.Add(this.labelFirstScore);
            this.Controls.Add(this.labelSecondUsername);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.labelSecondScore);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.labelThirdUsername);
            this.Controls.Add(this.labelThirdScore);
            this.Controls.Add(this.label1);
            this.Name = "FormHighScores";
            this.Text = "FormHighScores";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelThirdScore;
        private System.Windows.Forms.Label labelThirdUsername;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelSecondScore;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelSecondUsername;
        private System.Windows.Forms.Label labelFirstScore;
        private System.Windows.Forms.Label labelFirstUsername;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelFifthUsername;
        private System.Windows.Forms.Label labelFifthScore;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelFourthUsername;
        private System.Windows.Forms.Label labelFourthScore;
        private System.Windows.Forms.Label labelError;
        private System.Windows.Forms.Button buttonBack;
    }
}