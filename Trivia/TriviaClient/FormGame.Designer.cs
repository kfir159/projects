﻿
namespace TriviaClient
{
    partial class FormGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelQuestion = new System.Windows.Forms.Label();
            this.buttonAns1 = new System.Windows.Forms.Button();
            this.buttonAns2 = new System.Windows.Forms.Button();
            this.buttonAns3 = new System.Windows.Forms.Button();
            this.buttonAns4 = new System.Windows.Forms.Button();
            this.labelQuestionsLeft = new System.Windows.Forms.Label();
            this.labelCurrectAns = new System.Windows.Forms.Label();
            this.labelTime = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.labelError = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelQuestion
            // 
            this.labelQuestion.AutoSize = true;
            this.labelQuestion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.labelQuestion.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelQuestion.Location = new System.Drawing.Point(255, 87);
            this.labelQuestion.Name = "labelQuestion";
            this.labelQuestion.Size = new System.Drawing.Size(71, 17);
            this.labelQuestion.TabIndex = 0;
            this.labelQuestion.Text = "Loading...";
            // 
            // buttonAns1
            // 
            this.buttonAns1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.buttonAns1.FlatAppearance.BorderSize = 0;
            this.buttonAns1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAns1.Location = new System.Drawing.Point(168, 159);
            this.buttonAns1.Name = "buttonAns1";
            this.buttonAns1.Size = new System.Drawing.Size(130, 59);
            this.buttonAns1.TabIndex = 1;
            this.buttonAns1.Text = "Loading...";
            this.buttonAns1.UseVisualStyleBackColor = false;
            this.buttonAns1.Click += new System.EventHandler(this.buttonAns1_Click);
            // 
            // buttonAns2
            // 
            this.buttonAns2.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.buttonAns2.FlatAppearance.BorderSize = 0;
            this.buttonAns2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAns2.Location = new System.Drawing.Point(474, 159);
            this.buttonAns2.Name = "buttonAns2";
            this.buttonAns2.Size = new System.Drawing.Size(130, 59);
            this.buttonAns2.TabIndex = 2;
            this.buttonAns2.Text = "Loading...";
            this.buttonAns2.UseVisualStyleBackColor = false;
            this.buttonAns2.Click += new System.EventHandler(this.buttonAns2_Click);
            // 
            // buttonAns3
            // 
            this.buttonAns3.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.buttonAns3.FlatAppearance.BorderSize = 0;
            this.buttonAns3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAns3.Location = new System.Drawing.Point(168, 254);
            this.buttonAns3.Name = "buttonAns3";
            this.buttonAns3.Size = new System.Drawing.Size(130, 59);
            this.buttonAns3.TabIndex = 3;
            this.buttonAns3.Text = "Loading...";
            this.buttonAns3.UseVisualStyleBackColor = false;
            this.buttonAns3.Click += new System.EventHandler(this.buttonAns3_Click);
            // 
            // buttonAns4
            // 
            this.buttonAns4.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.buttonAns4.FlatAppearance.BorderSize = 0;
            this.buttonAns4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAns4.Location = new System.Drawing.Point(474, 254);
            this.buttonAns4.Name = "buttonAns4";
            this.buttonAns4.Size = new System.Drawing.Size(130, 59);
            this.buttonAns4.TabIndex = 4;
            this.buttonAns4.Text = "Loading...";
            this.buttonAns4.UseVisualStyleBackColor = false;
            this.buttonAns4.Click += new System.EventHandler(this.buttonAns4_Click);
            // 
            // labelQuestionsLeft
            // 
            this.labelQuestionsLeft.AutoSize = true;
            this.labelQuestionsLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.labelQuestionsLeft.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelQuestionsLeft.Location = new System.Drawing.Point(697, 45);
            this.labelQuestionsLeft.Name = "labelQuestionsLeft";
            this.labelQuestionsLeft.Size = new System.Drawing.Size(71, 17);
            this.labelQuestionsLeft.TabIndex = 5;
            this.labelQuestionsLeft.Text = "Loading...";
            // 
            // labelCurrectAns
            // 
            this.labelCurrectAns.AutoSize = true;
            this.labelCurrectAns.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.labelCurrectAns.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelCurrectAns.Location = new System.Drawing.Point(697, 109);
            this.labelCurrectAns.Name = "labelCurrectAns";
            this.labelCurrectAns.Size = new System.Drawing.Size(71, 17);
            this.labelCurrectAns.TabIndex = 6;
            this.labelCurrectAns.Text = "Loading...";
            // 
            // labelTime
            // 
            this.labelTime.AutoSize = true;
            this.labelTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelTime.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelTime.Location = new System.Drawing.Point(63, 45);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(72, 18);
            this.labelTime.TabIndex = 7;
            this.labelTime.Text = "Loading...";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // labelError
            // 
            this.labelError.AutoSize = true;
            this.labelError.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelError.ForeColor = System.Drawing.Color.IndianRed;
            this.labelError.Location = new System.Drawing.Point(274, 359);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(262, 18);
            this.labelError.TabIndex = 10;
            this.labelError.Text = "your password or username is wrong. ";
            this.labelError.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label1.Location = new System.Drawing.Point(559, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 17);
            this.label1.TabIndex = 11;
            this.label1.Text = "Questions left: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label2.Location = new System.Drawing.Point(559, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 17);
            this.label2.TabIndex = 12;
            this.label2.Text = "Correct answers:";
            // 
            // FormGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(33)))), ((int)(((byte)(36)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelError);
            this.Controls.Add(this.labelTime);
            this.Controls.Add(this.labelCurrectAns);
            this.Controls.Add(this.labelQuestionsLeft);
            this.Controls.Add(this.buttonAns4);
            this.Controls.Add(this.buttonAns3);
            this.Controls.Add(this.buttonAns2);
            this.Controls.Add(this.buttonAns1);
            this.Controls.Add(this.labelQuestion);
            this.Name = "FormGame";
            this.Text = "FormGame";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelQuestion;
        private System.Windows.Forms.Button buttonAns1;
        private System.Windows.Forms.Button buttonAns2;
        private System.Windows.Forms.Button buttonAns3;
        private System.Windows.Forms.Button buttonAns4;
        private System.Windows.Forms.Label labelQuestionsLeft;
        private System.Windows.Forms.Label labelCurrectAns;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label labelError;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}