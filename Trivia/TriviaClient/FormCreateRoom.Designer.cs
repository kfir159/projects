﻿
namespace TriviaClient
{
    partial class FormCreateRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelRoomName = new System.Windows.Forms.Label();
            this.textBoxRoomName = new System.Windows.Forms.TextBox();
            this.textBoxMaxPlayers = new System.Windows.Forms.TextBox();
            this.textBoxTime = new System.Windows.Forms.TextBox();
            this.buttonCreateRoom = new System.Windows.Forms.Button();
            this.textBoxQuestions = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelError = new System.Windows.Forms.Label();
            this.buttonBack = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelRoomName
            // 
            this.labelRoomName.AutoSize = true;
            this.labelRoomName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(33)))), ((int)(((byte)(36)))));
            this.labelRoomName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelRoomName.ForeColor = System.Drawing.Color.White;
            this.labelRoomName.Location = new System.Drawing.Point(129, 118);
            this.labelRoomName.Name = "labelRoomName";
            this.labelRoomName.Size = new System.Drawing.Size(98, 18);
            this.labelRoomName.TabIndex = 0;
            this.labelRoomName.Text = "Room Name:";
            // 
            // textBoxRoomName
            // 
            this.textBoxRoomName.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.textBoxRoomName.Location = new System.Drawing.Point(236, 118);
            this.textBoxRoomName.Name = "textBoxRoomName";
            this.textBoxRoomName.Size = new System.Drawing.Size(126, 20);
            this.textBoxRoomName.TabIndex = 3;
            // 
            // textBoxMaxPlayers
            // 
            this.textBoxMaxPlayers.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.textBoxMaxPlayers.Location = new System.Drawing.Point(236, 190);
            this.textBoxMaxPlayers.Name = "textBoxMaxPlayers";
            this.textBoxMaxPlayers.Size = new System.Drawing.Size(126, 20);
            this.textBoxMaxPlayers.TabIndex = 4;
            // 
            // textBoxTime
            // 
            this.textBoxTime.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.textBoxTime.Location = new System.Drawing.Point(538, 117);
            this.textBoxTime.Name = "textBoxTime";
            this.textBoxTime.Size = new System.Drawing.Size(117, 20);
            this.textBoxTime.TabIndex = 5;
            // 
            // buttonCreateRoom
            // 
            this.buttonCreateRoom.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.buttonCreateRoom.FlatAppearance.BorderSize = 0;
            this.buttonCreateRoom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCreateRoom.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F);
            this.buttonCreateRoom.Location = new System.Drawing.Point(332, 268);
            this.buttonCreateRoom.Name = "buttonCreateRoom";
            this.buttonCreateRoom.Size = new System.Drawing.Size(133, 77);
            this.buttonCreateRoom.TabIndex = 6;
            this.buttonCreateRoom.Text = "Create Room";
            this.buttonCreateRoom.UseVisualStyleBackColor = false;
            this.buttonCreateRoom.Click += new System.EventHandler(this.buttonCreateRoom_Click);
            // 
            // textBoxQuestions
            // 
            this.textBoxQuestions.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.textBoxQuestions.Location = new System.Drawing.Point(538, 190);
            this.textBoxQuestions.Name = "textBoxQuestions";
            this.textBoxQuestions.Size = new System.Drawing.Size(117, 20);
            this.textBoxQuestions.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(33)))), ((int)(((byte)(36)))));
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(444, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 18);
            this.label2.TabIndex = 9;
            this.label2.Text = "Time:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(33)))), ((int)(((byte)(36)))));
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(129, 192);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 18);
            this.label3.TabIndex = 10;
            this.label3.Text = "Max players:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(33)))), ((int)(((byte)(36)))));
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(445, 190);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 18);
            this.label4.TabIndex = 11;
            this.label4.Text = "Questions:";
            // 
            // labelError
            // 
            this.labelError.AutoSize = true;
            this.labelError.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.labelError.ForeColor = System.Drawing.Color.IndianRed;
            this.labelError.Location = new System.Drawing.Point(375, 231);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(40, 18);
            this.labelError.TabIndex = 12;
            this.labelError.Text = "dsda";
            this.labelError.Visible = false;
            // 
            // buttonBack
            // 
            this.buttonBack.BackgroundImage = global::TriviaClient.Properties.Resources.backArrorButton1;
            this.buttonBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonBack.FlatAppearance.BorderSize = 0;
            this.buttonBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBack.Location = new System.Drawing.Point(2, 2);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(62, 57);
            this.buttonBack.TabIndex = 26;
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // FormCreateRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(33)))), ((int)(((byte)(36)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonBack);
            this.Controls.Add(this.labelError);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxQuestions);
            this.Controls.Add(this.buttonCreateRoom);
            this.Controls.Add(this.textBoxTime);
            this.Controls.Add(this.textBoxMaxPlayers);
            this.Controls.Add(this.textBoxRoomName);
            this.Controls.Add(this.labelRoomName);
            this.Name = "FormCreateRoom";
            this.Text = "FormCreateRoom";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelRoomName;
        private System.Windows.Forms.TextBox textBoxRoomName;
        private System.Windows.Forms.TextBox textBoxMaxPlayers;
        private System.Windows.Forms.TextBox textBoxTime;
        private System.Windows.Forms.Button buttonCreateRoom;
        private System.Windows.Forms.TextBox textBoxQuestions;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelError;
        private System.Windows.Forms.Button buttonBack;
    }
}