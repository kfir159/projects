﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;

namespace TriviaClient
{
    class Communicator
    {
        private NetworkStream _clientStream;

        public Communicator(int port)
        {
            try
            {
                TcpClient client = new TcpClient();
                IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), port);
                client.Connect(serverEndPoint);
                NetworkStream clientStream = client.GetStream();
                this._clientStream = clientStream;
            }
            catch
            {
                Console.WriteLine("Error with connect");
            }
        }

        /// <summary>
        /// This function Send to the server.
        /// </summary>
        /// <param name="data"></param>
        public void Send(byte[] data)
        {
            this._clientStream.Write(data, 0, data.Length);
            this._clientStream.Flush();
        }
        
        /// <summary>
        /// This function recving data.
        /// </summary>
        /// <returns>The code and the data.</returns>
        public (int, string) Recv()
        {
            byte[] buff = new byte[8]; // 1 byte for the code part
            this._clientStream.Read(buff, 0, buff.Length);
            Console.WriteLine((System.Text.Encoding.ASCII.GetString(buff)));
            int code = Convert.ToInt32(System.Text.Encoding.ASCII.GetString(buff), 2);

            byte[] lenBuff = new byte[4*8]; // 4 bytes for the code part
            this._clientStream.Read(lenBuff, 0, lenBuff.Length);
            Console.WriteLine(System.Text.Encoding.ASCII.GetString(lenBuff));
            int len = Convert.ToInt32(System.Text.Encoding.ASCII.GetString(lenBuff), 2);

            byte[] dataBuff = new byte[len*8];
            this._clientStream.Read(dataBuff, 0, dataBuff.Length);
            string str = System.Text.Encoding.ASCII.GetString(dataBuff);
            Console.WriteLine(str);

            return (code, BinaryToString(str));
        }

        /// <summary>
        /// This function ia a helper that convert string binary to string text.
        /// </summary>
        /// <param name="data"> - the binary string.</param>
        /// <returns>The string text.</returns>
        private static string BinaryToString(string data)
        {
            List<Byte> byteList = new List<Byte>();

            for (int i = 0; i < data.Length; i += 8)
            {
                byteList.Add(Convert.ToByte(data.Substring(i, 8), 2));
            }
            return Encoding.ASCII.GetString(byteList.ToArray());
        }
    }
}
