﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;

namespace TriviaClient
{
    public partial class FormCreateRoom : Form
    {
        public FormCreateRoom()
        {
            InitializeComponent();
            this.Text = "Create Room Screen";
        }

        /// <summary>
        /// Sends the create room request to the server
        /// </summary>
        private void buttonCreateRoom_Click(object sender, EventArgs e)
        {
            Dictionary<string, object> dict = new Dictionary<string, object> {
                { "name", this.textBoxRoomName.Text },
                { "maxUsers", Int32.Parse(this.textBoxMaxPlayers.Text) },
                { "questionCount", Int32.Parse(this.textBoxQuestions.Text) },
                { "answerTimeout", Int32.Parse(this.textBoxTime.Text) }
            };

            (int code, JObject j) = Requests.req(Codes.CREATE_ROOM, dict);

            if (code == Codes.ERROR)
            {
                this.labelError.Text = "create room failed.";
                return;
            }

            this.Hide();
            FormViewRoom view = new FormViewRoom(this.textBoxRoomName.Text, Int32.Parse(j["roomId"].ToString()));
            view.ShowDialog();
            this.Show();
        }

        // Closes the program on click
        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
