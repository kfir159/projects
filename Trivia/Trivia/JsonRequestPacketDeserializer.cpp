#include "JsonRequestPacketDeserializer.h"

LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(std::string buffer)
{
    json j = json::parse(Helper::binaryToString(buffer));
    return LoginRequest { j["username"], j["password"] };
}

SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(std::string buffer)
{
    json j = json::parse(Helper::binaryToString(buffer));
    return SignupRequest { j["username"], j["password"], j["email"] };
}

GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersRequest(std::string buffer)
{
    json j = json::parse((Helper::binaryToString(buffer)));
    return GetPlayersInRoomRequest { j["roomId"] };
}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(std::string buffer)
{
    json j = json::parse((Helper::binaryToString(buffer)));
    return JoinRoomRequest { j["roomId"] };
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(std::string buffer)
{
    json j = json::parse((Helper::binaryToString(buffer)));
    return CreateRoomRequest { j["name"], j["maxUsers"], j["questionCount"], j["answerTimeout"] };
}

SubmitAnswerRequest JsonRequestPacketDeserializer::deserializerSubmitAnswerRequest(std::string buffer)
{
    json j = json::parse(Helper::binaryToString(buffer));
    return SubmitAnswerRequest{ j["id"] };
}

AddQuestionRequest JsonRequestPacketDeserializer::deserializerAddQuestionRequest(std::string buffer)
{
    json j = json::parse(Helper::binaryToString(buffer));
    return AddQuestionRequest{ j["question"], j["wrongAns1"], j["wrongAns2"], j["wrongAns3"], j["correctAns"] };
}