#pragma once

#include <vector>
#include <iostream>
#include <map>
#include "IDatabase.h"
#include "Room.h"
//#include <ctime>


class RequestHandlerFactory;

struct GameData
{
	Question currQuestion;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int avgAnswerTime;
};

class Game
{
public:
	Game(std::vector<Question> questions, std::vector<User> users, unsigned int roomId);
	Question getQuestienForUser(User user);
	void submitAnswerForUser(User user, std::string answer);
	void removePlayer(User user);
	GameData getGameData(User user);
	unsigned int getRoomId();

private:
	std::vector<Question> _questions;
	unsigned int _roomId;
	std::map<User, GameData> _players;
};

class GameManager
{
public:
	GameManager(IDatabase* db);
	Game createGame(Room room);
	void deleteGame(Game game);

private:
	IDatabase* _db;
	std::vector<Game> _games;
};


