#pragma once

#include "IRequestHandler.h"
#include "LoginManager.h"
#include "RequestHandlerFactory.h"
#include "Sqlitedatabase.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "MenuRequestHandler.h"
#include "Requests.h"
#include "Responses.h"
#include "StatisticsManager.h"

class RequestHandlerFactory;

using namespace JsonRequestPacketDeserializer;
using namespace JsonResponsePacketSerializer;

class LoginRequestHandler : public IRequestHandler
{
public:
	/// <summary>
	/// C'tor for the LoginRequestHandler class.
	/// </summary>
	LoginRequestHandler(IDatabase* db, RequestHandlerFactory* factory);
	/// <summary>
	/// Checks if the request is a valid login/signup request
	/// </summary>
	/// <param name="req">A RequestInfo struct containg data from client</param>
	/// <returns>bool</returns>
	virtual bool isRequestRelevant(RequestInfo req);
	/// <summary>
	/// Handles the request sent from the client, reads the buffer, responds and calls the next handler
	/// </summary>
	/// <param name="req">A RequestInfo struct containg data from client</param>
	/// <returns>RequestResult struct</returns>
	virtual RequestResult handleRequest(RequestInfo req);

private:
	/// <summary>
	/// Creates a new Requset result struct for the given requestInfo
	/// </summary>
	/// <param name="req"> - the request info.</param>
	/// <returns>The request result.</returns>
	RequestResult _login(RequestInfo req);
	RequestResult _signup(RequestInfo req);

	LoginManager _loginManager;
	RequestHandlerFactory* _handleFactory;
};

