#pragma once

#include "IDatabase.h"
#include <vector>
#include <string>
#include <algorithm>
#include <iostream>

class StatisticsManager
{
public:
	/// <summary>
	/// C'tor for StatisticsManager. 
	/// </summary>
	/// <param name="db"> - given db</param>
	StatisticsManager(IDatabase* db);
	StatisticsManager() = default;
	/// <summary>
	/// Getting the 5 best rated users on the db. 
	/// </summary>
	/// <returns>Vector of the 5 best players (names)</returns>
	std::vector<std::pair<std::string, int>> getTopFiveUsers();
	/// <summary>
	/// Getting the stats of a user by a given username.
	/// </summary>
	/// <param name="username"> - an username</param>
	/// <returns>Vector of the stats</returns>
	std::vector<std::string> getUserStatistics(std::string username);

private:
	IDatabase* _db;
};