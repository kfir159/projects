#pragma once

#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"

class RequestHandlerFactory;

using namespace JsonRequestPacketDeserializer;
using namespace JsonResponsePacketSerializer;

class RoomMemberRequestHandler : public IRequestHandler
{
public:
	RoomMemberRequestHandler(Room room, User user, RoomManager* roomManager, RequestHandlerFactory* factory);
	/// <summary>
	/// Checks if the request is a valid login/signup request
	/// </summary>
	/// <param name="req">A RequestInfo struct containg data from client</param>
	/// <returns>bool</returns>
	virtual bool isRequestRelevant(RequestInfo req);
	/// <summary>
	/// Handles the request sent from the client, reads the buffer, responds and calls the next handler
	/// </summary>
	/// <param name="req">A RequestInfo struct containg data from client</param>
	/// <returns>RequestResult struct</returns>
	virtual RequestResult handleRequest(RequestInfo req);

protected:
	Room _room;
	User _user;
	RoomManager* _roomManager;
	RequestHandlerFactory* _handlerFactory;

	/// <summary>
	/// Creates a new Requset result struct for the given requestInfo
	/// </summary>
	/// <param name="req"> - the request info.</param>
	/// <returns>The request result.</returns>
	RequestResult _leaveRoom(RequestInfo req);
	RequestResult _startGame(RequestInfo req);
	RequestResult _getRoomState(RequestInfo req);
};

class RoomAdminRequestHandler : public RoomMemberRequestHandler
{
public:
	RoomAdminRequestHandler(Room room, User user, RoomManager* roomManager, RequestHandlerFactory* factory);
	/// <summary>
	/// Checks if the request is a valid login/signup request
	/// </summary>
	/// <param name="req">A RequestInfo struct containg data from client</param>
	/// <returns>bool</returns>
	virtual bool isRequestRelevant(RequestInfo req);
	/// <summary>
	/// Handles the request sent from the client, reads the buffer, responds and calls the next handler
	/// </summary>
	/// <param name="req">A RequestInfo struct containg data from client</param>
	/// <returns>RequestResult struct</returns>
	virtual RequestResult handleRequest(RequestInfo req);

private:
	/// <summary>
	/// Creates a new Requset result struct for the given requestInfo
	/// </summary>
	/// <param name="req"> - the request info.</param>
	/// <returns>The request result.</returns>
	RequestResult _closeRoom(RequestInfo req);
	RequestResult _askStartGame(RequestInfo req);
};
