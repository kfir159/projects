#include "GameRequestHandler.h"

GameRequestHandler::GameRequestHandler(Game* game, User user, GameManager* gameManager, RequestHandlerFactory* handlerFactory)
{
    this->_game = game;
    this->_user = user;
    this->_gameManger = gameManager;
    this->_factory = handlerFactory;
}

bool GameRequestHandler::isRequestRelevant(RequestInfo req)
{
    return req.id >= CODE_GET_RESULT && req.id <= CODE_LEAVE_GAME || req.id == CODE_GET_STATE;
}

RequestResult GameRequestHandler::handleRequest(RequestInfo req)
{
	switch (req.id)
	{
	case CODE_GET_STATE:
		return this->_getRoomState(req);
	case CODE_GET_RESULT:
		return this->_getGameResults(req);
	case CODE_SUBMIT_ANSWER:
		return this->_submitAnswer(req);
	case CODE_GET_QUESTION:
		return this->_getQuestion(req);
	case CODE_LEAVE_GAME:
		return this->_leaveGame(req);
	default:
		return RequestResult{ serializeResponse(ErrorResponse{"general error"}), nullptr };
	}
}

RequestResult GameRequestHandler::_getQuestion(RequestInfo req)
{
	auto question = this->_game->getQuestienForUser(this->_user);
	std::map<unsigned int, std::string> m = {
		{1, question.answers[0]},
		{2, question.answers[1]},
		{3, question.answers[2]},
		{4, question.answers[3]} 
	};

	return RequestResult{ serializeResponse(GetQuestionResponse{ CODE_GET_QUESTION, question.question, m }), this };
}

RequestResult GameRequestHandler::_submitAnswer(RequestInfo req)
{
	SubmitAnswerRequest sar = deserializerSubmitAnswerRequest(req.buffer);

	if (sar.answerId == 5)
		this->_game->submitAnswerForUser(this->_user, "");
	else
		this->_game->submitAnswerForUser(this->_user, this->_game->getGameData(this->_user).currQuestion.answers[sar.answerId-1]);
	return RequestResult{ serializeResponse(SubmitAnswerResponse{ CODE_SUBMIT_ANSWER, 4 }), this };
}

RequestResult GameRequestHandler::_getGameResults(RequestInfo req)
{
	std::vector<PlayerResults> results;
	#define ITER(X) for (auto it = X.begin(); it != X.end(); ++it)

	ITER(this->_factory->getRoomManager()->getRoomById(this->_game->getRoomId()).getUsers()) 
	{
		GameData data = this->_game->getGameData(*it);
		results.push_back(PlayerResults{ it->username, data.correctAnswerCount, data.wrongAnswerCount, data.avgAnswerTime });
	}
	return RequestResult{ serializeResponse(GetGameResultsResponse{ CODE_GET_RESULT, results }), this };
}

RequestResult GameRequestHandler::_leaveGame(RequestInfo req)
{
	this->_game->removePlayer(this->_user);
	
	return RequestResult{ serializeResponse(LeaveGameResponse{CODE_LEAVE_GAME}), this};
}

RequestResult GameRequestHandler::_getRoomState(RequestInfo req)
{
    return RequestResult{ serializeResponse(GetRoomStateResponse {
        (unsigned int)req.id,
		this->_factory->getRoomManager()->getRoomById(this->_game->getRoomId()).getMetadata().isActive,
		this->_factory->getRoomManager()->getRoomById(this->_game->getRoomId()).getAllUsers(),
		this->_factory->getRoomManager()->getRoomById(this->_game->getRoomId()).getMetadata().sumOfQuestionsInGame,
        this->_factory->getRoomManager()->getRoomById(this->_game->getRoomId()).getMetadata().timePerQuestion } ),
        this
    };
}
