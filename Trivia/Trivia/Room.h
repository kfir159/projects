#pragma once
#include "Sqlitedatabase.h"
#include <vector>

struct RoomData
{
	/// <summary>
	/// This function is a c'tor of RoomData.
	/// </summary>
	/// <param name="id"> - the id of the room.</param>
	/// <param name="name"> - the name of the room.</param>
	/// <param name="maxPlayers"> - the max players that can to play in the room.</param>
	/// <param name="sumOfQuestionsInGame"> - the sum of the qustion in the game.</param>
	/// <param name="timePerQuestion"> - the time that have to every question.</param>
	/// <param name="isActive"> - if the room active.</param>
	RoomData(unsigned int id, std::string name, unsigned int maxPlayers, 
		unsigned int sumOfQuestionsInGame,
		unsigned int timePerQuestion, unsigned int isActive)
	{
		this->id = id;
		this->name = name;
		this->maxPlayers = maxPlayers;
		this->sumOfQuestionsInGame = sumOfQuestionsInGame;
		this->timePerQuestion = timePerQuestion;
		this->isActive = isActive;
	}
	
	/// <summary>
	/// This function is a copy c'tor of RoomData.
	/// </summary>
	/// <param name="data"> - the else RoomData.</param>
	RoomData(const RoomData &data)
	{
		this->id = data.id;
		this->name = data.name;
		this->maxPlayers = data.maxPlayers;
		this->sumOfQuestionsInGame = data.sumOfQuestionsInGame;
		this->timePerQuestion = data.timePerQuestion;
		this->isActive = data.isActive;
	}

	RoomData() = default;

	unsigned int id;
	std::string name;
	unsigned int maxPlayers;
	unsigned int sumOfQuestionsInGame;
	unsigned int timePerQuestion;
	unsigned int isActive;
};

class Room
{
public:
	/// <summary>
	/// This function is a c'tor of Room.
	/// </summary>
	/// <param name="data"> - the data of the room.</param>
	Room(RoomData data);
	Room() = default;
	/// <summary>
	/// This function add user to the room.
	/// </summary>
	/// <param name="user"> - the user.</param>
	void addUser(User user);
	/// <summary>
	/// This functio remove user from the room.
	/// </summary>
	/// <param name="user"> - the user.</param>
	void removeUser(User user);
	/// <summary>
	/// This function return the all names of users.
	/// </summary>
	/// <returns>Vector with the all names of the users.</returns>
	std::vector<std::string> getAllUsers();
	/// <summary>
	/// This function return the data of the room.
	/// </summary>
	/// <returns>The data of the room.</returns>
	RoomData getMetadata();
	/// <summary>
	/// This function set the active of room.
	/// </summary>
	/// <param name="active"> - the new active.</param>
	void setRoomActive(unsigned int active);

	std::vector<User> getUsers();

private:
	RoomData _metadata;
	std::vector<User> _users;
};
