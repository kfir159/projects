#include "Room.h"

Room::Room(RoomData data)
{
	this->_metadata = RoomData(data);
}

void Room::addUser(User user)
{
	this->_users.push_back(user);
}

void Room::removeUser(User user)
{
	for (auto it = this->_users.begin(); it != this->_users.end(); ++it)
	{
		if (*it == user)
		{
			this->_users.erase(it);
			break;
		}
	}
}

std::vector<std::string> Room::getAllUsers()
{
	std::vector<std::string> strVec;

	for (auto it: this->_users) 
	{
		strVec.push_back(it.username);
	}

	return strVec;
}

RoomData Room::getMetadata()
{
	return this->_metadata;
}

void Room::setRoomActive(unsigned int active)
{
	this->_metadata.isActive = active;
}

std::vector<User> Room::getUsers()
{
	return this->_users;
}
