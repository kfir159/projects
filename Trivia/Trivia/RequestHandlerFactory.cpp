#include "RequestHandlerFactory.h"

RequestHandlerFactory::RequestHandlerFactory(IDatabase* db, LoginManager loginManager, StatisticsManager statisticsManager) : _db(db), _loginManager(loginManager), _statisticsManager(statisticsManager)
{
}

RequestHandlerFactory::RequestHandlerFactory()
{
	this->_db = new Sqlitedatabase();
	this->_loginManager = LoginManager(this->_db);
	this->_statisticsManager = StatisticsManager(this->_db);
	this->_roomManager = new RoomManager();
	this->_gameManager = new GameManager(this->_db);
	this->_db->open();
}

RequestHandlerFactory::~RequestHandlerFactory()
{
	this->_db->close();
	delete this->_db;
}

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
	return new LoginRequestHandler(this->_db, this);
}

LoginManager RequestHandlerFactory::getLoginManager()
{
	return this->_loginManager;
}

GameManager* RequestHandlerFactory::getGameManager()
{
	return this->_gameManager;
}

IDatabase* RequestHandlerFactory::getDb()
{
	return this->_db;
}

MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(User user)
{
	return new MenuRequestHandler(user, this);
}

RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(User user, Room room)
{
	return new RoomAdminRequestHandler(room, user, this->_roomManager, this);
}

RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(User user, Room room)
{
	return new RoomMemberRequestHandler(room, user, this->_roomManager, this);
}

GameRequestHandler* RequestHandlerFactory::createGameRequestHandler(Game* game, User user)
{
	return new GameRequestHandler(game, user, this->_gameManager, this);
}

StatisticsManager& RequestHandlerFactory::getStatisticsManager()
{
	return this->_statisticsManager;
}

RoomManager* RequestHandlerFactory::getRoomManager()
{
	return this->_roomManager;
}
