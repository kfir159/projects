#include "Helper.h"

#define CHECK_SOCKET(BUFF) if (res == SOCKET_ERROR || !strcmp(BUFF, "")) throw std::exception("socket died");

RequestInfo Helper::createRequestInfo(SOCKET sock)
{
    RequestInfo req;

    char code[BYTE_LEN] = { 0 };
    int res = recv(sock, code, BYTE_LEN, 0);
    CHECK_SOCKET(code);

    int intCode = std::stoi(code, nullptr, 2);

    char size[BYTE_LEN * 4] = { 0 };
    res = recv(sock, size, BYTE_LEN * 4, 0);
    CHECK_SOCKET(size);

    int intSize = std::stoi(size, nullptr, 2) * 8;

    char *data = new char[intSize];
    res = recv(sock, data, intSize, 0);
    CHECK_SOCKET(data);

    std::cout << "Input: " << Helper::binaryToString(data) << " code: " << intCode << std::endl;

    req.buffer = std::string(data); 
    req.id = intCode;
    req.receivalTime = NULL;

    delete[] data;

    return req;
}

RequestInfo Helper::createRequestInfo(std::string binaryBuffer)
{
    int id = Helper::binaryToDecimal(binaryBuffer.substr(0, BYTE_LEN));
    return RequestInfo{ id, time_t() , binaryBuffer.substr(BYTE_LEN * 5) };
}
    
std::string Helper::binaryToString(const std::string& binaryBuffer)
{
    std::stringstream sstream(binaryBuffer);
    std::string buffer;
    std::bitset<8> bits;
    char c;

    while (sstream.good())
    {
        sstream >> bits;
        c = char(bits.to_ulong());
        buffer += c;
    }

    buffer.pop_back();
    return buffer;
}

int Helper::binaryToDecimal(const std::string& n)
{
    int decValue = 0;

    int base = 1;

    int len = n.length();
    for (int i = len - 1; i >= 0; i--) 
    {
        if (n[i] == '1')
        {
            decValue += base;
        }
        base = base * 2;
    }

    return decValue;
}


std::string Helper::toBinary(const std::string& s)
{
	std::string newS = "";

	for (unsigned int i = 0; i < s.size(); i++)
	{
		// Converting from char to binary char
		newS += (std::bitset<8>(s.c_str()[i])).to_string();
	}

	return newS;
}

std::string Helper::toBinary(int n)
{
    return std::bitset<8>(n).to_string(); 
}

std::string Helper::padBinaryNumber(const std::string& s, const int size)
{
	std::string vec;
	std::ostringstream ostr;
	
	// filling zeros at the start
	ostr << std::setw(size) << std::setfill('0') << s;

	return ostr.str();
}

std::string Helper::vecToString(std::vector<std::string> resp)
{
    std::string temp = "";
    for(unsigned int i = 0 ; i < resp.size() ; i++)
    {
        temp += resp[i];
        if(resp.size() - i != 1)
        {
            temp += ',';
        }
    }

    return temp;
}

std::string Helper::vecToString(std::vector<RoomData> resp)
{
    std::string temp = "";
    for(unsigned int i = 0 ; i < resp.size() ; i++)
    {
        temp += std::to_string(resp[i].id) + '.' + std::to_string(resp[i].isActive) + '.'
            + std::to_string(resp[i].maxPlayers) + '.' + resp[i].name + '.'
            + std::to_string(resp[i].sumOfQuestionsInGame) + '.' + std::to_string(resp[i].timePerQuestion);

        if(resp.size() - i != 1)
        {
            temp += ',';
        }
    }

    return temp;
}

std::string Helper::vecToString(std::vector<std::pair<std::string, int>> resp)
{
    std::string temp = "";
    for (unsigned int i = 0; i < resp.size(); i++)
    {
        temp += resp[i].first + '.' + std::to_string(resp[i].second);
        if (resp.size() - i != 1)
        {
            temp += ',';
        }
    }

    return temp;
}

std::string Helper::vecToString(std::vector<PlayerResults> resp)
{
    std::string temp = "";

    for (auto it: resp)
    {
        temp = it.username + '.' + std::to_string(it.averageAnswerTime) + '.' +
            std::to_string(it.correctAnswerCount) + '.' + std::to_string(it.wrongAnswerCount) + ',';
    }

    return temp.substr(0, temp.length() - 1);
}

std::string Helper::vecToString(std::map<unsigned int, std::string> resp)
{
    std::string temp = "";
    for (auto it = resp.begin(); it != resp.end() ; ++it)
    {
        temp += std::to_string(it->first) + '.' + it->second + ',';
    }

    return temp.substr(0, temp.length() - 1);
}
