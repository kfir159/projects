﻿#include "Sqlitedatabase.h"

#define SQL_DEBUG true

#if SQL_DEBUG == true
#define EXEC_SQL_DEBUG_PRINT std::cout << "Sql error:" << std::endl << this->_sql << std::endl << this->_err << std::endl;
#else
#define EXEC_SQL_DEBUG_PRINT
#endif // !SQL_DEBUG

#define SQLITE_EXEC(callback, callbackData) {this->_res = sqlite3_exec(this->_db, this->_sql.c_str(), callback, callbackData, &this->_err); \
						if (this->_res != SQLITE_OK) EXEC_SQL_DEBUG_PRINT; }

bool Sqlitedatabase::open()
{
	int exists = _access(DB_PATH, 0);
	this->_res = sqlite3_open(DB_PATH, &this->_db);

	if (this->_res != SQLITE_OK) return false;

	if (exists != -1)
	{
		// db exists already no need to build it
		return false;
	}

	this->_sql	= "CREATE TABLE USERS( \
				ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, \
				UNAME	TEXT NOT NULL, \
				PWORD	TEXT NOT NULL, \
				EMAIL	TEXT NOT NULL);";

	SQLITE_EXEC(nullptr, nullptr);

	this->_sql = "CREATE TABLE QUESTIONS( \
				ID		INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, \
				QUESTION_STR TEXT NOT NULL, \
				W_ANS1	TEXT NOT NULL, \
				W_ANS2	TEXT NOT NULL, \
				W_ANS3	TEXT NOT NULL, \
				C_ANS	TEXT NOT NULL);";

	SQLITE_EXEC(nullptr, nullptr);

	this->addDefaultQuestions();

	this->_sql = "CREATE TABLE STATS( \
				ID		INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, \
                USER_ID INTEGER NOT NULL, \
                SUM     INTEGER NOT NULL, \
                CORRECT INTEGER NOT NULL, \
                WRONG   INTEGER NOT NULL, \
                TIME	INTEGER NOT NULL, \
                GAMES   INTEGER NOT NULL, \
                FOREIGN KEY (USER_ID) REFERENCES USERS(ID));";

	SQLITE_EXEC(nullptr, nullptr);

	return true;
}

void Sqlitedatabase::close()
{
	sqlite3_close(this->_db);
}

bool Sqlitedatabase::doesUserExist(const std::string& userName)
{
	User *user = new User();
	this->_sql = ("SELECT * FROM USERS WHERE UNAME='" + userName + "';");

	SQLITE_EXEC(Sqlitedatabase::userCallBack, user);

	return (user->username == userName);
}

bool Sqlitedatabase::doesPasswordMatch(const std::string& userName, const std::string& password)
{
	User* user = new User();
	this->_sql = ("SELECT * FROM USERS WHERE UNAME='" + userName + "';");

	SQLITE_EXEC(Sqlitedatabase::userCallBack, user);

	bool does = user->password == password;
	delete user;

	return does;
}

void Sqlitedatabase::addNewUser(const std::string& userName, const std::string& password, const std::string& email)
{
	if (this->doesUserExist(userName)) return; // user exists

	this->_sql = "INSERT INTO USERS (UNAME, PWORD, EMAIL) VALUES (";

	this->_sql += ('\'' + userName + "','" + 
		password + "','" + email + "');");

	SQLITE_EXEC(nullptr, nullptr);

	this->_sql = "SELECT ID FROM USERS WHERE UNAME = '" + userName + "';";
	int id = 0;
	SQLITE_EXEC(Sqlitedatabase::idCallback, &id);

	this->_sql = "INSERT INTO STATS (USER_ID, SUM, CORRECT, WRONG, TIME, GAMES) VALUES (";
	this->_sql += std::to_string(id) +  ", 0, 0, 0, 0, 0);";

	SQLITE_EXEC(nullptr, nullptr);
}

std::vector<Question> Sqlitedatabase::getQuestions(int amount)
{
	std::vector<Question> questions;
	int questionsAmount;
	Question *tmpQuestion = new Question();
	int tmpNum = 0;
	bool numFound = false;
	int* randomNums = new int[amount];
	memset(randomNums, 0, sizeof(randomNums));
	srand((unsigned int)time(0));

	this->_sql = "SELECT ID FROM QUESTIONS ORDER BY id DESC LIMIT 1";
	SQLITE_EXEC(Sqlitedatabase::idCallback, &questionsAmount);

	for (int i = 0; i < amount; i++) 
	{
		while (true)
		{
			numFound = false;
			tmpNum = (std::rand() % questionsAmount + 1); // randon num from 1 to the amount of questions in db 

			for (int j = 0; j <= i; j++)
			{
				if (randomNums[j] == tmpNum)
				{
					numFound = true;
					break;
				}
			}
			if (!numFound)
			{
				randomNums[i] = tmpNum;
				break;
			}
		}

		this->_sql = "SELECT * FROM QUESTIONS WHERE ID=" + std::to_string(randomNums[i]) + ';';
		SQLITE_EXEC(Sqlitedatabase::questionCallBack, tmpQuestion);
		questions.push_back(*tmpQuestion);
	}

	delete[] randomNums;
	delete tmpQuestion;

	return questions;
}

void Sqlitedatabase::addQuestion(const Question& q)
{
	this->_sql = "INSERT INTO QUESTIONS (QUESTION_STR, W_ANS1, W_ANS2, W_ANS3, C_ANS) VALUES(";

	this->_sql += ('\'' + q.question + "','" +
		q.answers[0] + "','" + q.answers[1] + "','" +
		q.answers[2] + "','" + q.answers[3] + "');");

	SQLITE_EXEC(nullptr, nullptr);
}

float Sqlitedatabase::getPlayerAverageAnswerTime(std::string username)
{
	int sum = this->getStatsOfUser(username).sum;
	if (sum == 0)
	{
		return 0;
	}
	return float(this->getStatsOfUser(username).time / sum);
}

int Sqlitedatabase::getNumOfCorrectAnswers(std::string username)
{
	return this->getStatsOfUser(username).correct;
}

int Sqlitedatabase::getNumOfTotalAnswers(std::string username)
{
	return this->getStatsOfUser(username).sum;
}

int Sqlitedatabase::getNumOfPlayerGames(std::string username)
{
	return this->getStatsOfUser(username).games;
}

std::vector<std::pair<std::string, int>> Sqlitedatabase::getUsersScore()
{
	std::vector<std::pair<std::string, int>> usersScores;
	int userAmount;
	User user;

	this->_sql = "SELECT ID FROM USERS ORDER BY ID DESC LIMIT 1";
	SQLITE_EXEC(Sqlitedatabase::idCallback, &userAmount);

	for (int i = 1; i <= userAmount; i++)
	{	
		this->_sql = "SELECT * FROM USERS WHERE ID = " + std::to_string(i) + ";";
		SQLITE_EXEC(Sqlitedatabase::userCallBack, &user);
		usersScores.push_back({ user.username, this->getStatsOfUser(user.username).getScore() });
	}
	
	return usersScores;
}

UserStats Sqlitedatabase::getStatsOfUser(std::string username)
{
	UserStats s;
	this->_sql = "SELECT * FROM STATS WHERE ID = (SELECT ID FROM USERS WHERE UNAME == '" + username + "');";

	SQLITE_EXEC(Sqlitedatabase::userStatsCallback, &s);

	return s;
}

int Sqlitedatabase::userCallBack(void* data, int argc, char** argv, char** azColName)
{
	User *user = (User*)data;
	
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "UNAME")
		{
			user->username = std::string(argv[i]);
		}
		else if (std::string(azColName[i]) == "PWORD")
		{
			user->password = std::string(argv[i]);
		}
		else if (std::string(azColName[i]) == "EMAIL")
		{
			user->email = std::string(argv[i]);
		}
	}

	return 0;
}

int Sqlitedatabase::questionCallBack(void* data, int argc, char** argv, char** azColName)
{
	Question* question = (Question*)data;

	question->answers.resize(4);

	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "QUESTION_STR")
		{
			question->question = std::string(argv[i]);
		}
		else if (std::string(azColName[i]) == "W_ANS1")
		{
			question->answers[0] = std::string(argv[i]);
		}
		else if (std::string(azColName[i]) == "W_ANS2")
		{
			question->answers[1] = std::string(argv[i]);
		}
		else if (std::string(azColName[i]) == "W_ANS3")
		{
			question->answers[2] = std::string(argv[i]);
		}
		else if (std::string(azColName[i]) == "C_ANS")
		{
			question->answers[3] = std::string(argv[i]);
		}
	}

	return 0;
}

int Sqlitedatabase::idCallback(void* data, int argc, char** argv, char** azColName)
{
	int* num = (int*)data;
	
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "ID")
		{
			*num = std::atoi(argv[i]);
		}
	}

	return 0;
}

int Sqlitedatabase::userStatsCallback(void* data, int argc, char** argv, char** azColName)
{
	UserStats* s = (UserStats*)data;

	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "SUM")
		{
			s->sum = std::atoi(argv[i]);
		}
		else if (std::string(azColName[i]) == "CORRECT")
		{
			s->correct = std::atoi(argv[i]);
		}
		else if (std::string(azColName[i]) == "WRONG")
		{
			s->wrong = std::atoi(argv[i]);
		}
		else if (std::string(azColName[i]) == "TIME")
		{
			s->time = std::atoi(argv[i]);
		}
		else if (std::string(azColName[i]) == "GAMES")
		{
			s->games = std::atoi(argv[i]);
		}
	}
	return 0;
}

void Sqlitedatabase::addDefaultQuestions()
{
	std::vector<std::string> answers = { "Phapos", "Limasol", "Larnaka", "Nicosia" };
	this->addQuestion(Question{ "What is the capital city of Cyprus?", answers });
	answers = { "Tel aviv", "Tveria", "haifa", "Jeruslaem" };
	this->addQuestion(Question{ "What is the capital city of Israel?", answers });
	answers = { "Berlin", "Gibraltar", "Athens", "Budapest" };
	this->addQuestion(Question{ "What is the capital city of Hungery?", answers });
	answers = { "Rome", "Saint Helier", "Pristina", "Riga" };
	this->addQuestion(Question{ "What is the capital city of Latvia?", answers });
	answers = { "Pristina", "Riga", "Vaduz", "Vilnius" };
	this->addQuestion(Question{ "What is the capital city of Lithuania?", answers });
	answers = { "Warsaw", "Lisbon", "Bucharest", "Moscow" };
	this->addQuestion(Question{ "What is the capital city of Russia?", answers });
	answers = { "Belgrade", "Bratislava", "Ljubljana", "Madrid" };
	this->addQuestion(Question{ "What is the capital city of Spain?", answers });
	answers = { "Chisinau", "Monaco", "Podgorica", "Amsterdam" };
	this->addQuestion(Question{ "What is the capital city of Netherlands?", answers });
	answers = { "Valletta", "Chisinau", "Monaco", "Podgorica" };
	this->addQuestion(Question{ "What is the capital city of Montenegro?", answers });
	answers = { "Reykjavik", "Dublin", "Douglas", "Rome" };
	this->addQuestion(Question{ "What is the capital city of Italy?", answers });	
}
