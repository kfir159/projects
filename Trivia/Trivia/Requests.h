#pragma once

#include "IRequestHandler.h"

#include <iostream>
#include <time.h>

#define CODE_LOGIN 1
#define CODE_SIGNUP 2
#define CODE_LOGOUT 3
#define CODE_GET_ROOMS 4
#define CODE_CREATE_ROOM 5
#define CODE_JOIN_ROOM 6
#define CODE_GET_STATS 7
#define CODE_GET_HIGH_SCORE 8
#define CODE_GET_PLAYERS 9
#define CODE_CLOSE_ROOM 10
#define CODE_START_GAME 11
#define CODE_GET_STATE 12
#define CODE_LEAVE_ROOM 13
#define CODE_GET_RESULT 14
#define CODE_SUBMIT_ANSWER 15
#define CODE_GET_QUESTION 16
#define CODE_LEAVE_GAME 17
#define CODE_ADD_QUESTION 18
#define CODE_ERROR 101

class IRequestHandler;

struct LoginRequest
{
	std::string username;
	std::string password;
};

struct SignupRequest
{
	std::string username;
	std::string password;
	std::string email;
};

struct RequestInfo
{
	int id;
	time_t receivalTime;
	std::string buffer;
};

struct RequestResult
{
	std::string response;
	IRequestHandler* newHandler;
};

struct GetPlayersInRoomRequest
{
	unsigned int roomId;
};

struct JoinRoomRequest
{
	unsigned int roomId;
};

struct CreateRoomRequest
{
	std::string name;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
};

struct SubmitAnswerRequest
{
	unsigned int answerId;
};

struct AddQuestionRequest
{
	std::string question;
	std::string wrongAns1;
	std::string wrongAns2;
	std::string wrongAns3;
	std::string correctAns;
};
