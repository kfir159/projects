#include "LoginRequestHandler.h"

LoginRequestHandler::LoginRequestHandler(IDatabase* db, RequestHandlerFactory* factory)
{
	this->_loginManager = LoginManager(db);
	this->_handleFactory = factory;
}


bool LoginRequestHandler::isRequestRelevant(RequestInfo req)
{
	return req.id == CODE_LOGIN || req.id == CODE_SIGNUP;
}

RequestResult LoginRequestHandler::handleRequest(RequestInfo req)	
{
	switch (req.id)
	{
	case CODE_LOGIN:
		return this->_login(req);	
	case CODE_SIGNUP:
		return this->_signup(req);
	default:
		return RequestResult{ serializeResponse(ErrorResponse{"general error"}), nullptr };
	}
}

RequestResult LoginRequestHandler::_login(RequestInfo req)
{
	std::string buffer;
	LoginRequest lr = deserializeLoginRequest(req.buffer);

	if (this->_loginManager.login(lr.username, lr.password))
	{
		buffer = serializeResponse(LoginResponse{ CODE_LOGIN });
		return RequestResult{ buffer, this->_handleFactory->createMenuRequestHandler(User{ lr.username,lr.password, "" })};
	}

	buffer = serializeResponse(ErrorResponse{ "login failed" });
	return RequestResult{ buffer, this };
}

RequestResult LoginRequestHandler::_signup(RequestInfo req)
{
	std::string buffer;
	SignupRequest sr = deserializeSignupRequest(req.buffer);

	if (this->_loginManager.signup(sr.username, sr.password, sr.email))
	{
		buffer = serializeResponse(SignupResponse{ CODE_SIGNUP });
		return RequestResult{ buffer, this->_handleFactory->createMenuRequestHandler(User{ sr.username, sr.password, sr.email}) };
	}

	buffer = serializeResponse(ErrorResponse{ "signup failed" });
	return RequestResult{buffer, this};
}
