#pragma once

#include "Sqlitedatabase.h"
#include <iostream>
#include <vector>

#include <regex>
#include <string>

class LoginManager
{
public:
	/// <summary>
	/// This function is a c'tor of LoginManager.
	/// </summary>
	/// <param name="db"> - data base.</param>
	LoginManager(IDatabase* db);
	LoginManager() = default;
	/// <summary>
	/// This function is signup to the system.
	/// </summary>
	/// <param name="username"> - the user name.</param>
	/// <param name="password"> - the password of user.</param>
	/// <param name="email"> - the email of user.</param>
	bool signup(const std::string& username, const std::string& password, const std::string& email);
	/// <summary>
	///This function ligin user to the system.
	/// </summary>
	/// <param name="username"> - the user name.</param>
	/// <param name="password"> - the password of user.</param>
	bool login(const std::string& username, const std::string& password);
	/// <summary>
	/// Logs the user with a given username to the system.
	/// </summary>
	/// <param name="username"> - user name to logout</param>
	bool logout(const std::string& username);

private:
	/// <summary>
	/// This function check if the password valid.
	/// </summary>
	/// <param name="password"> - the password.</param>
	/// <returns>True of false if valid.</returns>
	bool isValidPassowrd(const std::string& password);
	/// <summary>
	/// This function check if the email valid.
	/// </summary>
	/// <param name="email"> - the email.</param>
	/// <returns>True of false if valid.</returns>
	bool isValidEmail(const std::string& email);


	IDatabase* _database;
	std::vector<User> _loggedUsers;
};
