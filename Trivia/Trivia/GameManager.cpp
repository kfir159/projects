#include "GameManager.h"

Game::Game(std::vector<Question> questions, std::vector<User> users, unsigned int roomId)
{
	this->_questions = questions;
	this->_roomId = roomId;
	
	for (unsigned int i = 0; i < users.size(); i++)
	{
		this->_players.insert({ users[i], (GameData{ Question(questions[0]), 0, 0, 0 }) });
	}
}

Question Game::getQuestienForUser(User user)
{
	if (this->_questions[0].question == this->_players[user].currQuestion.question)
	{
		this->_players[user].currQuestion = this->_questions[1];
		return this->_questions[0];
	}

	for (int i = 0; i < this->_questions.size(); i++)
	{
		if (this->_questions[i].question == this->_players[user].currQuestion.question)
		{
			if (i + 1 != this->_questions.size())
			{
				this->_players[user].currQuestion = this->_questions[i + 1];
				return this->_players[user].currQuestion;
			}
		}
	}
}

void Game::submitAnswerForUser(User user, std::string answer)
{
	if (this->_players[user].currQuestion.answers[3] == answer)
	{
		this->_players[user].correctAnswerCount++;
	}
	else 
	{
		this->_players[user].wrongAnswerCount++;
	}
}

void Game::removePlayer(User user)
{
	this->_players.erase(user);
}

GameData Game::getGameData(User user)
{
	return this->_players[user];
}

unsigned int Game::getRoomId()
{
	return this->_roomId;
}

GameManager::GameManager(IDatabase* db)
{
	this->_db = db;
}

Game GameManager::createGame(Room room)
{
	Game game(this->_db->getQuestions(room.getMetadata().sumOfQuestionsInGame), room.getUsers(), room.getMetadata().id);
	this->_games.push_back(game);
	return game;
}

void GameManager::deleteGame(Game game)
{
	for (auto it = this->_games.begin(); it != this->_games.end(); ++it)
	{
		if (it->getRoomId() == game.getRoomId())
		{
			this->_games.erase(it);
		}
	}
}
