#include "JsonResponsePacketSerializer.h"

std::string JsonResponsePacketSerializer::serializeResponse(const LoginResponse& resp)
{
	json jsonData = { {"status", CODE_LOGIN} };
	return JsonResponsePacketSerializer::_serializeJson(CODE_LOGIN, jsonData.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const SignupResponse& resp)
{
	json jsonData = { {"status", CODE_SIGNUP} };
	return JsonResponsePacketSerializer::_serializeJson(CODE_SIGNUP, jsonData.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const ErrorResponse& resp)
{
	json jsonData = { {"message", resp.message} };
	return JsonResponsePacketSerializer::_serializeJson(CODE_ERROR, jsonData.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const LogoutResponse& resp)
{
	json jsonData = { {"status", CODE_LOGOUT} };
	return JsonResponsePacketSerializer::_serializeJson(CODE_LOGOUT, jsonData.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const GetRoomsResponse& resp)
{
	json jsonData = {
		{"status", CODE_GET_ROOMS},
		{"rooms", Helper::vecToString(resp.rooms)}
	};
	return JsonResponsePacketSerializer::_serializeJson(CODE_GET_ROOMS, jsonData.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const CreateRoomResponse& resp)
{
	json jsonData = { {"status", CODE_CREATE_ROOM}, {"roomId", resp.id}};
	return JsonResponsePacketSerializer::_serializeJson(CODE_CREATE_ROOM, jsonData.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const JoinRoomResponse& resp)
{
	json jsonData = { {"status", CODE_JOIN_ROOM} };
	return JsonResponsePacketSerializer::_serializeJson(CODE_JOIN_ROOM, jsonData.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const getPersonalStatsResponse& resp)
{
	json jsonData = {
		{"status", CODE_GET_STATS},
		{"stats", Helper::vecToString(resp.stats)}
	};
	return JsonResponsePacketSerializer::_serializeJson(CODE_GET_STATS, jsonData.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const getHighScoreResponse& resp)
{
	json jsonData = {
		{"status", CODE_GET_HIGH_SCORE},
		{"scores", Helper::vecToString(resp.stats)}
	};

	return JsonResponsePacketSerializer::_serializeJson(CODE_GET_HIGH_SCORE, jsonData.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const GetPlayersInRoomResponse& resp)
{
	json jsonData = {
		{"status", CODE_GET_PLAYERS},
		{"players", Helper::vecToString(resp.players)}
	};
	return JsonResponsePacketSerializer::_serializeJson(CODE_GET_PLAYERS, jsonData.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const CloseRoomResponse& resp)
{
	json jsonData = {
		{"status", CODE_CLOSE_ROOM}
	};
	return JsonResponsePacketSerializer::_serializeJson(CODE_CLOSE_ROOM, jsonData.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const StartGameResponse& resp)
{
	json jsonData = {
		{"status", CODE_START_GAME}
	};
	return JsonResponsePacketSerializer::_serializeJson(CODE_START_GAME, jsonData.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const GetRoomStateResponse& resp)
{
	json jsonData = {
		{"status", CODE_GET_STATE},
		{"gameBegun", resp.hasGameBegun},
		{"players", Helper::vecToString(resp.players)},
		{"questionCount", resp.questionCount},
		{"answerTimeout", resp.answerTimeOut}
	};
	return JsonResponsePacketSerializer::_serializeJson(CODE_GET_STATE, jsonData.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const LeaveRoomResponse& resp)
{
	json jsonData = {
		{"status", CODE_LEAVE_ROOM}
	};
	return JsonResponsePacketSerializer::_serializeJson(CODE_LEAVE_ROOM, jsonData.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const GetGameResultsResponse& resp)
{
	json jsonData = {
		{"status", CODE_GET_RESULT},
		{"results", Helper::vecToString(resp.results)}
	};
	return JsonResponsePacketSerializer::_serializeJson(CODE_LEAVE_ROOM, jsonData.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const SubmitAnswerResponse& resp)
{
	json jsonData = {
		{"status", CODE_SUBMIT_ANSWER},
		{"correctAnswerId", resp.correctAnswerId}
	};
	return JsonResponsePacketSerializer::_serializeJson(CODE_SUBMIT_ANSWER, jsonData.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const GetQuestionResponse& resp)
{
	json jsonData = {
		{"status", CODE_GET_QUESTION},
		{"question", resp.question},
		{"answers", Helper::vecToString(resp.answers)}
	};
	return JsonResponsePacketSerializer::_serializeJson(CODE_GET_QUESTION, jsonData.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const LeaveGameResponse& resp)
{
	json jsonData = {
		{"status", CODE_LEAVE_GAME}
	};
	return JsonResponsePacketSerializer::_serializeJson(CODE_LEAVE_GAME, jsonData.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(const AddQuestionResponse& resp)
{
	json jsonData = {
		{"status", CODE_ADD_QUESTION}
	};
	return JsonResponsePacketSerializer::_serializeJson(CODE_ADD_QUESTION, jsonData.dump());
}

std::string JsonResponsePacketSerializer::_serializeJson(int respCode, std::string data)
{
	std::string tmpBin = "";
	std::string buffer;
	int dataSize = JsonResponsePacketSerializer::_getDataSize(data);

	// resp code
	tmpBin = Helper::toBinary(respCode);
	buffer += Helper::padBinaryNumber(tmpBin, BYTE_LEN);

	// data size
	tmpBin = Helper::toBinary(dataSize);
	buffer += Helper::padBinaryNumber(tmpBin, BYTE_LEN * 4);

	// data
	tmpBin = Helper::toBinary(data);
	buffer += Helper::padBinaryNumber(tmpBin, BYTE_LEN * dataSize);

	return buffer;
}

int JsonResponsePacketSerializer::_getDataSize(std::string data)
{
	int dataSize = (Helper::toBinary(data).length()) / BYTE_LEN;

	if ((Helper::toBinary(data).length() % BYTE_LEN) != 0)
	{
		dataSize++;
	}

	return dataSize;
}
