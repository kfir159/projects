#pragma once

#include <WinSock2.h>
#include <Windows.h>

class WSAInitializer
{
public:
	/// <summary>
	/// This function is a c'tor of WSAInitializer.
	/// </summary>
	WSAInitializer();
	/// <summary>
	/// This function is a d'tor of WSAInitializer.
	/// </summary>
	~WSAInitializer();
};
