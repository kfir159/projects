#include "RoomMemberRequestHandler.h"

RoomAdminRequestHandler::RoomAdminRequestHandler(Room room, User user, RoomManager* roomManager, RequestHandlerFactory* factory) :
    RoomMemberRequestHandler(room, user, roomManager, factory)
{
    this->_room = room;
    this->_user = user;
    this->_roomManager = roomManager;
    this->_handlerFactory = factory;
}

bool RoomAdminRequestHandler::isRequestRelevant(RequestInfo req)
{
    return req.id >= CODE_CLOSE_ROOM && req.id <= CODE_GET_STATE;
}

RequestResult RoomAdminRequestHandler::handleRequest(RequestInfo req)
{
    switch (req.id)
    {
    case CODE_CLOSE_ROOM:
        return this->_closeRoom(req);
    case CODE_START_GAME:
        return this->_askStartGame(req);
    case CODE_GET_STATE:
        return this->_getRoomState(req);
    default:
        return RequestResult{ serializeResponse(ErrorResponse{"general error"}), nullptr };
    }
}

RequestResult RoomAdminRequestHandler::_closeRoom(RequestInfo req)
{
    this->_roomManager->deleteRoom(this->_room.getMetadata().id);
    return RequestResult{ serializeResponse(CloseRoomResponse {(unsigned int)req.id}), 
        this->_handlerFactory->createMenuRequestHandler(this->_user)};
}

RequestResult RoomAdminRequestHandler::_askStartGame(RequestInfo req)
{
    this->_roomManager->getRoomById(this->_room.getMetadata().id).setRoomActive(1);
    return this->_startGame(req);
}
