#pragma once

#include "IRequestHandler.h"
#include "IDatabase.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "RequestHandlerFactory.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"

class RequestHandlerFactory;

using namespace JsonRequestPacketDeserializer;
using namespace JsonResponsePacketSerializer;

class MenuRequestHandler : public IRequestHandler
{
public:
	MenuRequestHandler(User user, RequestHandlerFactory* factory);
	/// <summary>
	/// Checks if the request is a valid login/signup request
	/// </summary>
	/// <param name="req">A RequestInfo struct containg data from client</param>
	/// <returns>bool</returns>
	virtual bool isRequestRelevant(RequestInfo req);
	/// <summary>
	/// Handles the request sent from the client, reads the buffer, responds and calls the next handler
	/// </summary>
	/// <param name="req">A RequestInfo struct containg data from client</param>
	/// <returns>RequestResult struct</returns>
	virtual RequestResult handleRequest(RequestInfo req);

private:
	/// <summary>
	/// Creates a new Requset result struct for the given requestInfo
	/// </summary>
	/// <param name="req"> - the request info.</param>
	/// <returns>The request result.</returns>
	RequestResult _logout(RequestInfo req);
	RequestResult _getRooms(RequestInfo req);
	RequestResult _getPlayersInRoom(RequestInfo req);
	RequestResult _getPersonalStats(RequestInfo req);
	RequestResult _getHighScore(RequestInfo req);
	RequestResult _joinRoom(RequestInfo req);
	RequestResult _createRoom(RequestInfo req);
	RequestResult _addQuestion(RequestInfo req);

	User _user;
	RequestHandlerFactory* _handlerFactory;
};
