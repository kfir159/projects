#pragma once

#include "Responses.h"
#include "Requests.h"
#include "Helper.h"
#include "../json-develop/json-develop/single_include/nlohmann/json.hpp"

using json = nlohmann::json;

#define BYTE_LEN 8

namespace JsonResponsePacketSerializer
{
	/// <summary>
	/// This functions call to function _serializeJson with a message according to the response
	/// </summary>
	/// <param name="resp"> - the response structure.</param>
	/// <returns>The vector bytes of the message.</returnsk>
	std::string serializeResponse(const LoginResponse& resp);
	std::string serializeResponse(const SignupResponse& resp);
	std::string serializeResponse(const ErrorResponse& resp);
	std::string serializeResponse(const LogoutResponse& resp);
	std::string serializeResponse(const GetRoomsResponse& resp);
	std::string serializeResponse(const CreateRoomResponse& resp);
	std::string serializeResponse(const JoinRoomResponse& resp);
	std::string serializeResponse(const getPersonalStatsResponse& resp);
	std::string serializeResponse(const getHighScoreResponse& resp);
	std::string serializeResponse(const GetPlayersInRoomResponse& resp);
	std::string serializeResponse(const CloseRoomResponse& resp);
	std::string serializeResponse(const StartGameResponse& resp);
	std::string serializeResponse(const GetRoomStateResponse& resp);
	std::string serializeResponse(const LeaveRoomResponse& resp);
	std::string serializeResponse(const GetGameResultsResponse& resp);
	std::string serializeResponse(const SubmitAnswerResponse& resp);
	std::string serializeResponse(const GetQuestionResponse& resp);
	std::string serializeResponse(const LeaveGameResponse& resp);
	std::string serializeResponse(const AddQuestionResponse& resp);

	/// <summary>
	/// This function colculate the size of the data.
	/// </summary>
	/// <param name="data"> - the json data.</param>
	/// <returns>The size of the data.</returns>
	int _getDataSize(std::string data);
	/// <summary>
	/// Serializes a json according to the protocol.
	/// Protocol - (respCode (1 byte), dataLength (4 bytes), data (dataLength bytes))
	/// Every field of the protocol is written in binary.
	/// </summary>
	/// <param name="respCode"> - A given resp code</param>
	/// <param name="data"> - The json data to serialize</param>
	/// <returns>Vector of bytes</returns>
	std::string _serializeJson(int respCode, std::string data);
}
