#include "RoomMemberRequestHandler.h"

RoomMemberRequestHandler::RoomMemberRequestHandler(Room room, User user, RoomManager* roomManager, RequestHandlerFactory* factory)
{
    this->_room = room;
    this->_user = user;
    this->_roomManager = roomManager;
    this->_handlerFactory = factory;
}

bool RoomMemberRequestHandler::isRequestRelevant(RequestInfo req)
{
    return req.id >= CODE_START_GAME && req.id <= CODE_LEAVE_ROOM;
}

RequestResult RoomMemberRequestHandler::handleRequest(RequestInfo req)
{
    switch (req.id) 
    {
    case CODE_START_GAME:
        return this->_startGame(req);
    case CODE_GET_STATE:
        return this->_getRoomState(req);
    case CODE_LEAVE_ROOM:
        return this->_leaveRoom(req);
    default:
        return RequestResult{ serializeResponse(ErrorResponse{"general error"}), nullptr };
    }
}

RequestResult RoomMemberRequestHandler::_leaveRoom(RequestInfo req)
{
    this->_roomManager->getRoomById(this->_room.getMetadata().id).removeUser(this->_user);
    return RequestResult{ serializeResponse(LeaveRoomResponse {(unsigned int)req.id }), 
        this->_handlerFactory->createMenuRequestHandler(this->_user)};
}

RequestResult RoomMemberRequestHandler::_startGame(RequestInfo req)
{
    Game* game = new Game(this->_handlerFactory->getDb()->getQuestions(this->_room.getMetadata().sumOfQuestionsInGame), this->_room.getUsers(), this->_room.getMetadata().id);
    return RequestResult{ serializeResponse(StartGameResponse {(unsigned int)req.id}), this->_handlerFactory->createGameRequestHandler(game, this->_user) };
}

RequestResult RoomMemberRequestHandler::_getRoomState(RequestInfo req)
{
    std::cout << this->_user.username << std::endl;

    return RequestResult{ serializeResponse(GetRoomStateResponse {
        (unsigned int)req.id,
        this->_roomManager->getRoomById(this->_room.getMetadata().id).getMetadata().isActive,
        this->_roomManager->getRoomById(this->_room.getMetadata().id).getAllUsers(),
        this->_roomManager->getRoomById(this->_room.getMetadata().id).getMetadata().sumOfQuestionsInGame,
        this->_roomManager->getRoomById(this->_room.getMetadata().id).getMetadata().timePerQuestion } ),
        this
    };
}
