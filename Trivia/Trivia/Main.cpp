#include "Server.h"
#include "WSAInitializer.h"

#include "LoginManager.h"
#include "Sqlitedatabase.h"
#include "StatisticsManager.h"

int main()
{
	//server main code
	try
	{
		WSAInitializer wsainit;
		Server server;
		server.run();
	}
	catch (const std::exception& e)
	{
		std::cerr << "error occured: " << e.what() << std::endl;
	}

	return 0;
}
