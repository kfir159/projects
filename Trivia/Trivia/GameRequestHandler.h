#pragma once

#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "GameManager.h"

class RequestHandlerFactory;

using namespace JsonRequestPacketDeserializer;
using namespace JsonResponsePacketSerializer;

class GameRequestHandler : public IRequestHandler
{
public:
	GameRequestHandler(Game* game, User user, GameManager* gameManager, RequestHandlerFactory* handlerFactory);
	// <summary>
	/// Checks if the request is a valid request
	/// </summary>
	/// <param name="req">A RequestInfo struct containg data from client</param>
	/// <returns>bool</returns>
	virtual bool isRequestRelevant(RequestInfo req);
	/// <summary>
	/// Handles the request sent from the client, reads the buffer, responds and calls the next handler
	/// </summary>
	/// <param name="req">A RequestInfo struct containg data from client</param>
	/// <returns>RequestResult struct</returns> 
	virtual RequestResult handleRequest(RequestInfo req);

private:
	/// <summary>
	/// Creates a new Requset result struct for the given requestInfo
	/// </summary>
	/// <param name="req"> - the request info.</param>
	/// <returns>The request result.</returns>
	RequestResult _getQuestion(RequestInfo req);
	RequestResult _submitAnswer(RequestInfo req);
	RequestResult _getGameResults(RequestInfo req);
	RequestResult _leaveGame(RequestInfo req);
	RequestResult _getRoomState(RequestInfo req);

	Game* _game;
	User _user;
	GameManager* _gameManger;
	RequestHandlerFactory* _factory;
};

