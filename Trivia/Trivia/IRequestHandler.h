#pragma once

#include "Requests.h"

struct RequestInfo;
struct RequestResult;

class IRequestHandler
{
public:
	// <summary>
	/// Checks if the request is a valid request
	/// </summary>
	/// <param name="req">A RequestInfo struct containg data from client</param>
	/// <returns>bool</returns>
	virtual bool isRequestRelevant(RequestInfo req) = 0;
	/// <summary>
	/// Handles the request sent from the client, reads the buffer, responds and calls the next handler
	/// </summary>
	/// <param name="req">A RequestInfo struct containg data from client</param>
	/// <returns>RequestResult struct</returns> 
	virtual RequestResult handleRequest(RequestInfo req) = 0;
};
