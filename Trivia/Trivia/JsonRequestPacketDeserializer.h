#pragma once

#include "Requests.h"
#include "../json-develop/json-develop/single_include/nlohmann/json.hpp"
#include "Helper.h"

using json = nlohmann::json;

#define BYTE_LEN 8

namespace JsonRequestPacketDeserializer
{
	/// <summary>
	/// Creates a request struct according to the given buffer.
	/// </summary>
	/// <param name="buffer"> - a binary buffer</param>
	/// <returns>A requset struct</returns>
	LoginRequest deserializeLoginRequest(std::string buffer);
	SignupRequest deserializeSignupRequest(std::string buffer);
	GetPlayersInRoomRequest deserializeGetPlayersRequest(std::string buffer);
	JoinRoomRequest deserializeJoinRoomRequest(std::string buffer);
	CreateRoomRequest deserializeCreateRoomRequest(std::string buffer);
	SubmitAnswerRequest deserializerSubmitAnswerRequest(std::string buffer);
	AddQuestionRequest deserializerAddQuestionRequest(std::string buffer);
}
