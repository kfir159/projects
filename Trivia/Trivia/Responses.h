#pragma once

#include <iostream>
#include "Room.h"
#include <vector>
#include <map>


struct LoginResponse
{
	unsigned int status;
};

struct SignupResponse
{
	unsigned int status;
};

struct ErrorResponse
{
	std::string message;
};

struct LogoutResponse
{
    unsigned int status;
};

struct GetRoomsResponse
{
    unsigned int status;
    std::vector<RoomData> rooms;
};

struct CreateRoomResponse
{
	unsigned int id;
	unsigned int status;
};

struct JoinRoomResponse
{
	unsigned int status;
};

struct getPersonalStatsResponse
{
	unsigned int status;
	std::vector<std::string> stats;
};

struct getHighScoreResponse
{
	unsigned int status;
	std::vector<std::pair<std::string, int>> stats;
};

struct GetPlayersInRoomResponse
{
	unsigned int status;
	std::vector<std::string> players;
};

struct CloseRoomResponse
{
	unsigned int status;
};

struct StartGameResponse
{
	unsigned int status;
};

struct GetRoomStateResponse
{
	unsigned int status;
	unsigned int hasGameBegun;
	std::vector<std::string> players;
	unsigned int questionCount;
	unsigned int answerTimeOut;
};

struct LeaveRoomResponse
{
	unsigned int status;
};

struct PlayerResults
{
	std::string username;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int averageAnswerTime;
};

struct GetGameResultsResponse
{
	unsigned int status;
	std::vector<PlayerResults> results;
};

struct SubmitAnswerResponse
{
	unsigned int status;
	unsigned int correctAnswerId;
};

struct GetQuestionResponse
{
	unsigned int status;
	std::string question;
	std::map<unsigned int, std::string>	answers;
};

struct LeaveGameResponse
{
	unsigned int status;
};

struct AddQuestionResponse
{
	unsigned int status;
};