#pragma once

#include "Communicator.h"
#include <iostream>
#include <exception>

class Server
{
public:
	/// <summary>
	/// This function call to startHandleRequests in class Communicator.
	/// </summary>
	void run();

private:
	/// <summary>
	/// Works on a thread that gets input from adming
	/// </summary>
	void adminInput();

	IDatabase* _database;
	Communicator _communicator;
	RequestHandlerFactory _handlerFactory;
};
