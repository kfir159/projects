#include "Communicator.h"

Communicator::Communicator()
{
	this->_handlerFactory = new RequestHandlerFactory();
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	this->_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (this->_serverSocket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__ " - socket");
	}
}

Communicator::~Communicator()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(this->_serverSocket);
		delete this->_handlerFactory;
	}
	catch (...) {}
}

void Communicator::startHandleRequests()
{
	this->_bindAndListen();
}

void Communicator::_bindAndListen()
{
	struct sockaddr_in sa = { 0 };

	// port that server will listen for
	sa.sin_port = htons(SERVER_PORT);
	// must be AF_INET
	sa.sin_family = AF_INET;
	// when there are few ip's for the machine. We will use always "INADDR_ANY"
	sa.sin_addr.s_addr = INADDR_ANY;

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(this->_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - bind");
	}

	// Start listening for incoming requests of clients
	if (listen(this->_serverSocket, SOMAXCONN) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - listen");
	}

	while (true)
	{
		this->_accept();
	}
}

void Communicator::_accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET clientSocket = ::accept(this->_serverSocket, NULL, NULL);

	if (clientSocket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__);
	}

	std::thread(&Communicator::_handleNewClient, this, clientSocket).detach();
}

void Communicator::_handleNewClient(SOCKET sock)
{
	int res;
	RequestResult resp;
	RequestInfo req;
	LoginRequestHandler* loginHandler = this->_handlerFactory->createLoginRequestHandler();

	this->_clientsMutex.lock();
	this->_clients.insert({sock, loginHandler});
	this->_clients[sock] = loginHandler;
	this->_clientsMutex.unlock();

	while (sock != NULL)
	{
		try
		{
			req = Helper::createRequestInfo(sock);
			this->_clientsMutex.lock();
			resp = this->_clients[sock]->handleRequest(req);
			this->_clients[sock] = resp.newHandler;
			this->_clientsMutex.unlock();

			std::cout << "Output: " << Helper::binaryToString(resp.response.substr(5*8)) << std::endl;

			res = send(sock, resp.response.c_str(), resp.response.length(), 0);
		}
		catch(...)
		{
			this->_deleteClient(sock);
		}
	}

	this->_deleteClient(sock);
}

void Communicator::_deleteClient(SOCKET sock)
{
	this->_clientsMutex.lock();
	this->_clients.erase(sock);
	this->_clientsMutex.unlock();
}
