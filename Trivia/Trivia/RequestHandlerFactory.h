#pragma once

#include "LoginManager.h"
#include "LoginRequestHandler.h"
#include "GameRequestHandler.h"
#include "Sqlitedatabase.h"
#include "StatisticsManager.h"
#include "IDatabase.h"
#include "RoomManager.h"
#include "RoomMemberRequestHandler.h"

class LoginRequestHandler;
class MenuRequestHandler;
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;
class GameRequestHandler;

class GameManager;
class Game;

class RequestHandlerFactory
{
public:
	//c'tors & d'tor
	RequestHandlerFactory(IDatabase* db, LoginManager loginManager, StatisticsManager statisticsManager);
	RequestHandlerFactory();
	~RequestHandlerFactory();

	//getters create new class.
	LoginRequestHandler* createLoginRequestHandler();
	MenuRequestHandler* createMenuRequestHandler(User user);
	RoomAdminRequestHandler* createRoomAdminRequestHandler(User user, Room room);
	RoomMemberRequestHandler* createRoomMemberRequestHandler(User user, Room room);
	GameRequestHandler* createGameRequestHandler(Game* game, User user);

	//getters.
	StatisticsManager& getStatisticsManager();
	RoomManager* getRoomManager();
	LoginManager getLoginManager();
	GameManager* getGameManager();
	IDatabase* getDb();

private:
	LoginManager _loginManager;
	IDatabase* _db;
	RoomManager* _roomManager;
	StatisticsManager _statisticsManager;
	GameManager* _gameManager;
};