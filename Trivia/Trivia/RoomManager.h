#pragma once
#include "Room.h"
#include <map>
#include <vector>

class RoomManager
{
public:
	/// <summary>
	/// This function create room.
	/// </summary>
	/// <param name="user"> - the user.</param>
	/// <param name="data"> - the data of the room.</param>
	void createRoom(User user, RoomData data);
	/// <summary>
	/// This function delete the room.
	/// </summary>
	/// <param name="ID"> - the id of the room.</param>
	void deleteRoom(int ID);
	/// <summary>
	/// This function return the state of the room.
	/// </summary>
	/// <param name="ID"> - the id of the room.</param>
	/// <returns>If the room active or don't.</returns>
	unsigned int getRoomState(int ID);
	/// <summary>
	/// This function return the all rooms.
	/// </summary>
	/// <returns>Vector of the all rooms.</returns>
	std::vector<RoomData> getRooms();
	/// <summary>
	/// This function get room according his id.
	/// </summary>
	/// <param name="id"> - the id of room.</param>
	/// <returns>The room.</returns>
	Room& getRoomById(unsigned int id);

private:
	std::map<int, Room> _rooms;
};
