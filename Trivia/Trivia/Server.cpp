#include "Server.h"

void Server::run()
{
	std::thread threadAdminInput = std::thread(&Server::adminInput, this);
	
	this->_communicator.startHandleRequests();

	threadAdminInput.join();
}

void Server::adminInput()
{
	std::string in = "";

	while (true)
	{
		std::cin >> in;

		if (in == "EXIT")
		{
			exit(0);
		}
	}
}
