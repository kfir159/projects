#include "StatisticsManager.h"

StatisticsManager::StatisticsManager(IDatabase* db)
{
    this->_db = db;
}

std::vector<std::pair<std::string, int>> StatisticsManager::getTopFiveUsers()
{
    auto usersScore = this->_db->getUsersScore();
    std::sort(usersScore.begin(), usersScore.end(), [](auto &p1, auto &p2) {return p1.second < p2.second; });
    std::vector<std::pair<std::string, int>> names;
    int amount = (usersScore.size() >= 5) ? 5 : usersScore.size();

    for (int i = 0; i < amount; i++)
    {
        names.push_back({ usersScore.back().first, usersScore.back().second });
        usersScore.pop_back();
    }

    return names;
}

std::vector<std::string> StatisticsManager::getUserStatistics(std::string username)
{
    std::vector<std::string> stats;

    stats.push_back(std::to_string(this->_db->getNumOfCorrectAnswers(username)));
    stats.push_back(std::to_string(this->_db->getNumOfPlayerGames(username)));
    stats.push_back(std::to_string(this->_db->getNumOfTotalAnswers(username)));
    stats.push_back(std::to_string(this->_db->getPlayerAverageAnswerTime(username)));
    stats.push_back(std::to_string(this->_db->getStatsOfUser(username).getScore()));

    return stats;
}