#include "RoomManager.h"

void RoomManager::createRoom(User user, RoomData data)
{
	Room newRoom(data);
	newRoom.addUser(user);

	this->_rooms.insert({ data.id, newRoom});
}

void RoomManager::deleteRoom(int ID)
{
	this->_rooms.erase(ID);
}

unsigned int RoomManager::getRoomState(int ID)
{
	return this->_rooms[ID].getMetadata().isActive;
}

std::vector<RoomData> RoomManager::getRooms()
{
	std::vector<RoomData> dataVec;

	for (auto it = this->_rooms.begin(); it != this->_rooms.end(); ++it) 
	{
		dataVec.push_back(it->second.getMetadata());
	}

	return dataVec;
}

Room& RoomManager::getRoomById(unsigned int id)
{
	return this->_rooms[id];
}
