#pragma once

#include <iostream>
#include <bitset>
#include <sstream>
#include <iomanip>
#include <vector>
#include <string>
#include "Requests.h"
#include "Responses.h"
#include "Room.h"
#include <WinSock2.h>
#include <Windows.h>

#define BYTE_LEN 8

class Helper
{
public:
	static RequestInfo createRequestInfo(SOCKET sock);
	/// <summary>
	/// Creates the request info struct using the given binary buffer.
	/// </summary>
	/// <param name="binaryBuffer"> - a given buffer</param>
	/// <returns>RequestInfo struct</returns>
	static RequestInfo createRequestInfo(std::string binaryBuffer);
	/// <summary>
	/// Converts a binary string to a decimal int
	/// </summary>
	/// <param name="n"> - A given string</param>
	/// <returns>Int decimal number</returns>
	static int binaryToDecimal(const std::string& n);
	/// <summary>
	/// Converts a binary string to ascii string
	/// </summary>
	/// <param name="binaryBuffer"> - A given string</param>
	/// <returns>An ascii string.</returns>
	static std::string binaryToString(const std::string& binaryBuffer);
	/// <summary>
	/// Converts a string to a binary string.
	/// </summary>
	/// <param name="s"> - A given string</param>
	/// <returns>String of binary numbers</returns>
	static std::string	toBinary(const std::string& s);
	/// <summary>
	/// Converts a number to a binary string.
	/// </summary>
	/// <param name="n"> - A given number</param>
	/// <returns>String of binary numbers</returns>
	static std::string toBinary(int n);
	/// <summary>
	/// Padding zeros at the start of a binary string to fill a given
	/// amount of bytes (1010 -> 0000 1010).
	/// </summary>
	/// <param name="s"> - A binary string</param>
	/// <param name="size"> - Amount of bytes</param>
	/// <returns>Vector of bytes</returns>
	static std::string padBinaryNumber(const std::string& s, const int size);
	/// <summary>
	/// This funciton return string with the all of the elements of the vactor.
	/// </summary>
	/// <param name="resp"> - the vector.</param>
	/// <returns>The string.</returns>
	static std::string vecToString(std::vector<std::string> resp);
	/// <summary>
	/// This funciton return string with the all of the elements of the vactor.
	/// </summary>
	/// <param name="resp"> - the vector.</param>
	/// <returns>The string.</returns>
	static std::string vecToString(std::vector<RoomData> resp);
	/// <summary>
	/// This funciton return string with the all of the elements of the vactor.
	/// </summary>
	/// <param name="resp"> - the vector.</param>
	/// <returns>The string.</returns>
	static std::string vecToString(std::vector<std::pair<std::string, int>> resp);
	/// <summary>
	/// 
	/// </summary>
	/// <param name="vec"></param>
	/// <returns></returns>
	static std::string vecToString(std::vector<PlayerResults> resp);

	static std::string vecToString(std::map<unsigned int, std::string> resp);
};
