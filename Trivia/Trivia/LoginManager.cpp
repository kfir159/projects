#include "LoginManager.h"

LoginManager::LoginManager(IDatabase* db)
{
	this->_database = db;
}

bool LoginManager::signup(const std::string& username, const std::string& password, const std::string& email)
{
	if (!this->isValidPassowrd(password) || this->_database->doesUserExist(username) || !this->isValidEmail(email)) 
	{
		return false;
	}

	this->_database->addNewUser(username, password, email);
	return this->login(username, password);
}

bool LoginManager::login(const std::string& username, const std::string& password)
{
	// checks if user is already logged in
	for (auto it : this->_loggedUsers)
	{
		if (it.username == username)
		{
			return false;
		}
	}

	// checking if username exists and password matches
	if (!(this->_database->doesUserExist(username) && this->_database->doesPasswordMatch(username, password)))
	{
		return false;
	}
	
	this->_loggedUsers.push_back(User{username, password});
	return true;
}

bool LoginManager::logout(const std::string& username)
{
	for (auto it = this->_loggedUsers.begin(); it != this->_loggedUsers.end(); ++it)
	{
		if (it->username == username)
		{
			this->_loggedUsers.erase(it);
			return true;
		}
	}

	return false;
}

bool LoginManager::isValidPassowrd(const std::string& password)
{
	std::regex r(R"(^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[*!@#$%^&]).{8}$)");
	std::smatch m;

	std::regex_search(password, m, r);

	return m.size() > 0;
}

bool LoginManager::isValidEmail(const std::string& email)
{
	std::regex r(R"(^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$)");
	std::smatch m;

	std::regex_search(email, m, r);

	return m.size() > 0;
}