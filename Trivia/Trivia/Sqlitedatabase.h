#pragma once

#include "IDatabase.h"
#include "sqlite3.h"
#include <iostream>
#include <io.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <string>
#include <vector>

#define DB_PATH "triviaDB.sqlite"

class Sqlitedatabase : public IDatabase
{
public:
	Sqlitedatabase() = default;
	/// <summary>
	/// Opens a new database and creates the tables in it.
	/// </summary>
	/// <returns>true if the db didnt exist</returns>
	virtual bool open();
	/// <summary>
	/// Closes the db.
	/// </summary>
	virtual void close();
	/// <summary>
	/// Checks if a user with a given name exists in db.
	/// </summary>
	/// <param name="userName"> - username to check</param>
	/// <returns>bool</returns>
	virtual bool doesUserExist(const std::string& userName);
	/// <summary>
	/// Checks if a given password is the password of a user.
	/// </summary>
	/// <param name="userName"> - a given user name</param>
	/// <param name="password"> - password to check</param>
	/// <returns>bool</returns>
	virtual bool doesPasswordMatch(const std::string& userName, const std::string& password);
	/// <summary>
	/// Adds a new user to the db.
	/// </summary>
	/// <param name="userName"> - new username</param>
	/// <param name="password"> - new password</param>
	/// <param name="email"> - new email</param>
	virtual void addNewUser(const std::string& userName, const std::string& password, const std::string& email);
	/// <summary>
	/// Gets a given amount of questions from the db.
	/// </summary>
	/// <param name="amount"> - amount of questions</param>
	/// <returns>A list of questions</returns>
	virtual std::vector<Question> getQuestions(int amount);
	/// <summary>
	/// Adds a new question to the db
	/// </summary>
	/// <param name="q"> - a given question</param>
	virtual void addQuestion(const Question& q);
	/// <summary>
	/// Getting the stats of a user by a given username.
	/// </summary>
	/// <param name="username"> - username</param>
	/// <returns>A UserStats struct contains every stat on a player</returns>
	virtual UserStats getStatsOfUser(std::string username);
	/// <summary>
	///  This function get the average time that user did for questios.
	/// </summary>
	/// <param name="username"> - the user name.</param>
	/// <returns>The average time.</returns>
	virtual float getPlayerAverageAnswerTime(std::string username);
	/// <summary>
	/// This function get the count of the correct answer.
	/// </summary>
	/// <param name="username"> - the user name.</param>
	/// <returns>The count of the correct answer</returns>
	virtual int getNumOfCorrectAnswers(std::string username);
	/// <summary>
	/// This function get the count of total answer of users.
	/// </summary>
	/// <param name="username"> - the user name.</param>
	/// <returns>The count of total answer of users.</returns>
	virtual int getNumOfTotalAnswers(std::string username);
	/// <summary>
	/// Getting the amount of games a player has played. 
	/// </summary>
	/// <param name="username"> - username</param>
	/// <returns>Amount of games</returns>
	virtual int getNumOfPlayerGames(std::string username);
	/// <summary>
	/// Getting the scores of every user on the database. 
	/// </summary>
	/// <returns>Scores of all users</returns>
	virtual std::vector<std::pair<std::string, int>> getUsersScore();

private:
	/// <summary>
	/// Callback that creates a User struct.
	/// </summary>
	static int userCallBack(void* data, int argc, char** argv, char** azColName);
	/// <summary>
	/// This function is a callback of question.
	/// </summary>
	static int questionCallBack(void* data, int argc, char** argv, char** azColName);
	/// <summary>
	/// This function is a callback of id.
	/// </summary>
	static int idCallback(void* data, int argc, char** argv, char** azColName);
	/// <summary>
	/// This function is a callback of user.
	/// </summary>
	static int userStatsCallback(void* data, int argc, char** argv, char** azColName);
	/// <summary>
	/// This funtion add 10 questions to data base.
	/// </summary>
	void addDefaultQuestions();
	
	std::string _sql;
	char* _err;
	int _res;
	sqlite3* _db;
};