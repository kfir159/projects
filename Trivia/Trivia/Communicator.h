#pragma comment(lib, "ws2_32.lib")

#include <WinSock2.h>
#include <Windows.h>

#include <iostream>

#include <thread>
#include <mutex>

#include <exception>

#include <map>

#include "IRequestHandler.h"
#include "LoginRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "Helper.h"
#include "RequestHandlerFactory.h"

#define SERVER_PORT 7777
#define BUFFER_SIZE 4096

class Communicator
{
public:
	/// <summary>
	/// C'tor of Communicator, opens a new server main socket.
	/// </summary>
	Communicator();
	/// <summary>
	/// D'tor of Communicator, closes the server main socket.
	/// </summary>
	~Communicator();

	/// <summary>
	/// Calling the bind and listen function.
	/// </summary>
	void startHandleRequests();
private:
	/// <summary>
	/// Starts the communication, handling new clients
	/// and creating their connections.
	/// </summary>
	void _bindAndListen();
	/// <summary>
	/// Accepts new client to the server
	/// creating clients thread and calling their handler.
	/// </summary>
	void _accept();
	/// <summary>
	/// "Main" code for every connected client.
	/// </summary>
	/// <param name="sock"> - new clients socket.</param>
	void _handleNewClient(SOCKET sock);

	void _deleteClient(SOCKET sock);

	/// <summary>
	/// This map holds clients socket and an handler for them.
	/// </summary>
	std::map<SOCKET, IRequestHandler*> _clients;
	RequestHandlerFactory* _handlerFactory;
	SOCKET _serverSocket;
	std::mutex _clientsMutex;
};
