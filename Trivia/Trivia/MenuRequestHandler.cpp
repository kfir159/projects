#include "MenuRequestHandler.h"

MenuRequestHandler::MenuRequestHandler(User user, RequestHandlerFactory* factory)
{
	this->_user = user;
	this->_handlerFactory = factory;
}

bool MenuRequestHandler::isRequestRelevant(RequestInfo req)
{
	return (req.id >= CODE_LOGOUT && req.id <= CODE_GET_PLAYERS) || req.id == CODE_ADD_QUESTION;
}

RequestResult MenuRequestHandler::handleRequest(RequestInfo req)
{
	switch (req.id)
	{
	case CODE_LOGOUT:
		return this->_logout(req);
	case CODE_GET_ROOMS:
		return this->_getRooms(req);
	case CODE_CREATE_ROOM:
		return this->_createRoom(req);
	case CODE_JOIN_ROOM:
		return this->_joinRoom(req);
	case CODE_GET_STATS:
		return this->_getPersonalStats(req);
	case CODE_GET_HIGH_SCORE:
		return this->_getHighScore(req);
	case CODE_GET_PLAYERS:
		return this->_getPlayersInRoom(req);
	case CODE_ADD_QUESTION:
		return this->_addQuestion(req);
	default:
		return RequestResult{ serializeResponse(ErrorResponse{"general error"}), nullptr };
	}
}

RequestResult MenuRequestHandler::_logout(RequestInfo req)
{
	this->_handlerFactory->getLoginManager().logout(this->_user.username);
	return RequestResult{ serializeResponse(LogoutResponse{ CODE_LOGOUT }), this->_handlerFactory->createLoginRequestHandler()};
}

RequestResult MenuRequestHandler::_getRooms(RequestInfo req)
{
	return RequestResult
	{ 
		serializeResponse(GetRoomsResponse { 
		CODE_GET_ROOMS,
		this->_handlerFactory->getRoomManager()->getRooms() }), 
		this 
	};
}

RequestResult MenuRequestHandler::_getPlayersInRoom(RequestInfo req)
{
	std::string buffer;
	GetPlayersInRoomRequest reqPlayers = deserializeGetPlayersRequest(req.buffer);

	for (auto it : this->_handlerFactory->getRoomManager()->getRooms())
	{
		if (it.id == reqPlayers.roomId)
		{
			buffer = serializeResponse(GetPlayersInRoomResponse{
				CODE_GET_PLAYERS,
				this->_handlerFactory->getRoomManager()->getRoomById(reqPlayers.roomId).getAllUsers() 
			});
			return RequestResult{ buffer, this };
		}
	}

	buffer = serializeResponse(ErrorResponse{ "room id not found" });
	return RequestResult{ buffer, this };
}

RequestResult MenuRequestHandler::_getPersonalStats(RequestInfo req)
{
	return RequestResult{ serializeResponse(getPersonalStatsResponse{
		CODE_GET_STATS,
		this->_handlerFactory->getStatisticsManager().getUserStatistics(this->_user.username)}),
		this
	};
}

RequestResult MenuRequestHandler::_getHighScore(RequestInfo req)
{
	return RequestResult{serializeResponse(getHighScoreResponse{
		CODE_GET_HIGH_SCORE,
		this->_handlerFactory->getStatisticsManager().getTopFiveUsers()}),
		this
	};
}

RequestResult MenuRequestHandler::_joinRoom(RequestInfo req)
{
	std::string buffer;
	JoinRoomRequest jrq = deserializeJoinRoomRequest(req.buffer);

	for (auto it : this->_handlerFactory->getRoomManager()->getRooms())
	{
		if (it.id == jrq.roomId)
		{
			buffer = serializeResponse(JoinRoomResponse{ CODE_JOIN_ROOM });
			this->_handlerFactory->getRoomManager()->getRoomById(it.id).addUser(this->_user);

			return RequestResult
			{
				buffer,
				this->_handlerFactory->createRoomMemberRequestHandler(this->_user,
				this->_handlerFactory->getRoomManager()->getRoomById(it.id))
			};
		}
	}

	buffer = serializeResponse(ErrorResponse{ "room id not found" });
	return RequestResult{buffer, this};
}

RequestResult MenuRequestHandler::_createRoom(RequestInfo req)
{
	std::string buffer;
	int id = this->_handlerFactory->getRoomManager()->getRooms().size() + 1;
	CreateRoomRequest crq = deserializeCreateRoomRequest(req.buffer);

	for (auto it : this->_handlerFactory->getRoomManager()->getRooms())
	{
		if (it.name == crq.name)
		{
			buffer = serializeResponse(ErrorResponse{ "room name already exists" });
			return RequestResult{buffer, this};
		}
	}

	this->_handlerFactory->getRoomManager()->createRoom(this->_user, RoomData(unsigned int(id), crq.name, crq.maxUsers,
		crq.questionCount, crq.answerTimeout, unsigned int(0)));

	buffer = serializeResponse(CreateRoomResponse{unsigned int(id), CODE_CREATE_ROOM });

	return RequestResult{
		buffer,
		this->_handlerFactory->createRoomAdminRequestHandler(this->_user,
		this->_handlerFactory->getRoomManager()->getRoomById(id))
	};
}

RequestResult MenuRequestHandler::_addQuestion(RequestInfo req)
{
	AddQuestionRequest reqStruct = deserializerAddQuestionRequest(req.buffer);
	std::vector<std::string> answers = { reqStruct.wrongAns1, reqStruct.wrongAns2, reqStruct.wrongAns3, reqStruct.correctAns };
	this->_handlerFactory->getDb()->addQuestion(Question(reqStruct.question, answers));

	std::string buffer = serializeResponse(AddQuestionResponse{ CODE_ADD_QUESTION });

	return RequestResult{
		buffer,
		this
	};
}
