#pragma once

#include "sqlite3.h"
#include <iostream>
#include <list>
#include <vector>

struct Question
{
	Question() = default;

	Question(const Question& other)
	{
		this->question = other.question;
		this->answers = other.answers;
	}

	Question(std::string question, std::vector<std::string> answers)
	{
		this->question = question;
		this->answers = answers;
	}

	std::string question;
	std::vector<std::string> answers;
};

struct User
{
	std::string username;
	std::string password;
	std::string email;

	bool operator==(const User& user) const
	{
		return (this->username == user.username && this->password == user.password && this->email == user.email);
	}

	bool operator<(const User& user) const
	{
		return this->username < user.username;
	}
};

struct UserStats
{
	UserStats() = default;

	/// <summary>
	/// This function colculate the score of user according his stats.
	/// </summary>
	/// <returns>The score of user.</returns>
	int getScore()
	{
		if (this->sum == 0) return 0; // divide by zero exception
		return (this->correct * 10) + ((int)(this->correct / this->time) * 10) + ((int)(this->correct / this->sum) * this->games);
	}

	int sum;
	int correct;
	int wrong;
	int time;
	int games;
};

class IDatabase
{
public:
	/// <summary>
	/// Opens a new database and creates the tables in it.
	/// </summary>
	/// <returns>true if the db didnt exist</returns>
	virtual bool open() = 0;
	/// <summary>
	/// Closes the db.
	/// </summary>
	virtual void close() = 0;
	/// <summary>
	/// Checks if a user with a given name exists in db.
	/// </summary>
	/// <param name="userName"> - username to check</param>
	/// <returns>bool</returns>
	virtual bool doesUserExist(const std::string& userName) = 0;
	/// <summary>
	/// Checks if a given password is the password of a user.
	/// </summary>
	/// <param name="userName"> - a given user name</param>
	/// <param name="password"> - password to check</param>
	/// <returns>bool</returns>
	virtual bool doesPasswordMatch(const std::string& userName, const std::string& password) = 0;
	/// <summary>
	/// Adds a new user to the db.
	/// </summary>
	/// <param name="userName"> - new username</param>
	/// <param name="password"> - new password</param>
	/// <param name="email"> - new email</param>
	virtual void addNewUser(const std::string& userName, const std::string& password, const std::string& email) = 0;
	/// <summary>
	/// Gets a given amount of questions from the db.
	/// </summary>
	/// <param name="amount"> - amount of questions</param>
	/// <returns>A list of questions</returns>
	virtual std::vector<Question> getQuestions(int amount) = 0;
	/// <summary>
	/// Adds a new question to the db
	/// </summary>
	/// <param name="q"> - a given question</param>
	virtual void addQuestion(const Question& q) = 0;
	/// <summary>
	///  This function get the average time that user did for questios.
	/// </summary>
	/// <param name="username"> - the user name.</param>
	/// <returns>The average time.</returns>
	virtual float getPlayerAverageAnswerTime(std::string username) = 0;
	/// <summary>
	/// This function get the count of the correct answer.
	/// </summary>
	/// <param name="username"> - the user name.</param>
	/// <returns>The count of the correct answer</returns>
	virtual int getNumOfCorrectAnswers(std::string username) = 0;
	/// <summary>
	/// This function get the count of total answer of users.
	/// </summary>
	/// <param name="username"> - the user name.</param>
	/// <returns>The count of total answer of users.</returns>
	virtual int getNumOfTotalAnswers(std::string username) = 0;
	/// <summary>
	/// Getting the amount of games a player has played. 
	/// </summary>
	/// <param name="username"> - username</param>
	/// <returns>Amount of games</returns>
	virtual int getNumOfPlayerGames(std::string username) = 0;
	/// <summary>
	/// Getting the scores of every user on the database. 
	/// </summary>
	/// <returns>Scores of all users</returns>
	virtual std::vector<std::pair<std::string, int>> getUsersScore() = 0;
	/// <summary>
	/// Getting the stats of a user by a given username.
	/// </summary>
	/// <param name="username"> - username</param>
	/// <returns>A UserStats struct contains every stat on a player</returns>
	virtual UserStats getStatsOfUser(std::string username) = 0;
};
