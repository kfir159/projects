#include "Tool.h"

Tool::Tool(char type, int color) : _type(type), _color(color)
{
}

Tool::~Tool()
{
}

int Tool::getColor()
{
	return this->_color;
}

char Tool::getType()
{
	return this->_type;
}

int Tool::getY1(std::string coords)
{
	return coords[0] - 'a';
}

int Tool::getX1(std::string coords)
{
	return 7 - (coords[1] - '1');
}

int Tool::getY2(std::string coords)
{
	return coords[2] - 'a';
}

int Tool::getX2(std::string coords)
{
	return 7 - (coords[3] - '1');
}