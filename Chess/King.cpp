#include "King.h"

King::King(char type, int color) : Tool(type, color)
{
}

King::~King()
{
}

std::string King::CheckMove(std::string coordinates, Tool* board[][BOARD_SIZE], int currTurn)
{
	if (((abs(getX2(coordinates) - getX1(coordinates))) == 1 && (abs(getY2(coordinates) - getY1(coordinates))) == 1) || ((abs(getX2(coordinates) - getX1(coordinates))) == 1 && (abs(getY2(coordinates) - getY1(coordinates))) == 0) || ((abs(getX2(coordinates) - getX1(coordinates))) == 0 && (abs(getY2(coordinates) - getY1(coordinates))) == 1))
	{
		return VALID_MOVE;
	}
	return INVALID_MOVE_INVALID_MOVE;
}