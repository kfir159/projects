#pragma once
#include "Tool.h"

class Empty : public Tool
{
public:
	/// <summary>
	/// This function is a c'tor of Empty.
	/// </summary>
	/// <param name="type">- the type of the tool.</param>
	/// <param name="color">- the color of the type.</param>
	Empty(char type, int color);
	/// <summary>
	/// This funciton is a d'tor of Empty.
	/// </summary>
	virtual ~Empty();
	/// <summary>
	/// This funciton return 0 because empty cany move.
	/// </summary>
	/// <param name="coordinates">- the movement's string of the tool.</param>
	/// <param name="board">- the baord of game.</param>
	/// <param name="currTurn">- the current turn.</param>
	/// <returns>"0"</returns>
	virtual std::string CheckMove(std::string coordinates, Tool* board[][BOARD_SIZE], int currTurn);
};