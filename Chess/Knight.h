#pragma once
#include "Tool.h"

class Knight: public Tool
{
public:
	/// <summary>
	/// This function is a c'tor of Knight.
	/// </summary>
	/// <param name="type">- the type of the tool.</param>
	/// <param name="color">- the color of the tool.</param>
	Knight(char type, int color);
	/// <summary>
	/// This function is a d'tor of Knight.
	/// </summary>
	virtual ~Knight();
	/// <summary>
	/// This function check the move of the knight.
	/// </summary>
	/// <param name="coordinates">- the movement's string of the tool.</param>
	/// <param name="board">- the board of game.</param>
	/// <param name="currTurn">- the current turn.</param>
	/// <returns>The error code.</returns>
	virtual std::string CheckMove(std::string coordinates, Tool* board[][BOARD_SIZE], int currTurn);
};