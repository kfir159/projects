#pragma once
#include "Tool.h"

class Bishop : public Tool
{
public:
	/// <summary>
	/// This function is a c'tor of Bishop.
	/// </summary>
	/// <param name="type">- the type of the tool.</param>
	/// <param name="color">- the color of the tool.</param>
	Bishop(char type, int color);
	/// <summary>
	/// This function is a d'tor of Bishop.
	/// </summary>
	virtual ~Bishop();
	/// <summary>
	/// This Function check move of the bishop.
	/// </summary>
	/// <param name="coordinates">- the string with the movement of the tool.</param>
	/// <param name="board">- the board of game.</param>
	/// <param name="currTurn">- the current turn.</param>
	/// <returns>The error code.</returns>
	virtual std::string CheckMove(std::string coordinates, Tool* board[][BOARD_SIZE], int currTurn);
};