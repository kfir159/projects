#include "Rook.h"

Rook::Rook(char type, int color) : Tool(type, color)
{
}

Rook::~Rook()
{
}

std::string Rook::CheckMove(std::string coordinates, Tool* board[][BOARD_SIZE], int currTurn)
{
	if (abs(getY2(coordinates) - getY1(coordinates)) >= 1 && abs(getX2(coordinates) - getX1(coordinates)) >= 1)
	{
		return INVALID_MOVE_INVALID_MOVE;
	}

	if (getX1(coordinates) == getX2(coordinates))
	{
		if (getY2(coordinates) > getY1(coordinates))
		{
			for (int i = getY1(coordinates) + 1; i < getY2(coordinates); i++)
			{
				if (board[getX1(coordinates)][i]->getColor() != EMPTY_COLOR)
				{
					return INVALID_MOVE_INVALID_MOVE;
				}
			}
		}
		else
		{
			for (int i = getY2(coordinates) + 1; i < getY1(coordinates); i++)
			{
				if (board[getX1(coordinates)][i]->getColor() != EMPTY_COLOR)
				{
					return INVALID_MOVE_INVALID_MOVE;
				}
			}
		}
	}

	else if (getY1(coordinates) == getY2(coordinates))
	{
		if (getX2(coordinates) > getX1(coordinates))
		{
			for (int i = getX1(coordinates) + 1; i < getX2(coordinates); i++)
			{
				if (board[i][getY1(coordinates)]->getColor() != EMPTY_COLOR)
				{
					return INVALID_MOVE_INVALID_MOVE;
				}
			}
		}
		else
		{
			for (int i = getX2(coordinates) + 1; i < getX1(coordinates); i++)
			{
				if (board[i][getY1(coordinates)]->getColor() != EMPTY_COLOR)
				{
					return INVALID_MOVE_INVALID_MOVE;
				}
			}
		}
	}
	return VALID_MOVE;
}