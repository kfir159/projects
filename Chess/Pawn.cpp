#include "Pawn.h"

Pawn::Pawn(char type, int color) : Tool(type, color)
{
}

Pawn::~Pawn()
{
}

std::string Pawn::CheckMove(std::string coordinates, Tool* board[][BOARD_SIZE], int currTurn)
{
	if ((currTurn == BLACK_COLOR && getX1(coordinates) == 1) || (currTurn == WHITE_COLOR && getX1(coordinates) == 6))
	{
		if (getY1(coordinates) == getY2(coordinates) && board[getX2(coordinates)][getY2(coordinates)]->getColor() == EMPTY_COLOR)
		{
			if ((currTurn == BLACK_COLOR && ((getX2(coordinates) - getX1(coordinates))) > 0 && ((getX2(coordinates) - getX1(coordinates))) <= 2 && board[getX2(coordinates) - 1][getY2(coordinates)]->getColor() == EMPTY_COLOR) || (currTurn == WHITE_COLOR && ((getX2(coordinates) - getX1(coordinates))) < 0 && ((getX2(coordinates) - getX1(coordinates))) >= -2 && board[getX2(coordinates) + 1][getY2(coordinates)]->getColor() == EMPTY_COLOR))
			{
				return VALID_MOVE;
			}
		}
	}
	if (board[getX2(coordinates)][getY2(coordinates)]->getColor() != EMPTY_COLOR && board[getX2(coordinates)][getY2(coordinates)]->getColor() != currTurn)
	{
		if ((currTurn == BLACK_COLOR) && (((getX2(coordinates) - getX1(coordinates)) == 1) && (abs(getY2(coordinates) - getY1(coordinates)) == 1)))
		{
			return VALID_MOVE;
		}
		if ((currTurn == WHITE_COLOR) && (((getX2(coordinates) - getX1(coordinates)) == -1) && (abs(getY2(coordinates) - getY1(coordinates)) == 1)))
		{
			return VALID_MOVE;
		}
	}
	if (board[getX2(coordinates)][getY2(coordinates)]->getColor() == EMPTY_COLOR)
	{
		if (getY1(coordinates) == getY2(coordinates))
		{
			if ((currTurn == BLACK_COLOR) && (getX2(coordinates) - getX1(coordinates)) == 1)
			{
				return VALID_MOVE;
			}
			if ((currTurn == WHITE_COLOR) && (getX2(coordinates) - getX1(coordinates)) == -1)
			{
				return VALID_MOVE;
			}
		}
	}
	return INVALID_MOVE_INVALID_MOVE;
}