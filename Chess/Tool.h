#pragma once
#include <iostream>
#include "Board.h"
#include <cstring>
#include <cctype>
#include <iostream>
#define VALID_MOVE "0"
#define VALID_MOVE_AND_CHESS "1"
#define INVALID_MOVE_SRC_NOT_MINE "2"
#define INVALID_MOVE_DST_IS_MINE "3"
#define INVALID_MOVE_WILL_DO_CHESS_ON_ME "4"
#define INVALID_MOVE_INVALID_INDEXES "5"
#define INVALID_MOVE_INVALID_MOVE "6"
#define INVALID_MOVE_SAME_SQUARES "7"
#define VALID_MOVE_DID_MATT "8"
#define WHITE_KING 'K'
#define BLACK_KING 'k'
#define EMPTY_TOOL '#'
#define BOARD_SIZE 8
#define WHITE_COLOR 0
#define BLACK_COLOR 1
#define EMPTY_COLOR -1
class Board;

class Tool
{
public:
	/// <summary>
	/// This function is a c'tor of tool.
	/// </summary>
	/// <param name="type">- the type of the tool.</param>
	/// <param name="color">- the color of the tool.</param>
	Tool(char type, int color);
	/// <summary>
	/// This function is a d'tor of tool.
	/// </summary>
	virtual ~Tool();
	/// <summary>
	/// This function check the move of tool.
	/// </summary>
	/// <param name="coordinates">- the movement's string of the tool.</param>
	/// <param name="board">- the board of game.</param>
	/// <param name="currTurn">- the current turn.</param>
	/// <returns>The error code.</returns>
	virtual std::string CheckMove(std::string coordinates, Tool* board[][BOARD_SIZE], int currTurn) = 0;
	/// <summary>
	/// This function return the X point of source position.
	/// </summary>
	/// <param name="coords">- the movement's string of the tool.</param>
	/// <returns>The X point of source position.</returns>
	int getX1(std::string coords);
	/// <summary>
	/// This function return the Y point of source position.
	/// </summary>
	/// <param name="coords">- the movement's string of the tool.</param>
	/// <returns>The Y point of source position.</returns>
	int getY1(std::string coords);
	/// <summary>
	/// This function return the X point of destination position.
	/// </summary>
	/// <param name="coords">- the movement's string of the tool.</param>
	/// <returns>The X point of destination position.</returns>
	int getX2(std::string coords);
	/// <summary>
	/// This function return the Y point of destination position.
	/// </summary>
	/// <param name="coords">- the movement's string of the tool.</param>
	/// <returns>The Y point of destination position.</returns>
	int getY2(std::string coords);
	/// <summary>
	/// This function return the type of the tool.
	/// </summary>
	/// <returns>The type of the tool.</returns>
	char getType();
	/// <summary>
	/// This function return the color of the tool.
	/// </summary>
	/// <returns>The color of the tool.</returns>
	int getColor();

protected:
	/// <summary>
	/// The type of the tool.
	/// </summary>
	char _type;
	/// <summary>
	/// The color of the tool.
	/// </summary>
	int _color;
};
