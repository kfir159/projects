#define _CRTDBG_MAP_ALLOC  
#include <stdlib.h>  
#include <crtdbg.h>  
#include "Pipe.h"
#include <iostream>
#include <thread>
#include "Tool.h"
#include "Board.h"

int main()
{
	srand(time_t(NULL));
	Pipe p;
	bool isConnect = p.connect();
	std::string ans = "";
	while (!isConnect)
	{
		std::cout << "cant connect to graphics\nDo you try to connect again or exit? (0-try again, 1-exit)" << std::endl;
		std::cin >> ans;

		if (ans == "0")
		{
			std::cout << "trying connect again..." << std::endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else 
		{
			p.close();
			return 0;
		}
	}

	char msgToGraphics[1024] = { 0 };
	// msgToGraphics should contain the board string accord the protocol
	strcpy_s(msgToGraphics, "rnbqkbnrpppppppp################################PPPPPPPPRNBQKBNR0");
	Board* b = new Board(msgToGraphics);
	p.sendMessageToGraphics(msgToGraphics);   // send the board string
	b->PrintBoard();
	std::cout << (!b->getCurrTurn() ? "White starting!" : "Black starting!") << std::endl;
	// get message from graphics
	std::string msgFromGraphics = p.getMessageFromGraphics();

	while (msgFromGraphics != "quit")
	{
		// should handle the string the sent from graphics
		// according the protocol. Ex: e2e4           (move e2 to e4)
		std::string ans = b->Move(msgFromGraphics);
		
		if (ans == VALID_MOVE || ans == VALID_MOVE_AND_CHESS || ans == VALID_MOVE_DID_MATT)
		{
			b->setCurrTurn(!b->getCurrTurn());
		}
		
		msgToGraphics[0] = ans[0];
		msgToGraphics[1] = NULL;

		// return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);   

		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();
	}
	p.close();
	delete(b);
	return 0;
}