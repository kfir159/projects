#pragma once
#include "Tool.h"

class Pawn : public Tool
{
public:
	/// <summary>
	/// This function is a c'tor of Pawn.
	/// </summary>
	/// <param name="type">- the type of the tool.</param>
	/// <param name="color">- the color of the tool.</param>
	Pawn(char type, int color);
	/// <summary>
	/// This function is a d'tor of Pawn.
	/// </summary>
	virtual ~Pawn();
	/// <summary>
	/// This function checking the move of the pawn.
	/// </summary>
	/// <param name="coordinates">- the movement's string of the tool.</param>
	/// <param name="board">- the board of game.</param>
	/// <param name="currTurn">- the current turn.</param>
	/// <returns>The error code.</returns>
	virtual std::string CheckMove(std::string coordinates, Tool* board[][BOARD_SIZE], int currTurn);
};