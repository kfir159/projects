#include "Board.h"
#include "King.h"
#include "Queen.h"
#include "Rook.h"
#include "Knight.h"
#include "Bishop.h"
#include "Pawn.h"
#include "Empty.h"
#include <string>

Board::Board(std::string str) : _currTurn(str[str.length() - 1] - '0'), _board(), _isChessBlack(false), _isChessWhite(false)
{
	for (int i = 0; i < BOARD_SIZE * BOARD_SIZE; i++)
	{
		if (str[i] == WHITE_KING || str[i] == BLACK_KING)
		{
			this->_board[0][i] = new King(str[i], (str[i] == WHITE_KING ? WHITE_COLOR : BLACK_COLOR));
		}
		else if (str[i] == 'Q' || str[i] == 'q')
		{
			this->_board[0][i] = new Queen(str[i], (str[i] == 'Q' ? WHITE_COLOR : BLACK_COLOR));
		}
		else if (str[i] == 'R' || str[i] == 'r')
		{
			this->_board[0][i] = new Rook(str[i], (str[i] == 'R' ? WHITE_COLOR : BLACK_COLOR));
		}
		else if (str[i] == 'N' || str[i] == 'n')
		{
			this->_board[0][i] = new Knight(str[i], (str[i] == 'N' ? WHITE_COLOR : BLACK_COLOR));
		}
		else if (str[i] == 'B' || str[i] == 'b')
		{
			this->_board[0][i] = new Bishop(str[i], (str[i] == 'B' ? WHITE_COLOR : BLACK_COLOR));
		}
		else if (str[i] == 'P' || str[i] == 'p')
		{
			this->_board[0][i] = new Pawn(str[i], (str[i] == 'P' ? WHITE_COLOR : BLACK_COLOR));
		}
		else if (str[i] == EMPTY_TOOL)
		{
			this->_board[0][i] = new Empty(str[i], EMPTY_COLOR);
		}
	}
}

Board::~Board()
{
	for (int i = 0; i < (BOARD_SIZE * BOARD_SIZE); i++)
	{
		delete(this->_board[0][i]);
	}
}

int Board::getCurrTurn()
{
	return _currTurn;
}

void Board::setCurrTurn(int currTurn)
{
	this->_currTurn = currTurn;
}

void Board::PrintBoard()
{
	std::cout << std::endl;
	for (int i = 0; i < BOARD_SIZE; i++)
	{
		for (int j = 0; j < BOARD_SIZE; j++)
		{
			std::cout << this->_board[i][j]->getType() << " ";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}

std::string Board::checkGeneralMove(std::string coordinates)
{
	if (getX1(coordinates) > 7 || getX1(coordinates) < 0 || getX2(coordinates) > 7 || getX2(coordinates) < 0 || getY1(coordinates) > 7 || getY1(coordinates) < 0 || getY2(coordinates) > 7 || getY2(coordinates) < 0)
	{
		return INVALID_MOVE_INVALID_INDEXES;
	}

	if (this->_currTurn != this->_board[getX1(coordinates)][getY1(coordinates)]->getColor())
	{
		return INVALID_MOVE_SRC_NOT_MINE;
	}

	if (this->_currTurn == this->_board[getX2(coordinates)][getY2(coordinates)]->getColor())
	{
		return INVALID_MOVE_DST_IS_MINE;
	}
	
	if (getX1(coordinates) == getX2(coordinates) && getY1(coordinates) == getY2(coordinates))
	{
		return INVALID_MOVE_SAME_SQUARES;
	}

	return this->_board[getX1(coordinates)][getY1(coordinates)]->CheckMove(coordinates, this->_board, this->_currTurn);
}

std::string Board::Move(std::string coordinates)
{
	std::string answer = checkGeneralMove(coordinates);
	if (answer == VALID_MOVE)
	{
		Tool* temp = this->_board[getX2(coordinates)][getY2(coordinates)];
		this->_board[getX2(coordinates)][getY2(coordinates)] = this->_board[getX1(coordinates)][getY1(coordinates)];
		this->_board[getX1(coordinates)][getY1(coordinates)] = new Empty(EMPTY_TOOL, EMPTY_COLOR);

		if (this->_currTurn == BLACK_COLOR && this->_isChessWhite)
		{
			if (checkGeneralChess(!this->_currTurn) == VALID_MOVE_AND_CHESS)
			{
				delete(this->_board[getX1(coordinates)][getY1(coordinates)]);
				this->_board[getX1(coordinates)][getY1(coordinates)] = this->_board[getX2(coordinates)][getY2(coordinates)];
				this->_board[getX2(coordinates)][getY2(coordinates)] = temp;
				this->PrintBoard();
				return INVALID_MOVE_INVALID_MOVE;
			}
			this->_isChessWhite = false;
		}

		if (this->_currTurn == WHITE_COLOR && this->_isChessBlack)
		{
			if (checkGeneralChess(!this->_currTurn) == VALID_MOVE_AND_CHESS)
			{
				delete(this->_board[getX1(coordinates)][getY1(coordinates)]);
				this->_board[getX1(coordinates)][getY1(coordinates)] = this->_board[getX2(coordinates)][getY2(coordinates)];
				this->_board[getX2(coordinates)][getY2(coordinates)] = temp;
				this->PrintBoard();
				return INVALID_MOVE_INVALID_MOVE;
			}
			this->_isChessBlack = false;
		}

		if (!this->_isChessBlack && !this->_isChessWhite)
		{
			if (checkGeneralChess(!this->_currTurn) == VALID_MOVE_AND_CHESS)
			{
				delete(this->_board[getX1(coordinates)][getY1(coordinates)]);
				this->_board[getX1(coordinates)][getY1(coordinates)] = this->_board[getX2(coordinates)][getY2(coordinates)];
				this->_board[getX2(coordinates)][getY2(coordinates)] = temp;
				this->PrintBoard();
				return INVALID_MOVE_WILL_DO_CHESS_ON_ME;
			}
		}

		if (this->_currTurn == WHITE_COLOR && getX2(coordinates) == 0)
		{
			delete(this->_board[getX2(coordinates)][getY2(coordinates)]);
			this->_board[getX2(coordinates)][getY2(coordinates)] = new Queen('Q', 0);
		}
		else if (this->_currTurn == BLACK_COLOR && getX2(coordinates) == 7)
		{
			delete(this->_board[getX2(coordinates)][getY2(coordinates)]);
			this->_board[getX2(coordinates)][getY2(coordinates)] = new Queen('q', 1);
		}

		if (this->_board[getX2(coordinates)][getY2(coordinates)]->CheckMove(getCoordinates(this->_currTurn, getX2(coordinates), getY2(coordinates)), this->_board, this->_currTurn) == VALID_MOVE)
		{
			answer = VALID_MOVE_AND_CHESS;
		}
		
		if (answer == VALID_MOVE_AND_CHESS)
		{
			this->_currTurn == BLACK_COLOR ? this->_isChessBlack = true : this->_isChessWhite = true;
		}

		Tool* backup = this->_board[getX2(coordinates)][getY2(coordinates)];
		this->_board[getX2(coordinates)][getY2(coordinates)] = temp;
		delete(this->_board[getX2(coordinates)][getY2(coordinates)]);
		this->_board[getX2(coordinates)][getY2(coordinates)] = backup;
	}
	
	this->PrintBoard();
	return answer;
}

std::string Board::getCoordinates(int currTurn, int x, int y)
{
	std::string coordinates = "";
	coordinates += (char)(y + 'a');
	coordinates += (char)((7-x) + '1');
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			if ((this->_board[i][j]->getType() == 'k' && currTurn == 0) || (this->_board[i][j]->getType() == 'K' && currTurn == 1))
			{
				coordinates += (char)(j + 'a');
				coordinates += (char)((7-i) + '1');
				break;
			}
		}
	}
	return coordinates;
}

std::string Board::checkGeneralChess(int currTurn)
{
	for (int i = 0; i < BOARD_SIZE; i++)
	{
		for (int j = 0; j < BOARD_SIZE; j++)
		{
			if (this->_board[i][j]->getColor() == currTurn)
			{
				if (this->_board[i][j]->CheckMove(getCoordinates(currTurn, i, j), this->_board, this->_currTurn) == VALID_MOVE)
				{
					return "1";
				}
			}
		}
	}
	return "0";
}

int Board::getY1(std::string coords)
{
	return coords[0] - 'a';
}

int Board::getX1(std::string coords)
{
	return 7 - (coords[1] - '1');
}

int Board::getY2(std::string coords)
{
	return coords[2] - 'a';
}

int Board::getX2(std::string coords)
{
	return 7 - (coords[3] - '1');
}