#pragma once
#include "Tool.h"

class King : public Tool
{
public:
	/// <summary>
	/// This function is a c'tor of King.
	/// </summary>
	/// <param name="type">- the type of the tool.</param>
	/// <param name="color">- the color of the tool.</param>
	King(char type, int color);
	/// <summary>
	/// This function is a d'tor of King.
	/// </summary>
	virtual ~King();
	/// <summary>
	/// This function check if the valid of the movement's king.
	/// </summary>
	/// <param name="coordinates">- the movement's string of the tool.</param>
	/// <param name="board">- the baord of the game.</param>
	/// <param name="currTurn">- the current turn.</param>
	/// <returns>The error code.</returns>
	virtual std::string CheckMove(std::string coordinates, Tool* board[][BOARD_SIZE], int currTurn);
};