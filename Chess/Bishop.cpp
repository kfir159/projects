#include "Bishop.h"

Bishop::Bishop(char type, int color) : Tool(type, color)
{
}

Bishop::~Bishop()
{
}

std::string Bishop::CheckMove(std::string coordinates, Tool* board[][BOARD_SIZE], int currTurn)
{
	int i = 0, j = 0;
	if (abs(getY2(coordinates) - getY1(coordinates)) != abs(getX2(coordinates) - getX1(coordinates)))
	{
		return INVALID_MOVE_INVALID_MOVE;
	}

	if (getX2(coordinates) > getX1(coordinates) && getY2(coordinates) > getY1(coordinates))
	{
		for (i = getX1(coordinates) + 1, j = getY1(coordinates) + 1; i < getX2(coordinates); i++, j++)
		{
			if (board[i][j]->getColor() != -1)
			{
				return INVALID_MOVE_INVALID_MOVE;
			}
		}
	}

	else if (getX2(coordinates) < getX1(coordinates) && getY2(coordinates) > getY1(coordinates))
	{
		for (i = getX1(coordinates) - 1, j = getY1(coordinates) + 1; i > getX2(coordinates); i--, j++)
		{
			if (board[i][j]->getColor() != -1)
			{
				return INVALID_MOVE_INVALID_MOVE;
			}
		}
	}

	else if (getX2(coordinates) < getX1(coordinates) && getY2(coordinates) < getY1(coordinates))
	{
		for (i = getX1(coordinates) - 1, j = getY1(coordinates) - 1; i > getX2(coordinates); i--, j--)
		{
			if (board[i][j]->getColor() != -1)
			{
				return INVALID_MOVE_INVALID_MOVE;
			}
		}
	}

	else if (getX2(coordinates) > getX1(coordinates) && getY2(coordinates) < getY1(coordinates))
	{
		for (i = getX1(coordinates) + 1, j = getY1(coordinates) - 1; i < getX2(coordinates); i++, j--)
		{
			if (board[i][j]->getColor() != -1)
			{
				return INVALID_MOVE_INVALID_MOVE;
			}
		}
	}

	return VALID_MOVE;
}