#pragma once
#include "Tool.h"
#define BOARD_SIZE 8
class Tool;

class Board
{
public:
	/// <summary>
	/// The c'tor of the board.
	/// </summary>
	/// <param name="str">- the boot string of the board</param>
	Board(std::string str);
	/// <summary>
	/// This function is a d'tor of the board.
	/// </summary>
	~Board();
	/// <summary>
	/// This function return the current turn.
	/// </summary>
	/// <returns>The value of "currTurn".</returns>
	int getCurrTurn();
	/// <summary>
	/// This function set new value to the variable "currTurn".
	/// </summary>
	/// <param name="currTurn">The new value of currTurn.</param>
	void setCurrTurn(int currTurn);
	/// <summary>
	/// This function print the board.
	/// </summary>
	void PrintBoard();
	/// <summary>
	/// This function check if the move is valid. 
	/// </summary>
	/// <param name="coordinates">- the movement's string of the tool.</param>
	/// <returns>The error code.</returns>
	std::string checkGeneralMove(std::string coordinates);
	/// <summary>
	/// This function run about the board and check if some tool did a chess.
	/// </summary>
	/// <param name="currTurn">- the current turn.</param>
	/// <returns>The error code.</returns>
	std::string checkGeneralChess(int currTurn);
	/// <summary>
	/// This function return the X point of source position.
	/// </summary>
	/// <param name="coords">- the movement's string of the tool.</param>
	/// <returns>The X point of source position.</returns>
	int getX1(std::string coords);
	/// <summary>
	/// This function return the Y point of source position.
	/// </summary>
	/// <param name="coords">- the movement's string of the tool.</param>
	/// <returns>The Y point of source position.</returns>
	int getY1(std::string coords);
	/// <summary>
	/// This function return the X point of destination position.
	/// </summary>
	/// <param name="coords">- the movement's string of the tool.</param>
	/// <returns>The X point of destination position.</returns>
	int getX2(std::string coords);
	/// <summary>
	/// This function return the Y point of destination position.
	/// </summary>
	/// <param name="coords">- the movement's string of the tool.</param>
	/// <returns>The Y point of destination position.</returns>
	int getY2(std::string coords);
	/// <summary>
	/// This function check the move and if this valid, move the tool.
	/// </summary>
	/// <param name="coordinates">- the movement's string of the tool.</param>
	/// <returns>The error code.</returns>
	std::string Move(std::string coordinates);
	/// <summary>
	/// This function return the string of movement's tool to the king to check chess.
	/// </summary>
	/// <param name="currTurn">The current turn.</param>
	/// <param name="x">The X point of the tool.</param>
	/// <param name="y">The Y point of the tool.</param>
	/// <returns>The coordinates string.</returns>
	std::string getCoordinates(int currTurn, int x, int y);

private:
	/// <summary>
	/// The board.
	/// </summary>
	Tool* _board[BOARD_SIZE][BOARD_SIZE];
	/// <summary>
	/// The curr turn.
	/// </summary>
	int _currTurn;
	/// <summary>
	/// If the black did chess.
	/// </summary>
	bool _isChessBlack;
	/// <summary>
	/// If the white did chess.
	/// </summary>
	bool _isChessWhite;
};