#include "Knight.h"

Knight::Knight(char type, int color) : Tool(type, color)
{
}

Knight::~Knight()
{
}

std::string Knight::CheckMove(std::string coordinates, Tool* board[][BOARD_SIZE], int currTurn)
{
	return (abs((getX2(coordinates) - getX1(coordinates)) * ((getY2(coordinates) - getY1(coordinates)))) == 2) ? VALID_MOVE : INVALID_MOVE_INVALID_MOVE;
}