#pragma once
#include "Tool.h"

class Queen : public Tool
{
public:
	/// <summary>
	/// This function is a c'tor of Queen.
	/// </summary>
	/// <param name="type">- the type of the tool.</param>
	/// <param name="color">- the color of the tool.</param>
	Queen(char type, int color);
	/// <summary>
	/// This function is a d'tor of Queen.
	/// </summary>
	virtual ~Queen();
	/// <summary>
	/// This function check the move of the Queen.
	/// </summary>
	/// <param name="coordinates">- the movement's string of the tool.</param>
	/// <param name="board">- the board of game.</param>
	/// <param name="currTurn">- the current turn.</param>
	/// <returns>The error code.</returns>
	virtual std::string CheckMove(std::string coordinates, Tool* board[][BOARD_SIZE], int currTurn);
};