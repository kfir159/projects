import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * This class is the class of the algorithm that add the event.
 */
public class CalendarManager
{
    public static Map<String, DayInCalendar> calendarEvents;
    public static String changeEvent;

    /**
     * This function is a constructor of the class CalendarManager.
     */
    public CalendarManager(){

    }

    /**
     * This function is the manager of the algorithm, call to the other functions.
     * @param newEvent The event that need to add.
     * @return The status code that back.
     */
    public int theManager(AIEvent newEvent)
    {
        int status = Constants.NO_ADDED;
        if (newEvent.getSplitLevel() == Constants.NO_SPLIT) {
            if (addEvent(newEvent) == Constants.NO_ADDED) {
                status = checkPriority(newEvent);
            }
            else {
                status = Constants.ADDED;
            }
        }
        else if(newEvent.getSplitLevel() >= Constants.MAYBE_SPLIT && newEvent.getSplitLevel() <= Constants.NEED_SPLIT)
            status = needsSplit(newEvent);
        connectTwoCloseEvents(newEvent.date, newEvent.getDeadline());
        return status;
    }

    /**
     * This function do split to the event if its necessary.
     * @param newEvent The event that needs split.
     * @return The status code the back.
     */
    public int needsSplit(AIEvent newEvent)
    {
        if(newEvent.getSplitLevel() == Constants.MAYBE_SPLIT && addEvent(newEvent) == Constants.ADDED)
        {
            return Constants.ADDED;
        }
        float currDuration = Integer.valueOf(newEvent.duration.split(":")[0]) + (float)Integer.valueOf(newEvent.duration.split(":")[1]) / Constants.MINUTES_IN_HOUR;
        float sizeOfSplit = Integer.valueOf(newEvent.getLenOfSplit().split(":")[0]) + (float)Integer.valueOf(newEvent.getLenOfSplit().split(":")[1]) / Constants.MINUTES_IN_HOUR;
        int status = Constants.ADDED;
        for(; currDuration >= sizeOfSplit && status != Constants.NO_ADDED; currDuration = getMinusDuration(currDuration, newEvent.getLenOfSplit()))
        {
            AIEvent toAdd = new AIEvent(newEvent);
            toAdd.duration = newEvent.getLenOfSplit();
            toAdd.setSplitLevel(Constants.SPLIT_UP);
            toAdd.setLenOfSplit(String.valueOf(Constants.SPLIT_UP));
            status = addEvent(toAdd);
            if(status == Constants.NO_ADDED)
                status = checkPriority(toAdd);
        }
        if (currDuration == 0 && status != Constants.NO_ADDED)
            return status;
        if(currDuration < sizeOfSplit && status != Constants.NO_ADDED)
        {
            AIEvent toAdd = new AIEvent(newEvent);
            int hour = (int) currDuration;
            int minutes = (int) ((currDuration - hour) * Constants.MINUTES_IN_HOUR);
            toAdd.duration = hour + ":" + minutes;
            if(addEvent(toAdd) == Constants.NO_ADDED)
                return checkPriority(newEvent);
            return Constants.ADDED;
        }
        return Constants.NO_ADDED;
    }

    /**
     * This function search the first place that empty and add the event to there.
     * @param newEvent The event to add.
     * @return The status code.
     */
    public int addEvent (AIEvent newEvent)
    {
        calendarEvents = new TreeMap<>();
        CalendarAPI.loadSomeDays(newEvent.date, newEvent.getDeadline());
        for(Map.Entry<String, DayInCalendar> day : calendarEvents.entrySet())
        {
            String lastHour = "10:00:00";
            String currDate = day.getKey();
            for (Map.Entry<String, MyEvent> event : day.getValue().GetDay().entrySet())
            {
                if(checkDuration(event.getValue().startHour, newEvent, lastHour))
                {
                    return createNewAIEvent(newEvent, lastHour, currDate);
                }
                else
                {
                    lastHour = event.getValue().endHour;
                }
            }
            String finishDay = "22:00:00";
            if(checkDuration(finishDay, newEvent, lastHour))
            {
                return createNewAIEvent(newEvent, lastHour, currDate);
            }
        }
        return Constants.NO_ADDED;
    }

    /**
     * This function check if it can change place of event for other event the with deadline closer or priority bigger.
     * @param newEvent The new event to add.
     * @return The status code.
     */
    public int checkPriority(AIEvent newEvent)
    {
        AIEvent smallerPriority = new AIEvent();
        smallerPriority.setPriority(Constants.MAX_PRIORITY);
        for(Map.Entry<String, DayInCalendar> day : calendarEvents.entrySet()) {
            for (Map.Entry<String, MyEvent> event : day.getValue().GetDay().entrySet()) {
                if(event.getValue() instanceof AIEvent && newEvent.duration.compareTo(event.getValue().duration) <= 0)
                {
                    AIEvent aiEvent = new AIEvent((AIEvent)event.getValue());
                    if(addEvent(aiEvent) == Constants.ADDED)
                    {
                        CalendarAPI.deleteEvent(event.getValue());
                        addEvent(newEvent);
                        changeEvent = aiEvent.title;
                        return Constants.ADDED_AFTER_CHANGE;
                    }
                    if(smallerPriority.getPriority() > ((AIEvent) event.getValue()).getPriority())
                    {
                        smallerPriority = new AIEvent((AIEvent)event.getValue());
                    }
                }
            }
        }
        if(newEvent.getPriority() > smallerPriority.getPriority())
        {
            CalendarAPI.deleteEvent(smallerPriority);
            addEvent(newEvent);
            changeEvent = smallerPriority.title;
            return Constants.ADDED_AFTER_DELETE;
        }
        return Constants.NO_ADDED;
    }

    /**
     * This function create new AI event to add to the calendar.
     * @param newEvent The new event to add.
     * @param lastHour The hour of the event.
     * @param currDate The date of the event.
     * @return The status code.
     */
    public int createNewAIEvent(AIEvent newEvent, String lastHour, String currDate)
    {
        newEvent.startHour = lastHour;
        newEvent.endHour = String.format("%02d:%02d",
                (Integer.parseInt(newEvent.startHour.split(":")[0]) + Integer.parseInt(newEvent.duration.split(":")[0])) +
                ((Integer.parseInt(newEvent.startHour.split(":")[1]) + Integer.parseInt(newEvent.duration.split(":")[1])) >= Constants.MINUTES_IN_HOUR ? 1 : 0),
                (Integer.parseInt(newEvent.startHour.split(":")[1]) + Integer.parseInt(newEvent.duration.split(":")[1])) % Constants.MINUTES_IN_HOUR);
        newEvent.date = currDate;
        CalendarAPI.addAIEventToCalendar(newEvent);
        DayInCalendar updatedDay = calendarEvents.get(newEvent.date);
        updatedDay.addEvent(newEvent);
        calendarEvents.put(newEvent.date, updatedDay);
        return Constants.ADDED;
    }

    /**
     * This function connect some same events that close to one event.
     * @param startDate The start date to check.
     * @param endDate The end date to check.
     */
    public void connectTwoCloseEvents(String startDate, String endDate)
    {
        calendarEvents = new TreeMap<>();
        CalendarAPI.loadSomeDays(startDate, endDate);
        for(Map.Entry<String, DayInCalendar> day : calendarEvents.entrySet()) {
            AIEvent prevEvent = null;
            for (Map.Entry<String, MyEvent> event : day.getValue().GetDay().entrySet()) {
                if(event.getValue() instanceof AIEvent) {
                    if (prevEvent != null && prevEvent.title.equals(event.getValue().title)) {
                        AIEvent updatedEvent = new AIEvent(prevEvent);
                        updatedEvent.endHour = event.getValue().endHour;
                        updatedEvent.duration = sumDurations(prevEvent.duration, event.getValue().duration);
                        CalendarAPI.deleteEvent(prevEvent);
                        CalendarAPI.deleteEvent(event.getValue());
                        CalendarAPI.addAIEventToCalendar(updatedEvent);
                        prevEvent = updatedEvent;
                    }
                    else {
                        prevEvent = (AIEvent) event.getValue();
                    }
                }
            }
        }
    }

    /**
     * This function calculate the sum of two durations.
     * @param firstDur The first duration.
     * @param secondDur The second duration.
     * @return String with the result.
     */
    public String sumDurations(String firstDur, String secondDur)
    {
        LocalTime time1 = LocalTime.parse(firstDur, DateTimeFormatter.ofPattern("HH:mm"));
        LocalTime time2 = LocalTime.parse(secondDur, DateTimeFormatter.ofPattern("HH:mm"));

        // Add the durations and format the result as HH:mm
        LocalTime sum = time1.plusHours(time2.getHour()).plusMinutes(time2.getMinute());
        String result = sum.format(DateTimeFormatter.ofPattern("HH:mm"));
        return result;
    }

    /**
     * This function check if the event can be between the next event and the last event according his duration.
     * @param currHour The hour to check.
     * @param newEvent The event that want add.
     * @param lastHour The last hour that check.
     * @return True or false if that place is appropriate.
     */
    public boolean checkDuration(String currHour, AIEvent newEvent, String lastHour)
    {
        return ((Float.valueOf(currHour.split(":")[0]) +
                Float.valueOf(currHour.split(":")[1]) / Constants.MINUTES_IN_HOUR) -
                (Float.valueOf(lastHour.split(":")[0]) + Float.valueOf(lastHour.split(":")[1]) / Constants.MINUTES_IN_HOUR) >=
                Float.valueOf(newEvent.duration.split(":")[0]) + Float.valueOf(newEvent.duration.split(":")[1]) / Constants.MINUTES_IN_HOUR);
    }

    /**
     * This function do minus to the duration according the len of split.
     * @param currDuration The curr size of duration.
     * @param duration The len of split.
     * @return The duration after the minus.
     */
    public float getMinusDuration(float currDuration, String duration)
    {
        float durationInt = getFloatFromString(duration);
        return currDuration - durationInt;
    }

    /**
     * This function add regular event and check if at the time of the event there are another events.
     * @param myEvent The event to add.
     * @return The status code.
     */
    public int addMyEvent(MyEvent myEvent)
    {
        calendarEvents = new TreeMap<>();
        CalendarAPI.loadSomeDays(myEvent.date, myEvent.date);
        ArrayList<AIEvent> toDelete = new ArrayList<>();
        int status = Constants.ADDED;
        for(Map.Entry<String, DayInCalendar> day : calendarEvents.entrySet()) {
            for (Map.Entry<String, MyEvent> event : day.getValue().GetDay().entrySet()) {
                if (getFloatFromString(myEvent.startHour) >= getFloatFromString(event.getValue().startHour) &&
                        getFloatFromString(myEvent.startHour) < getFloatFromString(event.getValue().endHour)) {
                    if(event.getValue() instanceof AIEvent) {
                        toDelete.add((AIEvent) event.getValue());
                        status = Constants.ADDED_AFTER_DELETE;
                    }
                    else {
                        changeEvent = event.getValue().title;
                        return Constants.NO_ADDED;
                    }
                }
                else if (getFloatFromString(myEvent.startHour) <= getFloatFromString(event.getValue().startHour) &&
                        getFloatFromString(myEvent.endHour) >= getFloatFromString(event.getValue().endHour)) {
                    CalendarAPI.deleteEvent(event.getValue());
                    if(event.getValue() instanceof AIEvent) {
                        toDelete.add((AIEvent) event.getValue());
                        status = Constants.ADDED_AFTER_DELETE;
                    }
                    else {
                        changeEvent = event.getValue().title;
                        return Constants.NO_ADDED;
                    }
                }
                if (getFloatFromString(myEvent.endHour) < getFloatFromString(event.getValue().startHour) &&
                        getFloatFromString(myEvent.endHour) >= getFloatFromString(event.getValue().endHour)) {
                    if(event.getValue() instanceof AIEvent) {
                        toDelete.add((AIEvent) event.getValue());
                        status = Constants.ADDED_AFTER_DELETE;
                    }
                    else {
                        changeEvent = event.getValue().title;
                        return Constants.NO_ADDED;
                    }
                }
                else if (getFloatFromString(myEvent.startHour) >= getFloatFromString(event.getValue().startHour) &&
                        getFloatFromString(myEvent.endHour) <= getFloatFromString(event.getValue().endHour)) {
                    CalendarAPI.deleteEvent(event.getValue());
                    if(event.getValue() instanceof AIEvent) {
                        toDelete.add((AIEvent) event.getValue());
                        status = Constants.ADDED_AFTER_DELETE;
                    }
                    else {
                        changeEvent = event.getValue().title;
                        return Constants.NO_ADDED;
                    }
                }
            }
        }
        CalendarAPI.addMyEventToCalendar(myEvent);
        if(status == Constants.ADDED_AFTER_DELETE) {
            changeEvent = "";
            for (AIEvent aiEvent : toDelete) {
                AIEvent toAdd = new AIEvent(aiEvent);
                addEvent(toAdd);
                CalendarAPI.deleteEvent(aiEvent);
                changeEvent += aiEvent.title + ", ";
            }
        }
        return status;
    }

    /**
     * This function cast the struct "HH:mm" to float.
     * @param string The string in the format HH:mm that need cast.
     * @return The float value.
     */
    public float getFloatFromString(String string)
    {
        return Integer.valueOf(string.split(":")[0]) + (float)Integer.valueOf(string.split(":")[1]) / Constants.MINUTES_IN_HOUR;
    }
}
