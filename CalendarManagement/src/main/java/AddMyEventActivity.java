import javax.swing.*;
import java.awt.*;

/**
 * This class is GUI of add regular event.
 */
public class AddMyEventActivity
{
    /**
     * This function create the screen that get info about the regular event that want to add.
     * @param menuFrame The frame of menu to show him after show the events.
     */
    public AddMyEventActivity(JFrame menuFrame)
    {
        JFrame frame = new JFrame("Calendar Management");
        frame.setSize(Constants.FRAME_WIDTH, Constants.FRAME_HEIGHT);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel titleLabel = new JLabel("Title:");
        JTextField titleField = new JTextField(Constants.FIELD_WIDTH);

        JLabel dateLabel = new JLabel("Date in format yyyy-MM-dd:");
        JTextField dateField = new JTextField(Constants.FIELD_WIDTH);

        JLabel hourLabel = new JLabel("Start hour in format HH:mm:");
        JTextField hourField = new JTextField(Constants.FIELD_WIDTH);

        JLabel endLabel = new JLabel("End hour in format HH:mm:");
        JTextField endField = new JTextField(Constants.FIELD_WIDTH);

        // Create a panel to hold the form components and set its layout
        JPanel formPanel = new JPanel(new GridLayout(Constants.GRID_LAYOUT_4, Constants.GRID_LAYOUT_2, Constants.GRID_LAYOUT_5, Constants.GRID_LAYOUT_5));
        formPanel.add(titleLabel);
        formPanel.add(titleField);
        formPanel.add(dateLabel);
        formPanel.add(dateField);
        formPanel.add(hourLabel);
        formPanel.add(hourField);
        formPanel.add(endLabel);
        formPanel.add(endField);
        formPanel.setBorder(BorderFactory.createEmptyBorder(Constants.EMPTY_BORDER_10, Constants.EMPTY_BORDER_10, Constants.EMPTY_BORDER_10, Constants.EMPTY_BORDER_10));

        // Create a button to submit the form
        JButton submitButton = new JButton("Submit");
        submitButton.addActionListener(actionEvent -> {
            // Get the data from the form fields
            frame.setVisible(false);

            if(!CheckValidInput.checkTitle(titleField.getText()))
                msgToUser("The title is empty!", menuFrame);
            else if(!CheckValidInput.checkDate(dateField.getText()))
                msgToUser("The date doesn't in format.", menuFrame);
            else if(!CheckValidInput.checkHour(hourField.getText()))
                msgToUser("The start hour doesn't in format.", menuFrame);
            else if(!CheckValidInput.checkEndHour(hourField.getText(), endField.getText()))
                msgToUser("The end hour doesn't in format.", menuFrame);
            else {
                MyEvent newEvent = new MyEvent();
                newEvent.title = titleField.getText();
                newEvent.date = dateField.getText();
                newEvent.startHour = hourField.getText();
                newEvent.endHour = endField.getText();

                CalendarManager calendarManager = new CalendarManager();
                int status = calendarManager.addMyEvent(newEvent);
                if(status == Constants.ADDED)
                    msgToUser("The event '" + newEvent.title + "' created in successfully.", menuFrame);
                else if(status == Constants.ADDED_AFTER_DELETE)
                    msgToUser("The event '" + newEvent.title + "' created in successfully after delete or change the event/s: " +
                            CalendarManager.changeEvent.substring(Constants.FIRST_CHAR, CalendarManager.changeEvent.length() - Constants.TWO_LAST_CHARS) + ".", menuFrame);
                else
                    msgToUser("The event '" + newEvent.title + "' can't be created because you already have the event: " + CalendarManager.changeEvent + ".", menuFrame);
            }
        });

        // Create a panel to hold the submit button and set its layout
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        buttonPanel.add(submitButton);

        // Create a panel to hold the form and button panels and set its layout
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(formPanel, BorderLayout.CENTER);
        mainPanel.add(buttonPanel, BorderLayout.SOUTH);

        // Add the main panel to the frame
        frame.add(mainPanel);

        // Pack and show the frame
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    /**
     * This function show screen with a message to the user after his try to add AI event.
     * @param msg The message to show.
     * @param menuFrame The menu screen that should show after close the message.
     */
    public void msgToUser(String msg, JFrame menuFrame)
    {
        JFrame frame = new JFrame("Calendar Management");
        frame.setSize(Constants.FRAME_WIDTH, Constants.FRAME_HEIGHT);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel msgLabel = new JLabel(msg);
        JPanel formPanel = new JPanel(new GridLayout(Constants.GRID_LAYOUT_4, Constants.GRID_LAYOUT_2, Constants.GRID_LAYOUT_5, Constants.GRID_LAYOUT_5));
        formPanel.add(msgLabel);

        formPanel.setBorder(BorderFactory.createEmptyBorder(Constants.EMPTY_BORDER_10, Constants.EMPTY_BORDER_10, Constants.EMPTY_BORDER_10, Constants.EMPTY_BORDER_10));

        JButton submitButton = new JButton("Close");
        submitButton.addActionListener(actionEvent -> {
            frame.setVisible(false);
            menuFrame.setVisible(true);
        });

        // Create a panel to hold the submit button and set its layout
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        buttonPanel.add(submitButton);

        // Create a panel to hold the form and button panels and set its layout
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(formPanel, BorderLayout.CENTER);
        mainPanel.add(buttonPanel, BorderLayout.SOUTH);

        // Add the main panel to the frame
        frame.add(mainPanel);

        // Pack and show the frame
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
