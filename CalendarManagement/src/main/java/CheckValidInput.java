/**
 * This class has functions that check the input from the user from the GUI.
 */
public class CheckValidInput {
    /**
     * This function check if the user wrote title.
     * @param title The title that the user wrote.
     * @return True or false if the user wrote title to the event.
     */
    public static boolean checkTitle(String title)
    {
        return !title.equals("");
    }

    /**
     * This function check if the date is valid.
     * @param date The date to check.
     * @return True or false if the user wrote date in the format to the event.
     */
    public static boolean checkDate(String date)
    {
        if(date.split("-").length != Constants.SOME_FIELDS_DATE)
            return false;
        if(date.split("-")[0].length() != Constants.LEN_YEAR_FIELD)
            return false;
        if(!date.split("-")[0].chars().allMatch( Character::isDigit))
            return false;
        if(Integer.parseInt(date.split("-")[0]) < Constants.MIN_YEAR || Integer.valueOf(date.split("-")[0]) > Constants.MAX_YEAR)
            return false;
        if (date.split("-")[1].length() != Constants.LEN_MONTH_FIELD)
            return false;
        if(!date.split("-")[1].chars().allMatch( Character::isDigit))
            return false;
        if (date.split("-")[2].length() != Constants.LEN_DAY_FIELD)
            return false;
        if(!date.split("-")[2].chars().allMatch( Character::isDigit))
            return false;
        if(Integer.parseInt(date.split("-")[1]) < Constants.JANUARY || Integer.valueOf(date.split("-")[1]) > Constants.DECEMBER)
            return false;
        if(Integer.parseInt(date.split("-")[1]) == Constants.JANUARY || Integer.valueOf(date.split("-")[1]) == Constants.MARCH ||
                Integer.parseInt(date.split("-")[1]) == Constants.MAY || Integer.valueOf(date.split("-")[1]) == Constants.JULY ||
                Integer.parseInt(date.split("-")[1]) == Constants.AUGUST || Integer.valueOf(date.split("-")[1]) == Constants.OCTOBER ||
                Integer.parseInt(date.split("-")[1]) == Constants.DECEMBER)
        {
            if(Integer.parseInt(date.split("-")[2]) < Constants.MIN_DAY || Integer.valueOf(date.split("-")[2]) > Constants.MAX_DAYS_31)
                return false;
        }
        else if (Integer.parseInt(date.split("-")[1]) == Constants.APRIL || Integer.valueOf(date.split("-")[1]) == Constants.JUNE ||
                Integer.parseInt(date.split("-")[1]) == Constants.SEPTEMBER || Integer.valueOf(date.split("-")[1]) == Constants.NOVEMBER)
        {
            if(Integer.parseInt(date.split("-")[2]) < Constants.MIN_DAY || Integer.valueOf(date.split("-")[2]) > Constants.MAX_DAYS_30)
                return false;
        }
        else {
            if(Integer.parseInt(date.split("-")[2]) < Constants.MIN_DAY || Integer.valueOf(date.split("-")[2]) > Constants.MAX_DAYS_28)
                return false;
        }
        return true;
    }

    /**
     * This function check if the deadline that the user wrote is legal.
     * @param date The start date that the user wrote to check if the deadline is after this date.
     * @param deadline The deadline to check.
     * @return True or false if the user wrote the deadline in the format to the event.
     */
    public static boolean checkDeadline(String date, String deadline)
    {
        if(!checkDate(deadline))
            return false;
        if(Integer.parseInt(date.split("-")[0]) > Integer.parseInt(deadline.split("-")[0]))
            return false;
        if(Integer.parseInt(date.split("-")[0]) == Integer.parseInt(deadline.split("-")[0]) &&
                Integer.parseInt(date.split("-")[1]) > Integer.parseInt(deadline.split("-")[1]))
            return false;
        if(Integer.parseInt(date.split("-")[0]) == Integer.parseInt(deadline.split("-")[0]) &&
                Integer.parseInt(date.split("-")[1]) == Integer.parseInt(deadline.split("-")[1]) &&
                Integer.parseInt(date.split("-")[2]) > Integer.parseInt(deadline.split("-")[2]))
            return false;
        return true;
    }

    /**
     * This function check the hour that the user wrote if it in the format.
     * @param hour The hour to check.
     * @return True or false if the user wrote the hour in the format to the event.
     */
    public static boolean checkHour(String hour)
    {
        if(hour.split(":").length != Constants.SOME_FIELDS_HOUR)
            return false;
        if(hour.split(":")[0].length() != Constants.LEN_HOUR_FIELD)
            return false;
        if(!hour.split(":")[0].chars().allMatch( Character::isDigit))
            return false;
        if(Integer.parseInt(hour.split(":")[0]) < Constants.MIN_HOUR || Integer.valueOf(hour.split(":")[0]) >= Constants.HOURS_IN_DAY)
            return false;
        if (hour.split(":")[1].length() != Constants.LEN_MINUTES_FIELD)
            return false;
        if(!hour.split(":")[1].chars().allMatch( Character::isDigit))
            return false;
        if(Integer.parseInt(hour.split(":")[1]) < Constants.MIN_MINUTE || Integer.valueOf(hour.split(":")[1]) >= Constants.MINUTES_IN_HOUR)
            return false;
        return true;
    }

    /**
     * This function check if the end hour of the event that the user wrote is valid.
     * @param startHour The start hour to check if the end hour is after.
     * @param endHour The end hour to check.
     * @return True or false if the user wrote the end hour in the format adn legal to the event.
     */
    public static boolean checkEndHour(String startHour, String endHour)
    {
        if(!checkHour(endHour))
            return false;
        if(Integer.parseInt(startHour.split(":")[0]) > Integer.parseInt(endHour.split(":")[0]))
            return false;
        if(Integer.parseInt(startHour.split(":")[0]) == Integer.parseInt(endHour.split(":")[0]) &&
                Integer.parseInt(startHour.split(":")[1]) > Integer.parseInt(endHour.split(":")[1]))
            return false;
        return true;
    }

    /**
     * This function check if the split-level that the user wrote is a digit and between 1 and 3.
     * @param splitLevel The split-level that the user wrote to check.
     * @return True or false if the user wrote the split-level legal to the event.
     */
    public static boolean checkSplitLevel(String splitLevel)
    {
        return splitLevel.chars().allMatch( Character::isDigit) && Integer.parseInt(splitLevel) >= Constants.NO_SPLIT &&
                Integer.parseInt(splitLevel) <= Constants.NEED_SPLIT;
    }

    /**
     * This function check if the len of split legal, or in the format or empty if the split-level is 1.
     * @param splitLevel The split-level that the user wrote.
     * @param lenOfSplit The len of split to check.
     * @return True or false if the user wrote the len of split in the format and legal to the event.
     */
    public static boolean checkLenOfSplit(String splitLevel, String lenOfSplit)
    {
        if(Integer.parseInt(splitLevel) == Constants.NO_SPLIT)
            return (lenOfSplit.equals(""));
        return checkHour(lenOfSplit);
    }

    /**
     * This function check if the priority that the user wrote is digit and between 1 and 5.
     * @param priority The priority that the user wrote.
     * @return True or false if the user wrote the priority valid to the event.
     */
    public static boolean checkPriority(String priority)
    {
        return priority.chars().allMatch( Character::isDigit) && Integer.parseInt(priority) >= Constants.MIN_PRIORITY &&
                Integer.parseInt(priority) <= Constants.MAX_PRIORITY;
    }

    /**
     * This function check if the input in read events is legal.
     * @param data The data from the user.
     * @return True of false if the input legal.
     */
    public static boolean checkNumber(String data)
    {
        return data.chars().allMatch( Character::isDigit) && Integer.parseInt(data) >= Constants.MIN_DAYS_OR_EVENTS && Integer.parseInt(data) <= Constants.MAX_DAYS_OR_EVENTS;
    }
}
