import javax.swing.*;
import java.awt.*;

/**
 * This class is GUI of add AI event.
 */
public class AddAIEventActivity {
    /**
     * This function create the screen that get input from the user about the AI event that he want to add.
     * @param menuFrame The frame of menu to show him after show the events.
     */
    public AddAIEventActivity(JFrame menuFrame) {
        JFrame frame = new JFrame("Calendar Management");
        frame.setSize(Constants.FRAME_WIDTH, Constants.FRAME_HEIGHT);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel titleLabel = new JLabel("Title:");
        JTextField titleField = new JTextField(Constants.FIELD_WIDTH);

        JLabel dateLabel = new JLabel("Date in format yyyy-MM-dd:");
        JTextField dateField = new JTextField(Constants.FIELD_WIDTH);

        JLabel deadlineLabel = new JLabel("Deadline in format yyyy-MM-dd:");
        JTextField deadlineField = new JTextField(Constants.FIELD_WIDTH);

        JLabel durationLabel = new JLabel("Duration in format HH:mm:");
        JTextField durationField = new JTextField(Constants.FIELD_WIDTH);

        JLabel splitLevelLabel = new JLabel("Split Level (1-3):");
        JTextField splitLevelField = new JTextField(Constants.FIELD_WIDTH);

        JLabel splitLenLabel = new JLabel("Split Length in format HH:mm:");
        JTextField splitLenField = new JTextField(Constants.FIELD_WIDTH);

        JLabel priorityLabel = new JLabel("Priority (1-5):");
        JTextField priorityField = new JTextField(Constants.FIELD_WIDTH);

        // Create a panel to hold the form components and set its layout
        JPanel formPanel = new JPanel(new GridLayout(Constants.GRID_LAYOUT_4, Constants.GRID_LAYOUT_2, Constants.GRID_LAYOUT_5, Constants.GRID_LAYOUT_5));
        formPanel.add(titleLabel);
        formPanel.add(titleField);
        formPanel.add(dateLabel);
        formPanel.add(dateField);
        formPanel.add(deadlineLabel);
        formPanel.add(deadlineField);
        formPanel.add(durationLabel);
        formPanel.add(durationField);
        formPanel.add(splitLevelLabel);
        formPanel.add(splitLevelField);
        formPanel.add(splitLenLabel);
        formPanel.add(splitLenField);
        formPanel.add(priorityLabel);
        formPanel.add(priorityField);
        formPanel.setBorder(BorderFactory.createEmptyBorder(Constants.EMPTY_BORDER_10, Constants.EMPTY_BORDER_10, Constants.EMPTY_BORDER_10, Constants.EMPTY_BORDER_10));

        // Create a button to submit the form
        JButton submitButton = new JButton("Submit");
        submitButton.addActionListener(actionEvent -> {
            // Get the data from the form fields
            AIEvent aiEvent = new AIEvent();
            frame.setVisible(false);
            if(!CheckValidInput.checkTitle(titleField.getText()))
                msgToUser("The title is empty!", menuFrame);
            else if(!CheckValidInput.checkDate(dateField.getText()))
                msgToUser("The date doesn't in format.", menuFrame);
            else if (!CheckValidInput.checkDeadline(dateField.getText(), deadlineField.getText()))
                msgToUser("The deadline doesn't in format.", menuFrame);
            else if (!CheckValidInput.checkHour(durationField.getText()))
                msgToUser("The duration doesn't in format.", menuFrame);
            else if (!CheckValidInput.checkSplitLevel(splitLevelField.getText()))
                msgToUser("The split level doesn't legal.", menuFrame);
            else if (!CheckValidInput.checkLenOfSplit(splitLevelField.getText(), splitLenField.getText()))
                msgToUser("The len of split doesn't legal.", menuFrame);
            else if (!CheckValidInput.checkPriority(priorityField.getText()))
                msgToUser("The priority doesn't format.", menuFrame);
            else {
                aiEvent.title = titleField.getText();
                aiEvent.date = dateField.getText();
                aiEvent.setDeadline(deadlineField.getText());
                aiEvent.duration = durationField.getText();
                aiEvent.setSplitLevel(Integer.parseInt(splitLevelField.getText()));
                aiEvent.setLenOfSplit(splitLenField.getText());
                aiEvent.setPriority(Integer.parseInt(priorityField.getText()));
                CalendarManager cm = new CalendarManager();
                int codeResponse = cm.theManager(aiEvent);
                if(codeResponse == Constants.NO_ADDED) {
                    msgToUser("We don't have a place for the event '" + aiEvent.title + "' .", menuFrame);
                }
                else if (codeResponse == Constants.ADDED) {
                    msgToUser("The event '" + aiEvent.title + "' created in successfully.", menuFrame);
                }
                else if (codeResponse == Constants.ADDED_AFTER_CHANGE){
                    msgToUser("The event '" + aiEvent.title + "' added after change in the event: '" + CalendarManager.changeEvent + "'.", menuFrame);
                }
                else {
                    msgToUser("The event '" + aiEvent.title + "' added after delete the event: '" + CalendarManager.changeEvent + "'.", menuFrame);
                }
            }
        });

        // Create a panel to hold the submit button and set its layout
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        buttonPanel.add(submitButton);

        // Create a panel to hold the form and button panels and set its layout
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(formPanel, BorderLayout.CENTER);
        mainPanel.add(buttonPanel, BorderLayout.SOUTH);

        // Add the main panel to the frame
        frame.add(mainPanel);

        // Pack and show the frame
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    /**
     * This function show screen with a message to the user after his try to add AI event.
     * @param msg The message to show.
     * @param menuFrame The menu screen that should show after close the message.
     */
    public void msgToUser(String msg, JFrame menuFrame)
    {
        JFrame frame = new JFrame("Calendar Management");
        frame.setSize(Constants.FRAME_WIDTH, Constants.FRAME_HEIGHT);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel msgLabel = new JLabel(msg);
        JPanel formPanel = new JPanel(new GridLayout(Constants.GRID_LAYOUT_4, Constants.GRID_LAYOUT_2, Constants.GRID_LAYOUT_5, Constants.GRID_LAYOUT_5));
        formPanel.add(msgLabel);

        formPanel.setBorder(BorderFactory.createEmptyBorder(Constants.EMPTY_BORDER_10, Constants.EMPTY_BORDER_10, Constants.EMPTY_BORDER_10, Constants.EMPTY_BORDER_10));

        JButton submitButton = new JButton("Close");
        submitButton.addActionListener(actionEvent -> {
            frame.setVisible(false);
            menuFrame.setVisible(true);
        });

        // Create a panel to hold the submit button and set its layout
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        buttonPanel.add(submitButton);

        // Create a panel to hold the form and button panels and set its layout
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(formPanel, BorderLayout.CENTER);
        mainPanel.add(buttonPanel, BorderLayout.SOUTH);

        // Add the main panel to the frame
        frame.add(mainPanel);

        // Pack and show the frame
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
