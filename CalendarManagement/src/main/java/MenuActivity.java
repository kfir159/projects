import javax.swing.*;
import java.awt.*;

/**
 * This class is the screen of the menu in GUI.
 */
public class MenuActivity
{
    /**
     * This function create the screen of the menu.
     */
    public MenuActivity()
    {
        JFrame frame = new JFrame("Calendar Management");
        frame.setSize(Constants.FRAME_WIDTH, Constants.FRAME_HEIGHT);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel label = new JLabel("Welcome to Calendar Management", SwingConstants.CENTER);
        label.setFont(new Font("Arial", Font.BOLD, Constants.LABEL_SIZE));

        // Create the buttons and set their alignment
        JButton button1 = new JButton("Read Events");
        JButton button2 = new JButton("Add Regular Event");
        JButton button3 = new JButton("Add AI Event");
        button1.setFont(new Font("Arial", Font.PLAIN, Constants.BUTTON_SIZE));
        button2.setFont(new Font("Arial", Font.PLAIN, Constants.BUTTON_SIZE));
        button3.setFont(new Font("Arial", Font.PLAIN, Constants.BUTTON_SIZE));

        button1.addActionListener(actionEvent -> {
            frame.setVisible(false);
            ReadEventsActivity readEventsActivity = new ReadEventsActivity(frame);
        });

        button2.addActionListener(actionEvent -> {
            frame.setVisible(false);
            AddMyEventActivity addEventActivity = new AddMyEventActivity(frame);
            });

        button3.addActionListener(actionEvent -> {
            frame.setVisible(false);
            AddAIEventActivity addAIEventActivity = new AddAIEventActivity(frame);
        });

        // Create a panel to hold the buttons and center them horizontally
        JPanel buttonPanel = new JPanel(new GridLayout(Constants.GRID_LAYOUT_1, Constants.GRID_LAYOUT_3, Constants.GRID_LAYOUT_10, Constants.GRID_LAYOUT_10));
        buttonPanel.add(button1);
        buttonPanel.add(button2);
        buttonPanel.add(button3);
        buttonPanel.setBorder(BorderFactory.createEmptyBorder(Constants.EMPTY_BORDER_10, Constants.EMPTY_BORDER_10, Constants.EMPTY_BORDER_10, Constants.EMPTY_BORDER_10));

        // Create a panel to hold the label and button panels and center them vertically
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(label, BorderLayout.NORTH);
        mainPanel.add(buttonPanel, BorderLayout.CENTER);
        mainPanel.setBorder(BorderFactory.createEmptyBorder(Constants.EMPTY_BORDER_20, Constants.EMPTY_BORDER_20, Constants.EMPTY_BORDER_20, Constants.EMPTY_BORDER_20));
        frame.add(mainPanel);

        // Pack and show the frame
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
