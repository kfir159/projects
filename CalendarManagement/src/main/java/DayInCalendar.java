import java.util.Map;
import java.util.TreeMap;

/**
 * This class represent one day with all the events in him of the calendar.
 */
public class DayInCalendar
{
    private Map<String, MyEvent> day;

    /**
     * This function is a constructor of the class and init the treemap.
     */
    public DayInCalendar()
    {
        this.day = new TreeMap<>();
    }

    /**
     * This function add event to the treemap.
     * @param event The event to add.
     */
    public void addEvent(MyEvent event)
    {
        this.day.put(event.startHour, event);
    }

    /**
     * This function get day with the events.
     * @return The day with the events from the calendar.
     */
    public Map<String, MyEvent> GetDay()
    {
        return this.day;
    }
}
