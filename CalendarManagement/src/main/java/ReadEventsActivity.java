import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * This class is screen of the read events and have all the options of read events.
 */
public class ReadEventsActivity {
    /**
     * This function create the main screen of read events.
     * @param menuFrame The frame of menu to show him after show the events.
     */
    public ReadEventsActivity(JFrame menuFrame)
    {
        JFrame frame = new JFrame("Calendar Management");
        frame.setSize(Constants.FRAME_WIDTH, Constants.FRAME_HEIGHT);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel label = new JLabel("Choose Option", SwingConstants.CENTER);
        label.setFont(new Font("Arial", Font.BOLD, Constants.LABEL_SIZE));

        // Create the buttons and set their alignment
        JButton button1 = new JButton("Read some close Events");
        JButton button2 = new JButton("Read events from some days");
        JButton button3 = new JButton("Read events from specific date");
        button1.setFont(new Font("Arial", Font.PLAIN, Constants.BUTTON_SIZE));
        button2.setFont(new Font("Arial", Font.PLAIN, Constants.BUTTON_SIZE));
        button3.setFont(new Font("Arial", Font.PLAIN, Constants.BUTTON_SIZE));

        button1.addActionListener(actionEvent -> {
            frame.setVisible(false);
            inputData(menuFrame, Constants.UPCOMING_EVENTS);
        });

        button2.addActionListener(actionEvent -> {
            frame.setVisible(false);
            inputData(menuFrame, Constants.EVENTS_IN_DAYS);
        });

        button3.addActionListener(actionEvent -> {
            frame.setVisible(false);
            inputData(menuFrame, Constants.EVENTS_IN_DAY);
        });

        // Create a panel to hold the buttons and center them horizontally
        JPanel buttonPanel = new JPanel(new GridLayout(Constants.GRID_LAYOUT_1, Constants.GRID_LAYOUT_3, Constants.GRID_LAYOUT_10, Constants.GRID_LAYOUT_10));
        buttonPanel.add(button1);
        buttonPanel.add(button2);
        buttonPanel.add(button3);
        buttonPanel.setBorder(BorderFactory.createEmptyBorder(Constants.EMPTY_BORDER_10, Constants.EMPTY_BORDER_10, Constants.EMPTY_BORDER_10, Constants.EMPTY_BORDER_10));

        // Create a panel to hold the label and button panels and center them vertically
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(label, BorderLayout.NORTH);
        mainPanel.add(buttonPanel, BorderLayout.CENTER);
        mainPanel.setBorder(BorderFactory.createEmptyBorder(Constants.EMPTY_BORDER_20, Constants.EMPTY_BORDER_20, Constants.EMPTY_BORDER_20, Constants.EMPTY_BORDER_20));
        frame.add(mainPanel);

        // Pack and show the frame
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    /**
     * This function create screen of get input data for the ask of events.
     * @param menuFrame The menu screen that need back to him after get the data.
     * @param codeRequest The Code of the request for the truth question.
     */
    public void inputData(JFrame menuFrame, int codeRequest)
    {
        JFrame frame = new JFrame("Calendar Management");
        frame.setSize(Constants.FRAME_WIDTH, Constants.FRAME_HEIGHT);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JLabel askLabel;
        if(codeRequest == Constants.UPCOMING_EVENTS) {
            askLabel = new JLabel("enter some events (1-99):");
        }
        else if (codeRequest == Constants.EVENTS_IN_DAYS) {
            askLabel = new JLabel("enter some days (1-99):");
        }
        else {
            askLabel = new JLabel("enter date in format yyyy-MM-dd:");
        }
        JTextField dataField = new JTextField(Constants.FIELD_WIDTH);
        JPanel formPanel = new JPanel(new GridLayout(Constants.GRID_LAYOUT_4, Constants.GRID_LAYOUT_2, Constants.GRID_LAYOUT_5, Constants.GRID_LAYOUT_5));
        formPanel.add(askLabel);
        formPanel.add(dataField);

        formPanel.setBorder(BorderFactory.createEmptyBorder(Constants.EMPTY_BORDER_10, Constants.EMPTY_BORDER_10, Constants.EMPTY_BORDER_10, Constants.EMPTY_BORDER_10));

        JButton submitButton = new JButton("Submit");
        submitButton.addActionListener(actionEvent -> {
            frame.setVisible(false);
            if ((codeRequest == Constants.UPCOMING_EVENTS || codeRequest == Constants.EVENTS_IN_DAYS) && !CheckValidInput.checkNumber(dataField.getText()))
            {
                msgToUser("The input is illegal.", menuFrame);
            }
            else if(codeRequest == Constants.EVENTS_IN_DAY && !CheckValidInput.checkDate(dataField.getText()))
            {
                msgToUser("The date doesn't in format or illegal.", menuFrame);
            }
            else {
                ArrayList<String> theEvents = CalendarAPI.loadEvents(codeRequest, dataField.getText());
                listScreen(theEvents, menuFrame);
            }
        });

        // Create a panel to hold the submit button and set its layout
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        buttonPanel.add(submitButton);

        // Create a panel to hold the form and button panels and set its layout
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(formPanel, BorderLayout.CENTER);
        mainPanel.add(buttonPanel, BorderLayout.SOUTH);

        // Add the main panel to the frame
        frame.add(mainPanel);

        // Pack and show the frame
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    /**
     * This function show list of the asked events.
     * @param eventList The list with the details of the events.
     * @param menuFrame The menu screen to show after close.
     */
    public void listScreen(ArrayList<String> eventList, JFrame menuFrame) {
        JFrame frame = new JFrame("Calendar Management");

        // create the list data
        DefaultListModel<String> listModel = new DefaultListModel<>();

        // add the elements of the ArrayList<String> to the model
        for (String str : eventList) {
            listModel.addElement(str);
        }
        // create the list
        JList<String> list = new JList<>(listModel);

        // create a scroll pane for the list
        JScrollPane scrollPane = new JScrollPane(list);

        // create a button to close the window
        JButton closeButton = new JButton("Close");
        closeButton.addActionListener(actionEvent -> {
            frame.setVisible(false);
            menuFrame.setVisible(true);
        });

        // add the scroll pane and button to the frame
        frame.add(scrollPane, BorderLayout.CENTER);
        frame.add(closeButton, BorderLayout.SOUTH);

        // set the size of the frame
        frame.setSize(Constants.FRAME_WIDTH, Constants.FRAME_HEIGHT);

        // center the frame on the screen
        frame.setLocationRelativeTo(null);

        // set the default close operation
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // show the frame
        frame.setVisible(true);
    }

    /**
     * This function show screen with a message to the user after his try to add AI event.
     * @param msg The message to show.
     * @param menuFrame The menu screen that should show after close the message.
     */
    public void msgToUser(String msg, JFrame menuFrame)
    {
        JFrame frame = new JFrame("Calendar Management");
        frame.setSize(Constants.FRAME_WIDTH, Constants.FRAME_HEIGHT);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel msgLabel = new JLabel(msg);
        JPanel formPanel = new JPanel(new GridLayout(Constants.GRID_LAYOUT_4, Constants.GRID_LAYOUT_2, Constants.GRID_LAYOUT_5, Constants.GRID_LAYOUT_5));
        formPanel.add(msgLabel);

        formPanel.setBorder(BorderFactory.createEmptyBorder(Constants.EMPTY_BORDER_10, Constants.EMPTY_BORDER_10, Constants.EMPTY_BORDER_10, Constants.EMPTY_BORDER_10));

        JButton submitButton = new JButton("Close");
        submitButton.addActionListener(actionEvent -> {
            frame.setVisible(false);
            menuFrame.setVisible(true);
        });

        // Create a panel to hold the submit button and set its layout
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        buttonPanel.add(submitButton);

        // Create a panel to hold the form and button panels and set its layout
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(formPanel, BorderLayout.CENTER);
        mainPanel.add(buttonPanel, BorderLayout.SOUTH);

        // Add the main panel to the frame
        frame.add(mainPanel);

        // Pack and show the frame
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
