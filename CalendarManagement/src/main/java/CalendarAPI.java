import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.*;
import java.io.*;
import java.security.GeneralSecurityException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * This class is the connection with the Google calendar API and the calendar.
 */
public class CalendarAPI {
    /**
     * Application name.
     */
    private static final String APPLICATION_NAME = "Google Calendar API Java Quickstart";
    /**
     * Global instance of the JSON factory.
     */
    private static final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();
    /**
     * Directory to store authorization tokens for this application.
     */
    private static final String TOKENS_DIRECTORY_PATH = "tokens";

    /**
     * Global instance of the scopes required by this quickstart.
     * If modifying these scopes, delete your previously saved tokens/ folder.
     */
    private static final List<String> SCOPES =
            Collections.singletonList(CalendarScopes.CALENDAR);
    private static final String CREDENTIALS_FILE_PATH = "src/main/resources/client_secret_38261955918-ogq21qmg672o1hbli41t0rse4l76s9km.apps.googleusercontent.com.json";

    /**
     * Creates an authorized Credential object.
     * @param HTTP_TRANSPORT The network HTTP Transport.
     * @return An authorized Credential object.
     * @throws IOException If the credentials.json file cannot be found.
     */
    private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT)
            throws IOException {
        // Load client secrets.
        InputStream in = new FileInputStream(CREDENTIALS_FILE_PATH);
        if (in == null) {
            throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
        }
        GoogleClientSecrets clientSecrets =
                GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(Constants.PORT_FOR_USE).build();
        Credential credential = new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
        //returns an authorized Credential object.
        return credential;
    }

    /**
     * This function read events from the calendar according the choice and data when:
     * choice 1 use data to give X closest events.
     * choice 2 use data to give the all events from X closest days.
     * choice 3 use data to give all the events from specific date.
     * @param choice The choice that the user chose.
     * @param data The data that the user sent.
     * @return ArrayList with details about the events.
     */
    public static ArrayList<String> loadEvents(int choice, String data)
    {
        // Build a new authorized API client service.
        Calendar service;
        final NetHttpTransport HTTP_TRANSPORT;
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        service =
                new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                        .setApplicationName(APPLICATION_NAME)
                        .build();
        } catch (GeneralSecurityException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        // List the next 10 events from the primary calendar.
        Events events;
        List<Event> items = null;
        if(choice == Constants.UPCOMING_EVENTS) {
            DateTime now = new DateTime(System.currentTimeMillis());
            try {
                events = service.events().list("primary")
                        .setMaxResults(Integer.valueOf(data))
                        .setTimeMin(now)
                        .setOrderBy("startTime")
                        .setSingleEvents(true)
                        .execute();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            items = events.getItems();
        }
        else if(choice == Constants.EVENTS_IN_DAYS) {
            DateTime now = new DateTime(System.currentTimeMillis());
            LocalDateTime nowSomeMore = LocalDateTime.now().plusDays(Integer.valueOf(data)).with(LocalTime.MIDNIGHT);
            DateTime moreSomeDays = new DateTime(nowSomeMore.toString() + ":00+03:00");
            try {
                events = service.events().list("primary")
                        .setTimeMin(now)
                        .setTimeMax(moreSomeDays)
                        .setOrderBy("startTime")
                        .setSingleEvents(true)
                        .execute();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            items = events.getItems();
        }
        else if(choice == Constants.EVENTS_IN_DAY)
        {
            items = loadDay(data);
        }
        ArrayList<String> detailsOnEvents = new ArrayList<String>();
        if (items.isEmpty()) {
            detailsOnEvents.add("No events found.");
            //System.out.println("No upcoming events found.");
        } else {
            //System.out.println("Upcoming events");
            for (Event event : items) {
                DateTime start = event.getStart().getDateTime();
                DateTime end = event.getEnd().getDateTime();
                if (start == null) {
                    start = event.getStart().getDate();
                    end = event.getStart().getDate();
                }
                //System.out.printf("%s (%s - %s)\n", event.getSummary(), start, end);
                String toList = String.format("%s (%s - %s)\n", event.getSummary(), start, end);
                detailsOnEvents.add(toList);
            }
        }
        return detailsOnEvents;
    }

    /**
     * This function load the all necessary days to the data structure according the days - start and deadline.
     * @param start The start date that the event is relevant.
     * @param end The deadline of the event.
     */
    public static void loadSomeDays(String start, String end){
        LocalDateTime startDate = LocalDateTime.parse(start + " 00:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        int days = daysBetween(start, end);
        for(int i = 0; i <= days ; i++)
        {
            LocalDateTime nowSomeMore = startDate.plusDays(i).with(LocalTime.MIDNIGHT);
            DateTime moreSomeDays = new DateTime(nowSomeMore.toString() + ":00+03:00");
            String currDate = moreSomeDays.toString().split("T")[0];
            List<Event> items = loadDay(currDate);
            DayInCalendar dayInCalendar = new DayInCalendar();
            for(Event event : items) {
                if(event.getDescription() == null)
                {
                    MyEvent myEvent = new MyEvent();
                    addBasicDetails(myEvent, event);
                    dayInCalendar.addEvent(myEvent);
                }
                else
                {
                    AIEvent aiEvent = new AIEvent();
                    addBasicDetails(aiEvent, event);
                    dayInCalendar.addEvent(aiEvent);
                    loadOtherDetails(aiEvent, event);
                }
            }
            CalendarManager.calendarEvents.put(currDate, dayInCalendar);
        }
    }

    /**
     * This function add the basic details from the event to my event.
     * @param myEvent The AI or My event.
     * @param event The event from the calendar.
     */
    public static void addBasicDetails(MyEvent myEvent, Event event)
    {
        myEvent.title = event.getSummary();
        myEvent.date = event.getStart().getDateTime().toString().split("T")[0];
        myEvent.startHour = event.getStart().getDateTime().toString().split("T")[1].split("\\.")[0];
        myEvent.endHour = event.getEnd().getDateTime().toString().split("T")[1].split("\\.")[0];
        myEvent.duration = calcDuration(myEvent.startHour, myEvent.endHour);
    }

    /**
     * This function load to the AI event to fields from the description.
     * @param aiEvent The event that write to him.
     * @param event The event that read from him.
     */
    public static void loadOtherDetails(AIEvent aiEvent, Event event)
    {
        aiEvent.setDeadline(event.getDescription().toString().split("&")[0]);
        aiEvent.setPriority(Integer.valueOf(event.getDescription().toString().split("&")[1]));
        aiEvent.setSplitLevel(Integer.valueOf(event.getDescription().toString().split("&")[2]));
        if(event.getDescription().toString().split("&").length == Constants.LEN_FULL_DESCRIPTION)
            aiEvent.setLenOfSplit(event.getDescription().toString().split("&")[3]);
    }

    /**
     * This function calculate the duration of event according his start hour and his end hour.
     * @param start The start hour of the event.
     * @param end The end hour of the event.
     * @return The duration of the event.
     */
    public static String calcDuration(String start, String end)
    {
        LocalTime calcHour = LocalTime.parse(end, DateTimeFormatter.ofPattern("HH:mm:ss")).minusHours(Integer.valueOf(start.split(":")[0])).minusMinutes(Integer.valueOf(start.split(":")[1]));
        return calcHour.toString();
    }

    /**
     * This function calculate how many days there are between to dates.
     * @param start The start date.
     * @param end The end date.
     * @return How many days between the days.
     */
    public static int daysBetween(String start, String end)
    {
        LocalDateTime startDate = LocalDateTime.parse(start + " 00:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        LocalDateTime endDate = LocalDateTime.parse(end + " 00:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        return (int)Duration.between(startDate, endDate).toDays();
    }

    /**
     * This function get all the events from specific date.
     * @param date The wanted date.
     * @return List with all the events in the date.
     */
    public static List<Event> loadDay(String date){
        final NetHttpTransport HTTP_TRANSPORT;
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            Calendar service =
                    new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                            .setApplicationName(APPLICATION_NAME)
                            .build();
            DateTime now = new DateTime(date + "T00:00:00+03:00");
            LocalDateTime nowOneMore = LocalDateTime.parse(date + " 00:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")).plusDays(1).with(LocalTime.MIDNIGHT);
            DateTime moreOneDay = new DateTime(nowOneMore.toString() + ":00+03:00");
            Events events = service.events().list("primary")
                    .setTimeMin(now)
                    .setTimeMax(moreOneDay)
                    .setOrderBy("startTime")
                    .setSingleEvents(true)
                    .execute();
            return events.getItems();
        } catch (GeneralSecurityException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * This function add regular event to the calendar.
     * @param myEvent The event to add.
     */
    public static void addMyEventToCalendar(MyEvent myEvent){
        Event event = new Event()
                .setSummary(myEvent.title);

        String startTime = myEvent.date + "T" + myEvent.startHour;

        DateTime startDateTime;
        if(startTime.split(":").length == Constants.COUNT_NUMBERS_IN_HOUR)
            startDateTime = new DateTime(startTime + ":00+03:00");
        else
            startDateTime = new DateTime(startTime + "+03:00");
        EventDateTime start = new EventDateTime()
                .setDateTime(startDateTime);
        event.setStart(start);

        String endTime = myEvent.date + "T" + myEvent.endHour;
        DateTime endDateTime;
        if(endTime.split(":").length == Constants.COUNT_NUMBERS_IN_HOUR)
            endDateTime = new DateTime(endTime + ":00+03:00");
        else
            endDateTime = new DateTime(endTime + "+03:00");
        EventDateTime end = new EventDateTime()
                .setDateTime(endDateTime);
        event.setEnd(end);
        addEventToCalendar(event);
    }

    /**
     * This function add AI event to the calendar.
     * @param aiEvent The event to add.
     */
    public  static void addAIEventToCalendar(AIEvent aiEvent)
    {
        Event event = new Event()
                .setSummary(aiEvent.title);
        DateTime startDateTime;
        String startTime = aiEvent.date + "T" + aiEvent.startHour;
        if(startTime.split(":").length == Constants.COUNT_NUMBERS_IN_HOUR) {
            startDateTime = new DateTime(startTime + ":00+03:00");
            aiEvent.startHour += ":00";
        }
        else
            startDateTime = new DateTime(startTime + "+03:00");
        EventDateTime start = new EventDateTime()
                .setDateTime(startDateTime);
        event.setStart(start);

        LocalDateTime calcHour = LocalDateTime.parse(aiEvent.date + " " + aiEvent.startHour, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")).plusHours(Integer.valueOf(aiEvent.duration.split(":")[0])).plusMinutes(Integer.valueOf(aiEvent.duration.split(":")[1]));
        DateTime endDateTime;
        endDateTime = new DateTime(calcHour + ":00+03:00");
        EventDateTime end = new EventDateTime()
                .setDateTime(endDateTime);
        event.setEnd(end);
        if(aiEvent.getSplitLevel() == Constants.NO_SPLIT)
            event.setDescription(aiEvent.getDeadline() + "&" + aiEvent.getPriority() + "&" + aiEvent.getSplitLevel());
        else
            event.setDescription(aiEvent.getDeadline() + "&" + aiEvent.getPriority() + "&" + aiEvent.getSplitLevel() + "&" + aiEvent.getLenOfSplit());
        addEventToCalendar(event);
    }

    /**
     * This function add event to google calendar.
     * @param event The event to add.
     */
    public static void addEventToCalendar(Event event)
    {
        final NetHttpTransport HTTP_TRANSPORT;
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            Calendar service =
                    new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                            .setApplicationName(APPLICATION_NAME)
                            .build();
            String calendarId = "primary";
            event = service.events().insert(calendarId, event).execute();
            System.out.printf("Event created: %s\n", event.getHtmlLink());
        } catch (GeneralSecurityException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * This function delete event from Google calendar.
     * @param myEvent The event to delete.
     * @return True or false if the delete seceded.
     */
    public static boolean deleteEvent(MyEvent myEvent){
        final NetHttpTransport HTTP_TRANSPORT;
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            Calendar service =
                    new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                            .setApplicationName(APPLICATION_NAME)
                            .build();
            String calendarId = "primary";
            Event event = checkEvent(myEvent);
            if(event != null)
            {
                service.events().delete(calendarId, event.getId()).execute();
                return true;
            }
        } catch (GeneralSecurityException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return false;
    }

    /**
     * This function check if event exist in the calendar according his title, date and hour.
     * @param myEvent The event to check if existed.
     * @return The event from the calendar.
     */
    public static Event checkEvent(MyEvent myEvent){
        List<Event> events = loadDay(myEvent.date);
        for(Event event : events)
        {
            if(event.getSummary().equals(myEvent.title) && event.getStart().getDateTime().toString().split("T")[1].split("\\.")[0].equals(myEvent.startHour) && event.getStart().getDateTime().toString().split("T")[0].equals(myEvent.date))
            {
                return event;
            }
        }
        return null;
    }
}
