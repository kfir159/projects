/**
 * This class represented regular event.
 */
public class MyEvent
{
    protected String title;
    protected String startHour;
    protected String endHour;
    protected String date;
    protected String duration;

    /**
     * This function is a empty constructor.
     */
    public MyEvent() {
    }

    /**
     * This function is a regular constructor that give values and build event.
     * @param title The title of the event.
     * @param startHour The start hour of the event.
     * @param endHour The end hour of the event.
     * @param date The date of the event.
     * @param duration The duration of the event.
     */
    public MyEvent(String title, String startHour, String endHour, String date, String duration) {
        this.title = title;
        this.startHour = startHour;
        this.endHour = endHour;
        this.date = date;
        this.duration = duration;
    }
}
