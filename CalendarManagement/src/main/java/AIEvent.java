/**
 * This class is AI event that extends from the class MyEvent.
 * The type AIEvent should to represent smart event that add with the algorithm.
 */
public class AIEvent extends MyEvent
{
    private int priority;
    private String deadline;
    private int splitLevel;
    private String lenOfSplit;

    /**
     * This is a empty constructor of AIEvent.
     */
    public AIEvent() {
    }

    /**
     * This is a constructor that get event and copy his values.
     * @param event The event to copy his values.
     */
    public AIEvent(AIEvent event)
    {
        super(event.title, event.startHour, event.endHour, event.date, event.duration);
        this.setDeadline((event).getDeadline());
        this.setPriority((event).getPriority());
        this.setSplitLevel((event).getSplitLevel());
        this.setLenOfSplit((event).getLenOfSplit());
    }

    /**
     * This function get priority of AI event.
     * @return The priority if the AI event.
     */
    public int getPriority() {
        return priority;
    }

    /**
     * This function set a new priority to the AI event.
     * @param priority The new priority.
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    /**
     * This function get the deadline of the AI event.
     * @return The deadline of the event.
     */
    public String getDeadline() {
        return deadline;
    }

    /**
     * This function set a new deadline to the AI event.
     * @param deadline The new deadline.
     */
    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    /**
     * This function get the split-level of the AI event.
     * @return The split-level of the AI event.
     */
    public int getSplitLevel() {
        return splitLevel;
    }

    /**
     * This function set a new split-level to the AI event.
     * @param splitLevel This function is the new split-level of the event.
     */
    public void setSplitLevel(int splitLevel) {
        this.splitLevel = splitLevel;
    }

    /**
     * This function get the len of split of the AI event if the split-level is 2 or 3.
     * @return The len of split of the AI event.
     */
    public String getLenOfSplit() {
        return lenOfSplit;
    }

    /**
     * This function set a new len of split for the AI event.
     * @param lenOfSplit The new len of split.
     */
    public void setLenOfSplit(String lenOfSplit) {
        this.lenOfSplit = lenOfSplit;
    }
}
