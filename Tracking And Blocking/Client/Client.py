from win10toast import ToastNotifier
from firebase_admin import credentials, db
import json
import threading
import firebase_admin

lastLst = []
login = False
username = ""
password = ""
child = ""
PIndex = ""
is_childs = False
child_login = False
#firebase_admin.initialize_app(credentials.Certificate("../serviceAccountKey1.json"), {'databaseURL': 'https://tb---ik-default-rtdb.firebaseio.com/'})
firebase_admin.initialize_app(credentials.Certificate("../serviceAccountKey.json"), {'databaseURL': 'https://tracking-and-blocking-default-rtdb.firebaseio.com/'})

def notify():
	"""
	This function create thread to notify when the child try to enter to blocked program.
	:return: None
	"""
	t = threading.Thread(target = notifier)
	t.daemon = True
	t.start()

def notifier():
	"""
	This function pops up an notify for parents when the child enters blocked program.
	:return: None
	"""
	global child_login, PIndex, child
	while True:
		t = ToastNotifier()
		while child_login:
			ref = db.reference('Parents/' + str(PIndex) + '/childs/' + str(int(child) - 1) + '/')
			curr = ref.get()
			if len(curr['entry_history']) > 0:
				username = curr['entry_history'][-1]
				if curr['code'] == REQUEST_NOTIFY_ENTRY_PROGRAM or curr['code'] == REQUEST_NOTIFY_ENTRY_WEBSITE:
					t.show_toast("Blocked entry detected!", f"The child tried to enter into {username}", None, 5, False)
					curr['code'] = RESPONSE_NOTIFY_ENTRY_PROGRAM if curr['code'] == REQUEST_NOTIFY_ENTRY_PROGRAM else RESPONSE_NOTIFY_ENTRY_WEBSITE
					ref.set(curr)

def printEntries():
	"""
	This function print the ten try entries of the child to blocked programs.
	:return: None.
	"""
	ref = db.reference('Parents/' + str(PIndex) + '/childs/' + str(int(child) - 1) + '/')
	return ref.get()['entry_history']

def signIn(uname, passw):
	"""
	This function do sign in for the parents if they chose the first option in the first menu.
	:return: None.
	"""
	global login, username, password
	username = uname
	password = passw
	ref = db.reference('Parents/')
	lst = ref.get()
	if lst != None:
		for parent in lst:
			if parent['username'] == username and parent['password'] == password:
				login = True
	
def signUp(uname, passw):
	"""
	This function do sign up for the parents if they chose the second option in the first menu, by add a new parents to the firebase.
	:return: None.
	"""
	global login, username, password
	ref = db.reference('Parents/')
	parents = ref.get()
	if parents != None:
		for index in range(len(parents)):
			if parents[index]['username'] == uname:
				return
	username = uname
	password = passw
	ref = db.reference('Parents/')
	lst = ref.get()
	if lst == [] or lst == None:
		lst = [{"username": username, "password": password, 'childs': ''}]
	else:
		lst.append({"username": username, "password": password, 'childs': ''})
	ref.set([x for x in lst if x is not None])
	lst = ref.get()
	login = True

def findChild():
	"""
	This function printing all the childs of the parent if exists and let him choose child from a list of them.
	:return: None.
	"""
	global child, login, username, password, PIndex, is_childs, child_login
	is_childs = False
	ref = db.reference('Parents/')
	parents = ref.get()
	for index in range(len(parents)):
		if parents[index]['username'] == username and parents[index]['password'] == password:
			PIndex = index
			is_childs = True
			child_login = True
			return parents[index]['childs']

def getScreenShot():
	"""
	This 
	"""
	ref = db.reference('Parents/' + str(PIndex) + '/childs/' + str(int(child) - 1) + '/screenshot/')
	ref.set("GET")
	while ref.get() == "GET":
		pass
	return ref.get()

def getListPrograms():
	"""
	This function ask from the child that will give him list with all of his programs.
	:return: None.
	"""
	ref = db.reference('Parents/' + str(PIndex) + '/childs/' + str(int(child) - 1) + '/all_programs/')
	lastLst = ref.get()
	ref = db.reference('Parents/' + str(PIndex) + '/childs/' + str(int(child) - 1) + '/blocked_programs/')
	blockedProgs = ref.get()
	return [x for x in lastLst if x not in ([] if blockedProgs == '' else blockedProgs)]
	#return "\n".join([f"{i}. {item}" for (i, item) in enumerate(lastLst, start=1)])

def blockProgram(program):
	"""
	This function adding the username of the program the parents want to block to the firebase server.
	:return: None.
	"""
	#ref = db.reference('Parents/' + str(PIndex) + '/childs/' + str(int(child) - 1) + '/code')
	#ref.set(REQUEST_BLOCK_PROGRAM)
	ref = db.reference('Parents/' + str(PIndex) + '/childs/' + str(int(child) - 1) + '/blocked_programs')
	j = ref.get()
	if j == '':
		j = []
	j += [program]
	ref.set(j)

def getListBlockedPrograms():
	"""
	This function ask from the child that will give him list with all his blocking programs.
	:return: None.
	"""
	ref = db.reference('Parents/' + str(PIndex) + '/childs/' + str(int(child) - 1) + '/' + 'blocked_programs')
	lst = ref.get()
	if lst != '':
		return lst
	else:
		return []

def unblockProgram(program):
	"""
	This function adding the username of the program the parents want to unblock to the firebase server.
	:return: None.
	"""
	#ref = db.reference('Parents/' + str(PIndex) + '/childs/' + str(int(child) - 1) + '/code')
	#ref.set(REQUEST_UNBLOCK_PROGRAM)
	ref = db.reference('Parents/' + str(PIndex) + '/childs/' + str(int(child) - 1) + '/blocked_programs')
	j = ref.get()
	j = [x for x in j if x != program]
	if j == []:
		j = ''
	ref.set(j)

def BlockWebsite(website):
	"""
	This function adding the URL that the parents want to block to the firebase server.
	:return: None.
	"""
	#ref = db.reference('Parents/' + str(PIndex) + '/childs/' + str(int(child) - 1) + '/code')
	#ref.set(REQUEST_BLOCK_WEBSITE)
	ref = db.reference('Parents/' + str(PIndex) + '/childs/' + str(int(child) - 1) + '/blocked_websites')
	j = ref.get()
	if j == '':
		j = []
	j += [website]
	ref.set(j)

def getListBlockedWebsites():
	"""
	This function ask from the child that will give him list with all his blocking URL.
	:return: None.
	"""
	ref = db.reference('Parents/' + str(PIndex) + '/childs/' + str(int(child) - 1) + '/blocked_websites')
	lastLst = ref.get()
	if lastLst != '':
		return lastLst
	else:
		return []

def unblockWebsite(website):
	"""
	This function adding the URL that the parents want to unblock to the firebase server.
	:return: None.
	"""
	#ref = db.reference('Parents/' + str(PIndex) + '/childs/' + str(int(child) - 1) + '/code')
	#ref.set(REQUEST_UNBLOCK_WEBSITES)
	ref = db.reference('Parents/' + str(PIndex) + '/childs/' + str(int(child) - 1) + '/blocked_websites')
	j = ref.get()
	j = [x for x in j if x != website]
	if j == []:
		j = ''
	ref.set(j)

def getStatistics():
	"""
	This function return the all of statistics that the child used programs.
	:return: The statistics.
	"""
	ref = db.reference('Parents/' + str(PIndex) + '/childs/' + str(int(child) - 1) + '/stats')
	j = ref.get()
	j = {k: v for k, v in sorted(j.items(), key=lambda item: item[1], reverse=True)}
	return j

def viewProgramName():
	"""
	This function print the username of the program and the process username that the child using now.
	:return: None.
	"""
	ref = db.reference('Parents/' + str(PIndex) + '/childs/' + str(int(child) - 1) + '/')
	j = ref.get()
	return j['curr_program']