from tkinter import *
from PIL import Image as PILImage
from PIL import ImageTk as PILImageTk
import io
import math
import time
import Client
import threading
import ctypes

signInWindow = ''
signUpWindow = ''
childsWindow = ''
mainWindow = ''
signErr = ''
username = ''
password = ''
childList = []
progLbl = ''
lb = ''
childName = ''

def main():
	windowSignIn()

def windowSignIn():
	"""
	This function create the parents' sign in window.
	:return: None.
	"""
	global signInWindow, signErr, username, password
	signInWindow = Tk()
	signInWindow.resizable(width = False, height = False)
	signInWindow.protocol("WM_DELETE_WINDOW", lambda *args: quit())
	signInWindow.title('Tracking & Blocking | SIGN IN')
	label = Label(text="""Welcome to Tracking & Blocking!\nEnter your username and password.""")
	label.grid(row = 0, column = 0, columnspan = 15)
	tv = Label(signInWindow, text = "Enter your username: ")
	tv1 = Label(signInWindow, text = "Enter your password: ")
	username = Entry(signInWindow)
	password = Entry(signInWindow, show = "*")
	tv.grid(row = 1, column = 0)
	tv1.grid(row = 2, column = 0)
	username.grid(row = 1, column = 1, columnspan = 14)
	password.grid(row = 2, column = 1, columnspan = 14)
	signInWindow.bind('<Return>', checkSignIn)
	btn = Button(signInWindow, text="Sign In", command=lambda:checkSignIn(""))
	btn.grid(row = 3, column = 0, columnspan = 15)
	signErr = Label(signInWindow, text = "", fg = 'red')
	signErr.grid(row = 4, column = 0, columnspan = 15)
	signUp = Label(signInWindow, text = """Dont have an account?\nCreate now!""")
	signUp.grid(row = 5, column = 0, columnspan = 15)
	signUpBtn = Button(signInWindow, text = "Sign Up", command=lambda:windowSignUp(""))
	signUpBtn.grid(row = 6, column = 0, columnspan = 15)
	signInWindow.mainloop()

def checkSignIn(event):
	"""
	This function check is the username and the password are true.
	:return: None.
	"""
	Client.signIn(username.get(), password.get())
	if (Client.login):
		signInWindow.withdraw()
		windowChildList()
	else:
		signErr['text'] = "Username or Password is incorrect!"

def backSignIn(event):
	"""
	This function do back to the sign in window from the sign up.
	:return: None.
	"""
	signInWindow.deiconify()
	signUpWindow.withdraw()

def windowSignUp(event):
	"""
	This function creates the signup window.
	:return: None.
	"""
	global signUpWindow, username, password, signErr
	signInWindow.withdraw()
	signUpWindow = Tk()
	signUpWindow.resizable(width = False, height = False)
	signUpWindow.protocol("WM_DELETE_WINDOW", lambda *args: quit())
	signUpWindow.title('Tracking & Blocking | SIGN UP')
	backBtn = Button(signUpWindow, text = "Back", command=lambda:backSignIn(""))
	backBtn.grid(row = 0, column = 0)
	tv = Label(signUpWindow, text = "Enter username: ")
	tv1 = Label(signUpWindow, text = "Enter password: ")
	username = Entry(signUpWindow)
	password = Entry(signUpWindow, show = "*")
	tv.grid(row = 1, column = 0)
	tv1.grid(row = 2, column = 0)
	username.grid(row = 1, column = 1, columnspan = 14)
	password.grid(row = 2, column = 1, columnspan = 14)
	btn = Button(signUpWindow, text = "SIGN UP", command=lambda: checkSignUp(""))
	btn.grid(row = 4, column = 0, columnspan = 15)
	signErr = Label(signUpWindow, text = "", fg = 'red')
	signErr.grid(row = 3, column = 0, columnspan = 15)
	signUpWindow.mainloop()

def checkSignUp(event):
	"""
	This function do sign up and check if the username is legal and didnt exist.
	:return: None.
	"""
	Client.signUp(username.get(), password.get())
	if (Client.login):
		signUpWindow.withdraw()
		windowChildList()
	else:
		signErr['text'] = "The username is already taken!"

def showChilds():
	"""
	This funciton show all of the child of the parents.
	:return: None.
	"""
	global childList, lb
	childList = Client.findChild()
	if childList != '':
		if lb != '':
			lb.grid_remove()
		lb = Listbox(childsWindow, width = 50)
		i = 0
		for item in childList:
			if item != None:
				lb.insert(i, childList[i]['name'])
				i += 1
		lb.grid(row = 1, column = 0)
		chooseBtn = Button(childsWindow, text = "Choose", command=lambda: checkChildSelected(""))
		chooseBtn.grid(row = 2, column = 0)
	else:
		if lb != '':
			lb.grid_remove()
		lb = ''
		lbl = Label(childsWindow, text = "There are no childs yet", width = 30)
		lbl.grid(row = 1, column = 0)

def windowChildList():
	"""
	This function create window of child list to choose one.
	:return: None.
	"""
	global childsWindow, lb
	childsWindow = Tk()
	childsWindow.protocol("WM_DELETE_WINDOW", lambda *args: quit())
	childsWindow.title('Tracking & Blocking | CHILDS')
	childsWindow.resizable(width = False, height = False)
	showChilds()
	refreshBtn = Button(childsWindow, text = "Refresh", command=lambda: showChilds())
	refreshBtn.grid(row = 0, column = 0)
	childsWindow.mainloop()

def checkChildSelected(event):
	"""
	This function check the button choose child, check if someone chose.
	:return: None.
	"""
	global signErr, childName
	if len(lb.curselection()) > 0:
		Client.child = int(lb.curselection()[0]) + 1
		childName = lb.get(lb.curselection()[0])
		windowMenu()
	else:
		signErr = Label(childsWindow, text = "No child has been chosen!", fg = "red")
		signErr.grid(row = 3, column = 0)

def backToList(event):
	"""
	This function do back to the child list window from the menu.
	:return: None.
	"""
	childsWindow.deiconify()
	mainWindow.withdraw()

def windowMenu():
	"""
	This function create window of menu with all of the buttons with the features.
	:return: None.
	"""
	global mainWindow, progLbl, childName
	Client.notify()
	childsWindow.withdraw()
	mainWindow = Tk()
	mainWindow.protocol("WM_DELETE_WINDOW", lambda *args: quit())
	mainWindow.title('Tracking & Blocking | MENU')
	mainWindow.resizable(width = False, height = False)
	childNameLbl = Label(mainWindow, text = f"Child: '{childName}'", width = 40)
	childNameLbl.grid(row = 1, column = 1)
	progLbl = Label(mainWindow, text = "")
	progLbl.grid(row = 2, column = 1)
	t = threading.Thread(target = showCurrProg, daemon = True)
	t.start()
	backBtn = Button(mainWindow, text = "Back", command=lambda:backToList(""))
	backBtn.grid(row = 0, column = 0)
	blockProgBtn = Button(mainWindow, text = "Block Program", width = 15, command=lambda: windowBlockProg(""))
	unblockProgBtn = Button(mainWindow, text = "UnBlock Program", width = 15, command=lambda: windowUnblockProg(""))
	blockWebBtn = Button(mainWindow, text="Block Website", width = 15, command=lambda: windowBlockWeb(""))
	unblockWebBtn = Button(mainWindow, text="UnBlock Website", width = 15, command=lambda: windowUnblockWeb(""))
	showEntriesBtn = Button(mainWindow, text="Show Entries", width = 15, command=lambda: windowEntries(""))
	showScreenShotBtn = Button(mainWindow, text="Show ScreenShot", width = 15, command=lambda: windowScreenShot())
	showStatisticsBtn = Button(mainWindow, text="Show Statistics", width = 15, command=lambda: windowStatistics())
	blockProgBtn.grid(row = 1, column = 0)
	unblockProgBtn.grid(row = 2, column = 0)
	blockWebBtn.grid(row = 1, column = 2)
	unblockWebBtn.grid(row = 2, column = 2)
	showEntriesBtn.grid(row = 3, column = 0, sticky="NESW")
	showScreenShotBtn.grid(row = 3, column = 1, sticky="NESW")
	showStatisticsBtn.grid(row=3, column=2, sticky="NESW")
	mainWindow.mainloop()

def windowScreenShot():
	"""
	This function creates window to show the screenshot from the child computer.
	:return: None.
	"""
	screenShotWindow = Tk()
	screenShotWindow.protocol("WM_DELETE_WINDOW", lambda *args: quit())
	screenShotWindow.title('Tracking & Blocking | SCREENSHOT')
	screenShotWindow.resizable(width = False, height = False)
	with open("ss.png", "wb") as f:
		f.write(Client.getScreenShot().encode('ISO-8859-1'))

	screensize = ctypes.windll.user32.GetSystemMetrics(0), ctypes.windll.user32.GetSystemMetrics(1)
	scale = 1
	image = PILImage.open("ss.png")
	while (int(image.width*scale) > screensize[0] or int(image.height*scale) > screensize[1]) and scale > 0.2:
		scale -= 0.1
	scale -= 0.1
	image = image.resize((int(image.width*scale), int(image.height*scale)), PILImage.ANTIALIAS)
	image.save(fp = "ss.png")

	mainWindow.withdraw()
	backBtn = Button(screenShotWindow, text = "Back", command=lambda: backToMenu(screenShotWindow))
	backBtn.grid(row = 0, column = 0)

	img = PhotoImage(master=screenShotWindow, file = "ss.png")
	lbl = Label(screenShotWindow, image = img)
	lbl.grid(row = 1, column = 0)

	screenShotWindow.mainloop()

def backToMenu(curr):
	"""
	This function closing the current window and opening the main menu window.
	:return: None.
	"""
	curr.withdraw()
	mainWindow.deiconify()

def updateList(data, lb):
	"""
	This function gets list and listview and adding all the items.
	:return: None.
	"""
	lb.delete(0, END)
	if isinstance(data, dict):
		for key, val in data.items():
			lb.insert(END, f"{key}: {val}")
		return
	for item in data:
		lb.insert(END, item)

def check(e, lb, lst):
	"""
	This function gets search text and list and filtering the list by contains the search keyword.
	:return: None.
	"""
	typed = e.get()
	if typed == '':
		data = lst
	else:
		data = []
		if isinstance(lst, dict):
			for key, value in lst.items():
				if typed.lower() in key.lower():
					data.append(key + ": " + value)
		else:
			for item in lst:
				if typed.lower() in item.lower():
					data.append(item)
	updateList(data, lb)

def createWindowList(name, lst, backFun, chooseFun, typeItem):
	"""
	This function creates general window with list.
	:return: None.
	"""
	window = Tk()
	window.protocol("WM_DELETE_WINDOW", lambda *args: quit())
	window.title(f'Tracking & Blocking | {name}')
	window.resizable(width = False, height = False)
	backBtn = Button(window, text = "Back", command=lambda:backFun(""))
	backBtn.grid(row = 0, column = 0)
	if name == "STATISTICS":
		lb = Listbox(window, width = 125)
	else:
		lb = Listbox(window, width = 100)
	if lst != []:
		search = Entry(window, width = 60)
		search.grid(row = 1, column = 0)
		search.bind('<KeyRelease>', lambda event: check(search, lb, lst))
		updateList(lst, lb)
		lb.grid(row = 2, column = 0)
		if chooseFun != None:
			chooseBtn = Button(window, text = "Choose", command=lambda:chooseFun(""))
			chooseBtn.grid(row = 3, column = 0)
	else:
		lbl = Label(window, text = f"There is no {typeItem}")
		lbl.grid(row = 1, column = 0)
	return window, lb

def blockProg(lb, blockProgWindow):
	"""
	This function is blocking the selected program from the list of all of the installed programs on the child computer.
	:return: None.
	"""
	if len(lb.curselection()) > 0:
		Client.blockProgram(lb.get(lb.curselection()[0]))
		backToMenu(blockProgWindow)
	else:
		print("Nothing choosed")

def windowBlockProg(event):
	"""
	This function creates blocking program window.
	:return: None.
	"""
	mainWindow.withdraw()
	programList = Client.getListPrograms()
	returned = createWindowList("BLOCK PROGRAM", programList, lambda event: backToMenu(blockProgWindow), lambda event: blockProg(lb, blockProgWindow), "programs")
	blockProgWindow = returned[0]
	lb = returned[1]
	blockProgWindow.mainloop()

def unblockProg(lb, unblockProgWindow):
	"""
	This function unblocking the selected blocked program.
	:return: None.
	"""
	if len(lb.curselection()) > 0:
		Client.unblockProgram(lb.get(lb.curselection()[0]))
		backToMenu(unblockProgWindow)
	else:
		pass

def windowUnblockProg(event):
	"""
	This function creates unblocking program window.
	:return: None.
	"""
	mainWindow.withdraw()
	blockedList = Client.getListBlockedPrograms()
	returned = createWindowList("UNBLOCK PROGRAM", blockedList, lambda event: backToMenu(unblockProgWindow), lambda event: unblockProg(lb, unblockProgWindow), "blocked programs")
	unblockProgWindow = returned[0]
	lb = returned[1]
	unblockProgWindow.mainloop()

def unblockWebsite(lb, unblockWebWindow):
	"""
	This function unblocking the selected blocked website.
	:return: None.
	"""
	if len(lb.curselection()) > 0:
		Client.unblockWebsite(lb.get(lb.curselection()[0]))
		backToMenu(unblockWebWindow)
	else:
		pass

def blockWebsite(website, blockWebsiteWindow):
	"""
	This function gets website URL and blocking it if it's not blocked yet.
	:return: None.
	"""
	if website != '':
		Client.BlockWebsite(website)
		backToMenu(blockWebsiteWindow)
	
def windowBlockWeb(event):
	"""
	This function creates the blocking website window.
	:return: None.
	"""
	mainWindow.withdraw()
	blockWebsiteWindow = Tk()
	blockWebsiteWindow.protocol("WM_DELETE_WINDOW", lambda *args: quit())
	blockWebsiteWindow.title('Tracking & Blocking | BLOCK WEBSITE')
	blockWebsiteWindow.resizable(width = False, height = False)
	backBtn = Button(blockWebsiteWindow, text = "Back", command=lambda: backToMenu(blockWebsiteWindow))
	backBtn.grid(row = 0, column = 0)
	lbl = Label(blockWebsiteWindow, text = "URL to block: ")
	lbl.grid(row = 1, column = 0)
	url = Entry(blockWebsiteWindow, width = 40)
	url.grid(row = 1, column = 1)
	choose = Button(blockWebsiteWindow, text = "Block", command = lambda: blockWebsite(url.get(), blockWebsiteWindow))
	choose.grid(row = 2, column = 0, columnspan = 2)

def windowUnblockWeb(event):
	"""
	This function creates the unblocking website window.
	:return: None.
	"""
	mainWindow.withdraw()
	blockedList = Client.getListBlockedWebsites()
	returned = createWindowList("UNBLOCK WEBSITE", blockedList, lambda event: backToMenu(unblockWebWindow), lambda event: unblockWebsite(lb, unblockWebWindow), "blocked websites")
	unblockWebWindow = returned[0]
	lb = returned[1]
	unblockWebWindow.mainloop()
	
def windowEntries(event):
	"""
	This function creates the entries history window.
	:return: None.
	"""
	mainWindow.withdraw()
	entries = Client.printEntries()
	entries = [f"{x['name']} - {x['time']}" for x in entries]
	returned = createWindowList("ENTRIES", entries, lambda event: backToMenu(entriesWindow), None, "entries")
	entriesWindow = returned[0]
	lb = returned[1]
	entriesWindow.mainloop()

def windowStatistics():
	"""
	This function creates the statistics window.
	:return: None.
	"""
	mainWindow.withdraw()
	statistics = Client.getStatistics()
	statistics = {k.replace(",", "."): time.strftime('%H:%M:%S', time.gmtime(v)) for k, v in statistics.items()}
	returned = createWindowList("STATISTICS", statistics, lambda event: backToMenu(statisticsWindow), None, "statistics")
	statisticsWindow = returned[0]
	lb = returned[1]
	statisticsWindow.mainloop()
	
def showCurrProg():
	"""
	This function showing to the parents the current program the child is usign rn.
	:return: None.
	"""
	while True:
		progLbl['text'] = Client.viewProgramName()

if __name__ == "__main__":
	main()