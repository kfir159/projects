import psutil
from subprocess import Popen
from win32process import DETACHED_PROCESS

flag = False
processName = "Server.exe"

f = open("C:/Windows/Temp/watchdog.txt","w")
f.write("start")
for proc in psutil.process_iter():
	try:
		if processName.lower() == proc.name().lower():
			print(f"{proc.name()} is running")
			flag = True
			break
	except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
		pass
if not flag:
	f.write("TB is not running")
	pid = Popen([r"C:\Program Files\TB\Server.exe"],creationflags=DETACHED_PROCESS,shell=False).pid
	f.write(str(pid))
f.close()