from scapy.all import *
import time
import os

blockedWebsites = []
history = []

def blockWebsite():
	"""
	This function running in while True loop and writing the domains to block into the hosts file in order to block the websites.
	:return: None.
	"""
	if not os.path.isfile(r"C:\Windows\System32\drivers\etc\hosts"):
		with open(r"C:\Windows\System32\drivers\etc\hosts", 'w'):
			pass
	try:
		with open(r"C:\Windows\System32\drivers\etc\hosts",'w') as file:
			for website in blockedWebsites:
				if website.startswith('www'):
					file.write("\n127.0.0.1" + " " + website)
					file.write("\n127.0.0.1" + " " + website[4:])
				else:
					file.write("\n127.0.0.1" + " " + website)
					file.write("\n127.0.0.1" + " www." + website)
	except Exception:
		pass
	time.sleep(60)

def getBlockedWebsites():
	"""
	This function return the all blocked websites of child.
	:return: List with the all blocked websites of child.
	"""
	if blockedWebsites == []:
		return ''
	return blockedWebsites

def setBlockedWebsites(URL):
	"""
	This function set the all of blocked websites of the child from the FireBase in local list.
	:param URL: List with all blocked websites from the FireBase.
	:return: Nome.
	"""
	global blockedWebsites
	if URL == '':
		blockedWebsites = []
	else:
		blockedWebsites = URL
		print(blockedWebsites)
		print(URL)