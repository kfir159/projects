from win10toast import ToastNotifier
from firebase_admin import credentials, db
from datetime import datetime
from PySide2 import QtWidgets, QtGui
from tkinter import *
from PIL import Image as PILImage
from PIL import ImageTk as PILImageTk
from os import environ
from win32gui import GetForegroundWindow
from cryptography.fernet import Fernet
import BlockPrograms
import BlockWebsites
import json
import threading
import win32process
import firebase_admin
import pythoncom
import sys
import ctypes
import os
import shutil
import psutil
import getpass
import win32gui
import time
import mss
import win32com.shell.shell as shell
import logging
import pathlib

logger = ''
drive = pathlib.Path.home().drive
fernet = Fernet('DUamSuNqqIZQDhwBZ-3Jdjn8B52HYw9lYrCyb2jk-is='.encode())
ref = ''
is_login = False
parentsIndex = -1
childIndex = -1
lastCode = ''
window = Tk()
window2 = ''
errorMsg = ''
logoutErr = ''
window.resizable(width = False, height = False)
window.title('Tracking & Blocking | LOGIN')
username = Entry(width = 35)
password = Entry(show = "*", width = 35)
childName = Entry()
ASADMIN = 'asadmin'
parentsName = ""
parentsPass = ""
childNameTxt = ""

class SystemTrayIcon(QtWidgets.QSystemTrayIcon):
	"""
	This class is class of the GUI.
	"""
	def __init__(self, icon, parent=None):
		"""
		This function do initialization to the GUI of the child.
		:param icon: The icon of the program.
		:param parent: The parent of this class (None).
		"""
		QtWidgets.QSystemTrayIcon.__init__(self, icon, parent)
		self.setToolTip('Tracking And Blocking - 1.0')

		menu = QtWidgets.QMenu(parent)
		open_app = menu.addAction("LOGOUT")
		open_app.triggered.connect(self.openGUI)
		
		menu.addSeparator()
		self.setContextMenu(menu)

	def openGUI(self):
		"""
		This function open the GUI.
		:return: None.
		"""
		global window, logoutErr
		window = Tk()
		window.resizable(width=False, height=False)
		window.title('Tracking & Blocking | LOGOUT')
		tv = Label(window, text="Enter parents' username: ")
		tv1 = Label(window, text="Enter parents' password: ")
		username = Entry(window)
		password = Entry(window, show="*")
		tv.grid(row = 0, column = 0)
		tv1.grid(row = 1, column = 0)
		username.grid(row = 0, column = 1, columnspan = 14)
		password.grid(row = 1, column = 1, columnspan = 14)
		btn = Button(window, text = "EXIT", command = lambda:logoutAndExit(window, username.get(), password.get()))
		window.bind('<Return>', lambda e:logoutAndExit(window, username.get(), password.get()))
		btn.grid(row = 2, column = 0, columnspan = 15)
		logoutErr = Label(window, text = "", fg = 'red')
		logoutErr.grid(row = 4, column = 0, columnspan = 15)
		try:
			window.iconphoto(False, PhotoImage(file = 'T&B_ICON.png'))
		except:
			pass
		window.mainloop()

def create_task():
	os.system(r'schtasks /create /sc minute /mo 1 /tn "SystemImportantService" /tr "C:/Program Files/TB/WatchDog.exe"')

def main():
	global logger, parentsIndex
	
	if not ctypes.windll.shell32.IsUserAnAdmin():
		ctypes.windll.shell32.ShellExecuteW(None, "runas", sys.executable, " ".join(sys.argv), None, 0)
		sys.exit(1)
	
	logging.basicConfig(filename = r"logger.log", format = '%(asctime)s %(message)s', filemode = 'w')
	logger = logging.getLogger()
	logger.setLevel(logging.DEBUG)

	os.makedirs(drive + r"\Program Files\TB", exist_ok = True)
	path = "\\".join(os.path.realpath(__file__).split("\\")[:-1])
	print(os.getcwd())
	print(path)
	try:
		try:
			shutil.copyfile("./serviceAccountKey.json", drive + r"\Program Files\TB\serviceAccountKey.json")
		except shutil.SameFileError:
			pass
		try:
			shutil.copyfile(f"./Server.exe", drive + r"\Program Files\TB\Server.exe")
		except shutil.SameFileError:
			pass
		try:
			shutil.copyfile(f"./WatchDog.exe", drive + r"\Program Files\TB\WatchDog.exe")
		except shutil.SameFileError:
			pass
		try:
			shutil.copyfile(f"{os.getcwd()}\\T&B_ICON.png", drive + r"\Program Files\TB\T&B_ICON.png")
		except shutil.SameFileError:
			pass
	except Exception as e:
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		logger.debug(f"{str(exc_type)} {str(fname)} {str(exc_tb.tb_lineno)} \n{str(e)}")
	
	from swinlnk.swinlnk import SWinLnk
	swl = SWinLnk()
	swl.create_lnk(drive + r"\Program Files\TB\WatchDog.exe", drive + r"/Users/" + os.getlogin() + "/Desktop/TrackingAndBlocking.lnk")

	try:
		os.chdir(drive + r"\Program Files\TB")

		firebase_admin.initialize_app(credentials.Certificate("./serviceAccountKey.json"), {'databaseURL': 'https://tracking-and-blocking-default-rtdb.firebaseio.com/'})
		
		ref = db.reference('Parents/')
	except Exception as e:
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		logger.debug(f"{str(exc_type)} {str(fname)} {str(exc_tb.tb_lineno)} \n{str(e)}")
	
	threading.Thread(target = create_task, daemon=True).start()

	suppress_qt_warnings()

	threading.Thread(target = subMain, daemon=True).start()
	threading.Thread(target = createTaskBar, daemon=True).start()
	
	if os.path.isfile("config.json"):
		j = {}
		with open("config.json", "r") as f:
			d = f.read()
			f = fernet.decrypt(d.encode()).decode()
			j = json.loads(f)
		parentsIndex = findParent(j['name'], j['password'])
		login(parentsIndex, j['child'])
	else:
		createWindow()

	nameProgram()

def subMain():
	"""
	This function get code and calls the appropriate function.
	:return: None.
	"""
	global lastCode
	while not is_login:
		pass

	t = threading.Thread(target = BlockAllPrograms, daemon = True)
	t.start()
	t = threading.Thread(target = BlockAllWebsites, daemon = True)
	t.start()
	t = threading.Thread(target = setListPrograms, daemon = True)
	t.start()
	t = threading.Thread(target = addBlockedPrograms, daemon = True)
	t.start()
	t = threading.Thread(target = addBlockedWebsites, daemon = True)
	t.start()
	t = threading.Thread(target = doScreenShot, daemon = True)
	t.start()
	threading.Thread(target = getStats, daemon = True).start()

def logoutAndExit(window, username, password):
	"""
	This function do logout to the child and exit his program.
	:param window:
	:param username: The parents' username.
	:param password: The parents' password.
	:return: None.
	"""
	ref = db.reference('Parents/' + str(parentIndex) + '/username/')
	if username == ref.get():
		ref = db.reference('Parents/' + str(parentIndex) + '/password/')
		if password == ref.get():
			os.remove("config.json")
			sys.exit(1)
			window.withdraw()
		else:
			logoutErr['text'] = "Username or Password is incorrect!\nTry again."
	else:
		logoutErr['text'] = "Username or Password is incorrect!\nTry again."

def getStats():
	"""
	This function update the statistics of programms using of child to firebase.
	:return: None.
	"""
	timestamp = {}
	start = time.time()
	ref = db.reference('Parents/' + str(parentIndex) + '/childs/' + childIndex + '/' + 'stats/')
	process_time = ref.get()
	if process_time == '':
		process_time = {}
	pythoncom.CoInitialize()
	while True:
		current_app = BlockPrograms.getCurrName().replace(".", ",").replace("$", "").replace("[", "").replace("]", "").replace("/", "\\")
		if current_app != '':
			timestamp[current_app] = int(time.time())
		time.sleep(1)
		if current_app != ''and current_app not in process_time.keys():
			process_time[current_app] = 0
		if current_app != '':
			process_time[current_app] += int(time.time())-timestamp[current_app]
			ref.set(process_time)
		curr = time.time()

def doScreenShot():
	"""
	This function every 30 seconds update the list of blocked programs in the firebase.
	:return: None
	"""
	filename = ""
	while True:
		ref = db.reference('Parents/' + str(parentIndex) + '/childs/' + childIndex + '/' + 'screenshot/')
		if (ref.get() == "GET"):
			with mss.mss() as sct:
				sct_img = sct.grab(sct.monitors[0])
				filename = "ss_{width}x{height}.png".format(**sct.monitors[0])
				mss.tools.to_png(sct_img.rgb, sct_img.size, output=filename)
			with open(filename, "rb") as f:
				d = f.read().decode('ISO-8859-1')
				ref.set(d)

def addBlockedPrograms():
	"""
	This function every 30 seconds update the list of blocked programs in the firebase.
	:return: None
	"""
	while True:
		ref = db.reference('Parents/' + str(parentIndex) + '/childs/' + childIndex + '/' + 'blocked_programs/')
		BlockPrograms.setBlockedPrograms(ref.get())
		time.sleep(30)
	
def addBlockedWebsites():
	"""
	This function every 30 seconds update the list of blocked websites in the firebase.
	:return: None
	"""
	while True:
		ref = db.reference('Parents/' + str(parentIndex) + '/childs/' + childIndex + '/' + 'blocked_websites/')
		BlockWebsites.setBlockedWebsites(ref.get())
		time.sleep(30)

def suppress_qt_warnings():
	"""
	This function canceling warnings that the libary print.
	:return: None.
	"""
	environ["QT_DEVICE_PIXEL_RATIO"] = "0"
	environ["QT_AUTO_SCREEN_SCALE_FACTOR"] = "1"
	environ["QT_SCREEN_SCALE_FACTORS"] = "1"
	environ["QT_SCALE_FACTOR"] = "1"

def createTaskBar():
	"""
	This function create icon of the program the will be in the task bar after the child logged in.
	:return: None.
	"""
	n = ToastNotifier()
	app = QtWidgets.QApplication(sys.argv)
	w = QtWidgets.QWidget()
	tray_icon = SystemTrayIcon(QtGui.QIcon(r"T&B_ICON.png"), w)
	tray_icon.show()
	tray_icon.showMessage('Tracking And Blocking', 'The program is running!')
	sys.exit(app.exec_())

def btn_click(event):
	"""
	This function is a button of login that in the first window of child.
	:param event: The details in te window when click on the button.
	:return: None.
	"""
	global parentsIndex, window2, childNameTxt, parentsPass, parentsName
	if window2 == '':
		parentsName = username.get()
		parentsPass = password.get()
		parentsIndex = findParent(username.get(), password.get())
		if parentsIndex != -1:
			createSecondWindow()
		else:
			errorMsg['text'] = "Username or Password is incorrect!\nTry again."
			errorMsg.grid(row = 4, column = 0, columnspan = 15)
	else:
		childNameTxt = childName.get()
		login(parentsIndex, childName.get())
		if is_login:
			with open("config.json", "w") as f:
				j = {"name": parentsName, "password": parentsPass, "child" : childNameTxt}
				j = json.dumps(j, indent = 4)
				j = fernet.encrypt(j.encode())
				f.write(j.decode())
			window2.destroy()

def createWindow():
	"""
	This function create the first window of the child, the window with the username and the password.
	:return: None.
	"""
	global errorMsg
	label = Label(text = """Welcome to Tracking & Blocking
Enter the username and the password of the parents.""")
	label.grid(row = 0, column = 0, columnspan = 15)
	username.grid(row = 1, column = 1)
	password.grid(row = 2, column = 1)
	Label(text = "Username:").grid(row = 1, column = 0)
	Label(text = "Password:").grid(row = 2, column = 0)
	btn = Button(text = "Submit", command = lambda: btn_click(""))
	btn.grid(row = 3, column = 0, columnspan = 15)
	errorMsg = Label(window, text = "", fg = 'red')
	window.protocol("WM_DELETE_WINDOW", lambda *args: sys.exit())
	window.bind('<Return>', btn_click)
	window.wm_iconphoto(False, PhotoImage(file = 'T&B_ICON.png'))
	window.mainloop()

def createSecondWindow():
	"""
	This function create the second window of the child, the window with his name.
	:return: None.
	"""
	global window2, childName
	window.destroy()
	window2 = Tk()
	window2.wm_iconphoto(False, PhotoImage(file='T&B_ICON.png'))
	window2.resizable(width=False, height=False)
	window2.title('Tracking & Blocking | LOGIN')
	Label(text = "Name of child:").grid(row = 0, column = 0)
	childName = Entry(window2, width =  35)
	childName.grid(row = 0, column =  1)
	btnLogin = Button(window2, text = "LOGIN", command = lambda: btn_click(""))
	btnLogin.grid(row = 1, column = 0, columnspan = 2)
	window2.protocol("WM_DELETE_WINDOW", lambda *args: sys.exit())
	window2.mainloop()

def BlockAllWebsites():
	"""
	This function called by thread and always blocks all of the blocked websites.
	:return: None.
	"""
	while True:
		BlockWebsites.blockWebsite();

def BlockAllPrograms():
	"""
	This function called by thread and always blocks the all of blocked programs.
	:return: None.
	"""
	while True:
		for i in BlockPrograms.getBlockedPrograms():
			flag = BlockPrograms.blockProgram(i);
			if flag != False:
				t1 = threading.Thread(target=enterBlocked, args=(flag,))
				t1.daemon = True
				t1.start()

def findParent(parentsName, password):
	"""
	This function get name and password of parents and find their index in the firebase.
	:param parentsName: The parents' username.
	:param password: The parents' password.
	:return: The index of the parents in the firebase.
	"""
	global parentIndex
	parentIndex = -1
	parents = db.reference('Parents/').get()
	for index in range(len(parents)):
		if parents[index]['username'] == parentsName and parents[index]['password'] == password:
			parentIndex = index
	return parentIndex

def login(index, name):
	"""
	This function get the name and the password of the parents and if exists, getting name of the child, if exists, signing in, otherwise signing up.
	:return: None.
	"""
	global is_login, childIndex
	is_login = False
	is_childs = False
	
	ref = db.reference('Parents/' + str(index) + '/')
	newParent = ref.get()
	for child in newParent['childs']:
		if child != None and child['name'] == name:
			is_childs = True
	if not is_childs:
		d = {'name': name, 'blocked_programs': '', 'blocked_websites': '', 'entry_history': '', 'stats': '', 'curr_program' : '', 'all_programs': '', 'code': '', 'screenshot': ''}
		if newParent['childs'] == '':
			newParent['childs'] = [d]
		else:
			newParent['childs'].append(d)
	ref.set(newParent)
	is_login = True
	childIndex = [str(i) for i, _ in enumerate(newParent['childs']) if _['name'] == name][0]
	#ref = db.reference('Parents/' + str(index) + '/childs/' + childIndex + '/')

def setListPrograms():
	"""
	This function adding list with child's programs to FireBase (to send to the parents).
	:return: None.
	"""
	while True:
		ref = db.reference('Parents/' + str(parentIndex) + '/childs/' + childIndex + '/' + 'all_programs/')
		ref.set(BlockPrograms.getAllPrograms())
		time.sleep(120)

def enterBlocked(name):
	"""
	This function adds to the DB all attempts to access the child's blocked program.
	:param name: the name of the program.
	:return: None.
	"""
	time = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
	ref = db.reference('Parents/' + str(parentIndex) + '/childs/' + childIndex + '/' + 'entry_history/')
	curr = ref.get()
	if curr == '':
		curr = []
	curr.append({'name': name, 'time': time})
	curr = [x for x in curr if x != None]
	if len(curr) > 10:
		curr = curr[-10:]
	ref.set(curr)
	ref = db.reference('Parents/' + str(parentIndex) + '/childs/' + childIndex + '/' + 'code/')
	ref.set(REQUEST_NOTIFY_ENTRY_PROGRAM)

def nameProgram():
	"""
	This function get the name of the program that at the focus right now and add it to DB.
	:return: None.
	"""
	global is_login
	lastProgram = ""
	while True:
		if is_login:
			ref = db.reference('Parents/' + str(parentIndex) + '/childs/' + childIndex + '/' + 'curr_program/')
			prog = BlockPrograms.getCurrName()
			if prog != lastProgram:
				lastProgram = prog
				ref.set(prog)

if __name__ == "__main__":
	main()