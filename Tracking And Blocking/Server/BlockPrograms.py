from win32com.client import GetObject
import win32process
import win32gui
import winreg
import psutil
import time
import wmi

blockedPrograms = []

def getCurrName():
	"""
	This function get the name of the program and the process name that the child using now.
	:return: The name of program and the process.
	"""
	try:
		c = wmi.WMI()
		_, pid = win32process.GetWindowThreadProcessId(win32gui.GetForegroundWindow())
		name = win32gui.GetWindowText(win32gui.GetForegroundWindow())
		return c.query(f'SELECT Name FROM Win32_Process WHERE ProcessId = {str(pid)}')[0].Name + ' - ' + (name if " - " not in name else name.split(" - ")[-1])
	except Exception as e:
		print(e)
		return ""


def getPrograms(hive, flag):
	"""
	This function get list with the all of programs on the child's computer in specific path.
	:param hive: The location in the registry.
	:param flag: Flag.
	:return: List with the all of programs on the child's computer in specific path.
	"""
	aReg = winreg.ConnectRegistry(None, hive)
	aKey = winreg.OpenKey(aReg, r"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", 0, winreg.KEY_READ | flag)
	count_subkey = winreg.QueryInfoKey(aKey)[0]
	software_list = []
	for i in range(count_subkey):
		try:
			software_list.append(winreg.QueryValueEx(winreg.OpenKey(aKey, winreg.EnumKey(aKey, i)), "DisplayName")[0])
		except Exception:
			continue
	return software_list

def getAllPrograms():
	"""
	This function get list with the all of programs on the child's computer.
	:return: List with the all of programs on the child's computer.
	"""
	software_list = getPrograms(winreg.HKEY_LOCAL_MACHINE, winreg.KEY_WOW64_32KEY) + getPrograms(winreg.HKEY_LOCAL_MACHINE, winreg.KEY_WOW64_64KEY) + getPrograms(winreg.HKEY_CURRENT_USER, 0)
	return sorted(software_list, key = str.lower)

def blockProgram(program):
	"""
	This function splitting the name of the program that need to blocked by whitespace and calls other function that killing the program.
	:param program: The name of the program to block.
	:return: None.
	"""
	if killProcessByName(program):
		return program
	for word in program.split(" "):
		if killProcessByName(word):
			return program
	return False

def getBlockedPrograms():
	"""
	This function return the all blocked programs of child.
	:return: List with the all blocked programs of child.
	"""
	if blockedPrograms == []:
		return ''
	return blockedPrograms

def setBlockedPrograms(programs):
	"""
	This function set the all of blocked programs of the child from the FireBase in local list.
	:param programs: List with all blocked programs from the FireBase.
	:return: Nome.
	"""
	global blockedPrograms
	if programs == '':
		blockedPrograms = []
	else:
		blockedPrograms = programs

def killProcessByName(processName):
	"""
	This function iterating over all of the running processes at the moment and if found process with the name of a blocked program, its killing the process.
	:param processName: The name of the process to search if running and kill then.
	:return: None.
	"""
	for proc in psutil.process_iter():
		try:
			pinfo = proc.as_dict(attrs = ['pid', 'name'])
			if pinfo['name'] == None:
				continue
			if processName.lower() in pinfo['name'].lower():
				psutil.Process(pinfo['pid']).terminate()
				return True
		except (psutil.NoSuchProcess, psutil.AccessDenied , psutil.ZombieProcess):
			continue
	return False